package renderer
{
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;

import simulation.SimBall;

public class LauncherRenderer implements IFrameUpdateObject
{
	public static const ROLE_USER_OPPONENT:String = "role_user_opponent";
	public static const ROLE_AI_OPPONENT:String = "role_ai_opponent";
	public static const ROLE_USER_SERVE:String = "role_user_serve";
	private var side_:String;
	private var tableRenderer_:TableRenderer;
	private var role_:String;
	private var levelWidth_:int;
	private var levelHeight_:int;
	private static const WIDTH_PERCENT:int = 3;
	private static const ASPECT_RATIO:Number = 0.6;
	private static const NOZZLE_X_RATIO:Number = 0.2;
	private static const NOZZLE_Y_RATIO:Number = 0.4;
	public var nozzlePosX_:int;
	public var nozzlePosY_:int;
	private var states_:SimpleStateMachine;
	
	private var launcher_:Sprite;
	
	public function LauncherRenderer(side:String, tableRenderer:TableRenderer) {
		side_ = side;
		tableRenderer_ = tableRenderer;
		launcher_ = new Sprite();
		states_ = new SimpleStateMachine([
			['hidden', 0],
			['showing', 1],
		]);
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	public function show(role:String, relPosLeftScreenX:int, relPosScreenY:int):void {
		role_ = role;
		levelWidth_ = StageUtil.getSingleton().stageWidth_;
		levelHeight_ = StageUtil.getSingleton().stageHeight_;
		if (!states_.isStateActive("hidden")) {
			throw new Error("show: launcher is not hidden");
		}
		if ((role == ROLE_USER_OPPONENT) || (role == ROLE_AI_OPPONENT))  {
			nozzlePosX_ = (levelWidth_ / 2 - tableRenderer_.tableTopWidth_ / 2) + relPosLeftScreenX;
			nozzlePosX_ = (side_ == SimBall.RIGHT_SIDE) ? levelWidth_ - nozzlePosX_ : nozzlePosX_;
			if (role == ROLE_AI_OPPONENT) {
				nozzlePosX_ = levelWidth_ - nozzlePosX_;
			}
			nozzlePosY_ = tableRenderer_.tableTopPosY_ - tableRenderer_.tableLegHeight_ * 2 / 3 - relPosScreenY;
			drawOpponentRole();
		} else if (role == ROLE_USER_SERVE) {
			nozzlePosX_ = levelWidth_ / 2 + tableRenderer_.tableTopWidth_ / 2 * 0.8 + relPosLeftScreenX;
			nozzlePosX_ = (side_ == SimBall.RIGHT_SIDE) ? levelWidth_ - nozzlePosX_ : nozzlePosX_;  
			nozzlePosY_ = tableRenderer_.tableTopPosY_ - tableRenderer_.tableLegHeight_ * 1 / 4 - relPosScreenY;
			drawServeRole();
		} else {
			throw new Error("unhandled ball launcher role pos");
		}
		
		launcher_.cacheAsBitmap = true;
		StageUtil.getSingleton().addToStage(launcher_);
		states_.setActiveState("showing");
	}
	
	public function getRole():String {
		return role_;
	}
	
	public function getInitPosSubtype():String {
		if (role_ == ROLE_USER_OPPONENT) {
			return SimBall.INIT_POS_TYPE_OPPONENT_STRIKE;
		} else if (role_ == ROLE_AI_OPPONENT) {
			return SimBall.INIT_POS_TYPE_OPPONENT_STRIKE;
		} else if (role_ == ROLE_USER_SERVE) {
			return SimBall.INIT_POS_TYPE_SERVICE;
		} else {
			throw new Error("unhandled role for init pos subtype");
		}
	}
	
	private function drawOpponentRole():void {
		var width:int = levelWidth_ * WIDTH_PERCENT / 100;
		var height:int = width * ASPECT_RATIO;
		launcher_.graphics.clear();
		launcher_.graphics.beginFill(0x888888);
		launcher_.graphics.drawRect(0, 0, width * (1 - NOZZLE_X_RATIO), height);
		launcher_.graphics.drawRect(width * (1 - NOZZLE_X_RATIO), height * (1 - NOZZLE_Y_RATIO) / 2,
			width * NOZZLE_X_RATIO, height * NOZZLE_Y_RATIO);
		launcher_.graphics.endFill();
		launcher_.y = nozzlePosY_ - height / 2;
		
		if (nozzlePosX_ < levelWidth_ / 2) {
			launcher_.scaleX = 1;
			launcher_.x = nozzlePosX_ - width;
		} else {
			launcher_.scaleX = -1;
			launcher_.x = nozzlePosX_ + width;
		}
	}
	
	private function drawServeRole():void {
		var width:int = levelWidth_ * WIDTH_PERCENT / 100;
		var height:int = width * ASPECT_RATIO;
		launcher_.graphics.clear();
		launcher_.graphics.beginFill(0x888888);
		launcher_.graphics.drawRect(0, 0, width, height);
		launcher_.graphics.endFill();
		launcher_.scaleX = 1;
		launcher_.x = nozzlePosX_ - width / 2;
		launcher_.y = nozzlePosY_;
	}
	
	public function hide():void {
		if (!states_.isStateActive("hidden")) {
			StageUtil.getSingleton().removeFromStage(launcher_);
			states_.setActiveState("hidden");
		}
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		states_.update(frameElapsedTime);
		if (states_.isStateActive('showing') && states_.isActiveStateExpired()) {
			hide();
		}
		return true;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "launcher_renderer";
	}
}
}
package renderer
{
	import core.GameConfig;
	import core.ImageLoader;
	import core.StageUtil;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.utils.Dictionary;
	
	public class SetupItemSliderRenderer extends SetupItemRendererBase
	{
		private var bitmap1_:Bitmap;
		private var limitImage1_:Sprite;
		private var bitmap2_:Bitmap;
		private var limitImage2_:Sprite;
		private static const IMAGE_SIZE_HEIGHT_PERCENT:int = 30;
		private static const IMAGE_GAP_HEIGHT_PERCENT:int = 60;
		private static const IMAGE_TOP_MARGIN_PERCENT:int = 20;
		
		private var bar_:Sprite;
		private var barPosX_:int;
		private static const BAR_WIDTH_PERCENT:int = 70; 
		private static const BAR_HEIGHT_PERCENT:int = 3; 
		private static const BAR_POS_Y_PERCENT:int = 60; 
		private static const TICK_HEIGHT_PERCENT:int = 4; 
		private static const TICK_WIDTH_PIXELS:int = 6;
		
		private var pointer_:Sprite;
		private static const POINTER_WIDTH_PERCENT:int = 4;
		private static const POINTER_HEIGHT_PERCENT:int = 4;
		
		private var userInputArea_:Sprite;
		private static const USER_INPUT_AREA_HEIGHT_PERCENT:int = 12;

		public function SetupItemSliderRenderer(itemName:String, itemData:Dictionary) {
			super(itemName, itemData);

			limitImage1_ = new Sprite();
			bitmap1_ = ImageLoader.getSingleton().getBitmap(itemData['bitmapName1']);
			limitImage1_.addChild(bitmap1_);

			limitImage2_ = new Sprite();
			bitmap2_ = ImageLoader.getSingleton().getBitmap(itemData['bitmapName2']);
			limitImage2_.addChild(bitmap2_);
			
			bar_ = new Sprite();
			pointer_ = new Sprite();
			userInputArea_ = new Sprite();
			
			if (StageUtil.getSingleton().userInput_.touchPointSupported) {
				userInputArea_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
					onBarUserInput(event.localX);
				});
				userInputArea_.addEventListener(TouchEvent.TOUCH_MOVE, function(event:TouchEvent):void {
					onBarUserInput(event.localX);
				});
			} else {
				userInputArea_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
					onBarUserInput(event.localX);
				});
				userInputArea_.addEventListener(MouseEvent.MOUSE_MOVE, function(event:MouseEvent):void {
					onBarUserInput(event.localX);
				});
			}
		}
		
		public override function show(dialogWidth:int, dialogHeight:int):void {
			dialogWidth_ = dialogWidth;
			dialogHeight_ = dialogHeight;
			var button1AsRatio:Number = bitmap1_.width / bitmap1_.height;
			limitImage1_.width = dialogHeight * IMAGE_SIZE_HEIGHT_PERCENT / 100 * button1AsRatio;
			limitImage1_.height = dialogHeight * IMAGE_SIZE_HEIGHT_PERCENT / 100;
			var gap:int = dialogHeight * IMAGE_GAP_HEIGHT_PERCENT / 100;
			var topMargin:int = dialogHeight * IMAGE_TOP_MARGIN_PERCENT / 100;
			limitImage1_.x = dialogWidth / 2 - gap / 2 - limitImage1_.width;
			limitImage1_.y = topMargin;
			StageUtil.getSingleton().addToStage(limitImage1_);
			
			var button2AsRatio:Number = bitmap1_.width / bitmap1_.height;
			limitImage2_.width = dialogHeight * IMAGE_SIZE_HEIGHT_PERCENT / 100 * button2AsRatio;
			limitImage2_.height = dialogHeight * IMAGE_SIZE_HEIGHT_PERCENT / 100;
			limitImage2_.x = dialogWidth / 2 + gap / 2;
			limitImage2_.y = topMargin;
			StageUtil.getSingleton().addToStage(limitImage2_);
			
			bar_.graphics.clear();
			bar_.graphics.beginFill(0x22222);
			var barWidth:int = dialogWidth * BAR_WIDTH_PERCENT / 100;
			bar_.graphics.drawRect(0, 0, barWidth, -dialogHeight * BAR_HEIGHT_PERCENT / 100);
			var tickStep:int = itemData_['valueStep'] || 1;
			var numTicks:int = (itemData_['valueMax'] - itemData_['valueMin']) / tickStep + 1;
			for (var ti:int = 0; ti < numTicks; ti++) {
				var tickX:int;
				if (ti == 0) {
					tickX = 0;
				} else if (ti < numTicks - 1) {
					tickX = barWidth / (numTicks - 1) * ti - TICK_WIDTH_PIXELS / 2;
				} else {
					tickX = barWidth / (numTicks - 1) * ti - TICK_WIDTH_PIXELS;
				}
				bar_.graphics.drawRect(tickX, 0,
					TICK_WIDTH_PIXELS, TICK_HEIGHT_PERCENT * dialogHeight / 100);
			}
			bar_.graphics.endFill();
			barPosX_ = dialogWidth * (100 - BAR_WIDTH_PERCENT) / 2 / 100; 
			bar_.x = barPosX_;
			bar_.y = dialogHeight * BAR_POS_Y_PERCENT / 100;
			StageUtil.getSingleton().addToStage(bar_);

			showSelectedValue();
			StageUtil.getSingleton().addToStage(pointer_);
			
			userInputArea_.graphics.clear();
			userInputArea_.graphics.beginFill(0, 0);
			userInputArea_.graphics.drawRect(0, 0, barWidth, dialogHeight * USER_INPUT_AREA_HEIGHT_PERCENT / 100);
			userInputArea_.graphics.endFill();
			userInputArea_.x = barPosX_;
			userInputArea_.y = dialogHeight * BAR_POS_Y_PERCENT / 100 - 
				dialogHeight * USER_INPUT_AREA_HEIGHT_PERCENT / 100 / 2;
			StageUtil.getSingleton().addToStage(userInputArea_);
			
			itemChangeLock_ = false;
		}
		
		private function showSelectedValue():void {
			pointer_.graphics.clear();
			pointer_.graphics.beginFill(0xAA5555);
			pointer_.graphics.drawRect(0, 0, 
				POINTER_WIDTH_PERCENT * dialogWidth_ / 100, POINTER_HEIGHT_PERCENT * dialogHeight_ / 100);
			pointer_.graphics.endFill();
			var barWidth:int = dialogWidth_ * BAR_WIDTH_PERCENT / 100;
			var tickStep:int = itemData_['valueStep'] || 1;
			var ti:int = (GameConfig.getSingleton().getValue(itemData_['itemName']) - itemData_['valueMin']) / tickStep;
			var numTicks:int = (itemData_['valueMax'] - itemData_['valueMin']) / tickStep + 1;
			var tickX:int = barWidth / (numTicks - 1) * ti;
			pointer_.x = barPosX_ + tickX - POINTER_WIDTH_PERCENT * dialogWidth_ / 2 / 100;
			pointer_.y = dialogHeight_ * BAR_POS_Y_PERCENT / 100;
		}
		
		private function onBarUserInput(localX:int):void {
			if (itemChangeLock_) {
				return;
			}
			var barWidth:int = dialogWidth_ * BAR_WIDTH_PERCENT / 100;
			var tickStep:int = itemData_['valueStep'] || 1;
			var numTicks:int = (itemData_['valueMax'] - itemData_['valueMin']) / tickStep + 1;
			var closestTickIndex:int;
			var closestTickDist:int;
			for (var ti:int = 0; ti < numTicks; ti++) {
				var tickX:int = barWidth / (numTicks - 1) * ti;
				if (ti == 0) {
					closestTickDist = Math.abs(localX - tickX);
					closestTickIndex = 0;
				} else {
					var dist:int = Math.abs(localX - tickX);
					if (dist < closestTickDist) {
						closestTickDist = dist;
						closestTickIndex = ti;
					}
				}
			}
			var newItemValue:int = closestTickIndex * tickStep + itemData_['valueMin'];
			if (newItemValue != GameConfig.getSingleton().getValue(itemData_['itemName'])) {
				itemChangeLock_ = true;
				GameConfig.getSingleton().setValue(itemData_['itemName'], newItemValue, function():void {
					itemChangeLock_ = false;
					showSelectedValue();
				});
			}
		}
		
		public override function hide():void {
			StageUtil.getSingleton().removeFromStage(limitImage1_);
			StageUtil.getSingleton().removeFromStage(limitImage2_);
			StageUtil.getSingleton().removeFromStage(bar_);
			StageUtil.getSingleton().removeFromStage(pointer_);
			StageUtil.getSingleton().removeFromStage(userInputArea_);
		}
	}
		
}
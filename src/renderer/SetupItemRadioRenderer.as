package renderer
{
	import core.GameConfig;
	import core.ImageLoader;
	import core.StageUtil;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.utils.Dictionary;

	public class SetupItemRadioRenderer extends SetupItemRendererBase
	{
		private var bitmap1_:Bitmap;
		private var button1_:Sprite;
		private var buttonSelect1_:Sprite;
		private var bitmap2_:Bitmap;
		private var button2_:Sprite;
		private var buttonSelect2_:Sprite;
		
		private const BUTTON_SIZE_HEIGHT_PERCENT:int = 50;
		private const BUTTON_GAP_HEIGHT_PERCENT:int = 10;
		private const RADIOBOX_SIZE_HEIGHT_PERCENT:int = 10;
		private const RADIOBOX_COLOR_SELECTED:int = 0xAA8844;
		private const RADIOBOX_COLOR_UNSELECTED:int = 0x332211;
		
		public function SetupItemRadioRenderer(itemName:String, itemData:Dictionary) {
			super(itemName, itemData);
			
			button1_ = new Sprite();
			bitmap1_ = ImageLoader.getSingleton().getBitmap(itemData['bitmapName1']);
			button1_.addChild(bitmap1_);
			buttonSelect1_ = new Sprite();
			buttonSelect2_ = new Sprite();
			
			button2_ = new Sprite();
			bitmap2_ = ImageLoader.getSingleton().getBitmap(itemData['bitmapName2']);
			button2_.addChild(bitmap2_);

			if (StageUtil.getSingleton().userInput_.touchPointSupported) {
				button1_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
					onItemSelected(1);
				});
				button2_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
					onItemSelected(2);
				});
			} else {
				button1_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
					onItemSelected(1);
				});
				button2_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
					onItemSelected(2);
				});
			}
		}
		
		public override function show(dialogWidth:int, dialogHeight:int):void {
			var button1AsRatio:Number = bitmap1_.width / bitmap1_.height;
			button1_.width = dialogHeight * BUTTON_SIZE_HEIGHT_PERCENT / 100 * button1AsRatio;
			button1_.height = dialogHeight * BUTTON_SIZE_HEIGHT_PERCENT / 100;
			var gap:int = dialogHeight * BUTTON_GAP_HEIGHT_PERCENT / 100;
			button1_.x = dialogWidth / 2 - gap / 2 - button1_.width;
			button1_.y = dialogHeight / 2 - button1_.height / 2;
			
			StageUtil.getSingleton().addToStage(buttonSelect1_);
			StageUtil.getSingleton().addToStage(button1_);

			var button2AsRatio:Number = bitmap1_.width / bitmap1_.height;
			button2_.width = dialogHeight * BUTTON_SIZE_HEIGHT_PERCENT / 100 * button2AsRatio;
			button2_.height = dialogHeight * BUTTON_SIZE_HEIGHT_PERCENT / 100;
			button2_.x = dialogWidth / 2 + gap / 2;
			button2_.y = dialogHeight / 2 - button2_.height / 2;

			StageUtil.getSingleton().addToStage(buttonSelect2_);
			StageUtil.getSingleton().addToStage(button2_);
			
			showSelectedItem();
			itemChangeLock_ = false;
		}
		
		private function onItemSelected(itemIndex:int):void {
			if (itemChangeLock_) {
				trace("W SetupItemRadioRenderer onItemSelected with itemChangeLock_ true"); 
				return;
			}
			itemChangeLock_ = true;
			GameConfig.getSingleton().setValue(itemData_['itemName'], itemIndex, function():void {
				itemChangeLock_ = false;
			});
			showSelectedItem();
		}
		
		private function showSelectedItem():void {
			var itemIndex:int = GameConfig.getSingleton().getValue(itemData_['itemName']); 
				
			buttonSelect1_.graphics.clear();
			buttonSelect1_.graphics.beginFill(itemIndex == 1 ? 
				RADIOBOX_COLOR_SELECTED : RADIOBOX_COLOR_UNSELECTED);
			buttonSelect1_.graphics.drawRect(0, 0, button1_.width + 20, button1_.height + 20);
			buttonSelect1_.graphics.endFill();
			buttonSelect1_.x = button1_.x - 10;
			buttonSelect1_.y = button1_.y - 10;
			
			buttonSelect2_.graphics.clear();
			buttonSelect2_.graphics.beginFill(itemIndex == 2 ? 
				RADIOBOX_COLOR_SELECTED : RADIOBOX_COLOR_UNSELECTED);
			buttonSelect2_.graphics.drawRect(0, 0, button1_.width + 20, button1_.height + 20);
			buttonSelect2_.graphics.endFill();
			buttonSelect2_.x = button2_.x - 10;
			buttonSelect2_.y = button2_.y - 10;
		}
		
		public override function hide():void {
			StageUtil.getSingleton().removeFromStage(button1_);
			StageUtil.getSingleton().removeFromStage(buttonSelect1_);
			StageUtil.getSingleton().removeFromStage(button2_);
			StageUtil.getSingleton().removeFromStage(buttonSelect2_);
		}
	}
}
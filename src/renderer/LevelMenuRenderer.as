package renderer
{
import core.EngineUtil;
import core.GameConfig;
import core.IFrameUpdateObject;
import core.ImageLoader;
import core.StageUtil;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.utils.Dictionary;

public class LevelMenuRenderer implements IFrameUpdateObject, IForegroundRenderer
{
	public static const MENU_INDEX_SETUP:int = -1;
	
	private var backgroundRenderer_:BackgroundRenderer;
	private var numItems_:int;
	private var levelNameTexts_:Dictionary;
	private var buttonSprites_:Dictionary;
	private var menuSelectedCallback_:Function;
	private var setupBitmap_:Bitmap;
	private var setupButton_:Sprite;
	
	public function LevelMenuRenderer(backgroundRenderer:BackgroundRenderer)
	{
		backgroundRenderer_ = backgroundRenderer;
		numItems_ = 0;
		levelNameTexts_ = new Dictionary();
		buttonSprites_ = new Dictionary();
		setupButton_ = new Sprite();
		setupBitmap_ = null;
		if (StageUtil.getSingleton().userInput_.touchPointSupported) {
			setupButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				onMenuSelected(setupButton_);
			});
		} else {
			setupButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				onMenuSelected(setupButton_);
			});
		}
	}
	
	public function setMenuSelectedCallback(menuSelectedCallback:Function):void {
		menuSelectedCallback_ = menuSelectedCallback;
	}

	public function show(numItems:int = EngineUtil.IntNaN):void {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		if (!setupBitmap_) {
			setupBitmap_ = ImageLoader.getSingleton().getBitmap("level_button_setup_72.png");
			setupButton_.addChild(setupBitmap_);
		}
		if (numItems == EngineUtil.IntNaN) {
			if (numItems_ == 0) {
				throw new Error();
			}
		} else {
			numItems_ = numItems;
		}
		backgroundRenderer_.show(BackgroundRenderer.TYPE_LEVEL_MENU);
		
		var menuMarginLeft:int = levelWidth * 2 / 10; 
		var buttonWidth:int = (levelWidth - 2 * menuMarginLeft) / numItems_ * 6 / 10; 
		var buttonPaddingLeft:int = (levelWidth - 2 * menuMarginLeft) / numItems_ * 2 / 10;	// (10 - 6) / 2
		var menuMarginTop:int = levelHeight * 3 / 10;
		var buttonHeight:int = buttonWidth;
		
		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "Verdana";
		textFormat.size = 16;
		textFormat.color = 0xFFFFFF;

		var i:int;
		for (i = 0; i < numItems_; i++) {
			var levelNameText:TextField = new TextField();
			levelNameTexts_[i] = levelNameText;
			levelNameText.defaultTextFormat = textFormat;
			levelNameText.autoSize = TextFieldAutoSize.LEFT;
			levelNameText.text = "<" + (i + 1) + ">";
			levelNameText.x = buttonWidth / 2 - levelNameText.width / 2;
			levelNameText.y = buttonHeight / 2 - levelNameText.height / 2;
			
			var buttonSprite:Sprite = new Sprite();
			buttonSprites_[i] = buttonSprite;
			buttonSprite.graphics.clear();
			buttonSprite.graphics.beginFill(0x111111, 0.5);
			buttonSprite.graphics.lineStyle(2, 0x111111);
			buttonSprite.graphics.drawRect(0, 0, buttonWidth, buttonHeight);
			buttonSprite.graphics.endFill();
			buttonSprite.cacheAsBitmap = true;
			buttonSprite.addChild(levelNameText);
			buttonSprite.x = menuMarginLeft + (buttonWidth + 2 * buttonPaddingLeft) * i + buttonPaddingLeft;
			buttonSprite.y = menuMarginTop;
			if (StageUtil.getSingleton().userInput_.touchPointSupported) {
				buttonSprite.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
					var target:Object = event.target;
					if (target is TextField) {
						target = target.parent;
					}
					onMenuSelected(target);
				});
			} else {
				buttonSprite.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
					var target:Object = event.target;
					if (target is TextField) {
						target = target.parent;
					}
					onMenuSelected(target);
				});
			}
		}
		for (i = 0; i < numItems_; i++) {
			StageUtil.getSingleton().addToStage(buttonSprites_[i]);
		}
		
		showSetupButton(levelWidth, levelHeight);
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	// interface impl
	public function hide():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this); 
		hideSetupButton();
		for (var i:int = 0; i < numItems_; i++) {
			StageUtil.getSingleton().removeFromStage(buttonSprites_[i]);
			delete buttonSprites_[i];
		}
	}
	
	private function onMenuSelected(selectedButton:Object):void {
		if (!buttonSprites_.hasOwnProperty(0)) {
			return; // for keyboard input case, not to crash
		}
		var menuIndex:int = MENU_INDEX_SETUP;
		for (var i:int = 0; i < numItems_; i++) {
			if (selectedButton == buttonSprites_[i]) {
				menuIndex = i;
			}
		}
		menuSelectedCallback_(menuIndex);
		hide();
	}

	private function showSetupButton(levelWidth:int, levelHeight:int):void {
		var gap:int = levelHeight * GameConfig.BUTTON_GAP_HEIGHT_PERCENT / 100;
		setupButton_.width = levelHeight * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100;
		setupButton_.height = levelHeight * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100;
		setupButton_.x = levelWidth - (gap + setupButton_.width);
		setupButton_.y = gap;
		StageUtil.getSingleton().addToStage(setupButton_);
	}
	
	public function hideSetupButton():void {
		StageUtil.getSingleton().removeFromStage(setupButton_);
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		if (numItems_ > 0) {
			if ((StageUtil.getSingleton().userInput_.keyDownCharCode_ >= 49) &&	// 1 ~ keys
				(StageUtil.getSingleton().userInput_.keyDownCharCode_ <= 49 + numItems_)) {
				onMenuSelected(buttonSprites_[StageUtil.getSingleton().userInput_.keyDownCharCode_ - 49]);
			} else if (StageUtil.getSingleton().userInput_.keyDownCharCode_ == 115) { // S key for setup
				onMenuSelected(setupButton_);
			}
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "level_menu_renderer";
	}	
}
}
package renderer
{
import core.EngineUtil;
import core.GameConfig;
import core.IFrameUpdateObject;
import core.StageUtil;

import flash.display.Sprite;

import simulation.SimBall;
import simulation.SimWorld;

public class BallRenderer implements IFrameUpdateObject
{
	private var simWorld_:SimWorld;
	private var simBall_:SimBall;
	private var ball_:Sprite;
	private var ballRadius_:int;
	private var ballRadiusMag_:int;
	private var lastPosX_:int;
	private var lastPosY_:int;
	private static const BALL_COLOR_REAL:int = 0xEEEEEE;
	private static const BALL_COLOR_MAG:int = 0x999999;
	private const TRACE_DEBUG_MSG:Boolean = true;
	
	public function BallRenderer(simWorld:SimWorld) {
		simWorld_ = simWorld;
		simBall_ = simWorld.simBall_;
		ball_ = new Sprite();
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		if (isShown()) {
			if (!simWorld_.states_.isStateActive(SimWorld.STATE_SIMULATING)) {
				hide();
			} else {
				moveBall();
			}
		} else {
			if (simWorld_.states_.isStateActive(SimWorld.STATE_SIMULATING)) {
				show();
				if (TRACE_DEBUG_MSG) {
					trace("I show ball, pos: " + EngineUtil.roundBrief(simWorld_.simBall_.posX_) + 
						", " + EngineUtil.roundBrief(simWorld_.simBall_.posY_) + " at frame number " + 
						frameNumber);
				}
			}
		}
		
		return true;
	}
	
	private function show():void {
		var ballRadiusReal:Number = simWorld_.worldToScreen(SimBall.BALL_RADIUS_WORLD); 
		ballRadius_ = ballRadiusReal *  
			GameConfig.getSingleton().getValue(GameConfig.BALL_DRAW_MAGNITUDE);
		ball_.graphics.clear();
		ball_.graphics.beginFill(GameConfig.getSingleton().getValue(GameConfig.BALL_DRAW_MAGNITUDE) == 1 ? 
			BALL_COLOR_REAL : BALL_COLOR_MAG);
		ball_.graphics.drawCircle(ballRadius_, ballRadius_, ballRadius_);
		ball_.graphics.endFill();
		if (GameConfig.getSingleton().getValue(GameConfig.BALL_DRAW_MAGNITUDE) > 1) {
			ball_.graphics.beginFill(BALL_COLOR_REAL);
			ball_.graphics.drawCircle(ballRadius_, ballRadius_, ballRadiusReal);
			ball_.graphics.endFill();
		}
		StageUtil.getSingleton().addToStage(ball_);
		lastPosX_ = -1;
	}

	private function isShown():Boolean {
		return !!ball_.parent;
	}
	
	private function hide():void {
		trace("I hide ball");
		StageUtil.getSingleton().removeFromStage(ball_);
	}
	
	public function moveBall():void {
		var x:int;
		var y:int;
		if (isNaN(simBall_.posFrameX_)) {
			x = simWorld_.worldToScreenX(simBall_.posX_);
			y = simWorld_.worldToScreenY(simBall_.posY_);
		} else {
			x = simWorld_.worldToScreenX(simBall_.posFrameX_);	
			y = simWorld_.worldToScreenY(simBall_.posFrameY_);	
		}
		if ((x == lastPosX_) && (y == lastPosY_)) {
			return;
		}
		lastPosX_ = x;
		lastPosY_ = y;
		//StageUtil.getSingleton().infoText_.setText("render ball", "pos: " + x + ", " + y);
		ball_.x = x - ballRadius_ / 2;
		ball_.y = y - ballRadius_ / 2;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "ball_renderer";
	}
}
}
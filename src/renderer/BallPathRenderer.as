package renderer
{
import core.EngineUtil;
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;

import simulation.BallCfg;
import simulation.EstimatePath;
import simulation.SimWorld;

public class BallPathRenderer implements IFrameUpdateObject {
	private var simWorld_:SimWorld;
	private var estimatePath_:EstimatePath;
	private var path_:Sprite;
	private var lastEstimateSerialNum_:int;
	private var state_:SimpleStateMachine;
	private const TRACE_DEBUG_MSG:Boolean = false;
	
	public function BallPathRenderer(simWorld:SimWorld) {
		simWorld_ = simWorld;
		path_ = new Sprite();
		state_ = new SimpleStateMachine([
			['hidden', 0],
			['showing', 1],
		]);
	}
	
	public function init(estimatePath:EstimatePath):void {
		estimatePath_ = estimatePath;
		StageUtil.getSingleton().addFrameUpdateObject(this);
		StageUtil.getSingleton().addToStage(path_, StageUtil.STAGE_OBJECT_GROUP_VISUAL);
		lastEstimateSerialNum_ = 0;
	}
	
	public function cleanUp():void {
		if (state_.isStateActive("showing")) {
			hidePaths();
			state_.setActiveState("hidden");
		}
		StageUtil.getSingleton().removeFromStage(path_);
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		
	}

	// interface impl 
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		var stateElapsedTimeSec:Number = state_.update(frameElapsedTime);
		if (state_.isStateActive("showing") && state_.isActiveStateExpired()) {
			hidePaths();
			state_.setActiveState("hidden");
		}
		
		if (estimatePath_.estimateSerialNum_ > lastEstimateSerialNum_) {
			if (state_.isStateActive("showing")) {
				hidePaths();
			} else {
				state_.setActiveState("showing");
			}
			drawPaths();
			lastEstimateSerialNum_ = estimatePath_.estimateSerialNum_; 
		}
		return true;
	}
	
	private function drawPaths():void {
		if (TRACE_DEBUG_MSG) {
			trace("I draw path...");
		}
		path_.graphics.clear();
		path_.graphics.lineStyle(1, 0x223344, 0.3);
		var firstCfg:BallCfg = estimatePath_.estimatedCfgs_[0];
		var firstPosX:int = simWorld_.worldToScreenX(firstCfg.posX_); 
		var firstPosY:int = simWorld_.worldToScreenY(firstCfg.posY_); 
		path_.graphics.moveTo(0, 0);
		if (TRACE_DEBUG_MSG) {
			trace("I   path origin: " + EngineUtil.roundBrief(firstPosX) + ", " + EngineUtil.roundBrief(firstPosY));
		}
		var notchHeight:int = simWorld_.worldToScreenY(0.07, true);
		for (var simStep:int = 1; simStep < estimatePath_.numSimSteps_; simStep++) {
			var cfg2:BallCfg = estimatePath_.estimatedCfgs_[simStep];
			var pos2X:Number = simWorld_.worldToScreenX(cfg2.posX_) - firstPosX;
			var pos2Y:Number = simWorld_.worldToScreenY(cfg2.posY_) - firstPosY;
			path_.graphics.lineTo(pos2X, pos2Y);
			path_.graphics.moveTo(pos2X, pos2Y + notchHeight);
			path_.graphics.lineTo(pos2X, pos2Y - notchHeight);
			path_.graphics.moveTo(pos2X, pos2Y);
			if (TRACE_DEBUG_MSG) {
				trace("I   line to " + EngineUtil.roundBrief(pos2X) + ", " + EngineUtil.roundBrief(pos2Y));
			}
		}
		path_.graphics.endFill();
		path_.x = firstPosX; 
		path_.y = firstPosY;
	}
	
	private function hidePaths():void {
		path_.graphics.clear();
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "path_renderer";	
	}
}
}
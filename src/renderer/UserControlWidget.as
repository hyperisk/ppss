package renderer
{
import core.EngineUtil;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.utils.getTimer;

import level.IPaddleEventGenerator;
import level.PaddleEvent;

import simulation.SimBall;
import simulation.SimTable;
import simulation.SimWorld;

public class UserControlWidget implements IPaddleEventGenerator
{
	public static const INPUT_AREA_HORZ_MARGIN_PERCENT:int = 5;	// between widget bound and table edge & screen edge
	public static const INPUT_AREA_HEIGHT_TABLE_HEIGHT_RATIO:Number = 1.8;
	public static const INPUT_AREA_MARGIN_PERCENT:Number = 7;
	
	private var side_:String;
	private var simWorld_:SimWorld;
	private var simInputEventDispatcher_:EventDispatcher;
	
	private var inputWidget_:Sprite;
	private var widgetWidth_:int;
	private var widgetHeight_:int;
	private var lastPosX_:int;
	private var lastPosY_:int;
	private var lastPosTimeMsec_:int;
	private var mouseStates_:SimpleStateMachine;
	private const TRACE_DEBUG_MSG:Boolean = false;
	
	public function UserControlWidget(simWorld:SimWorld, side:String) {
		simWorld_ = simWorld;
		side_ = side;
		simInputEventDispatcher_ = new EventDispatcher();
		inputWidget_ = new Sprite();
		mouseStates_ = new SimpleStateMachine([
			['ignore', 0],
			['control', 0],
		]);
		
		if (StageUtil.getSingleton().userInput_.touchPointSupported) {
			inputWidget_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_SIM_INPUT);
			});
			inputWidget_.addEventListener(TouchEvent.TOUCH_MOVE, function(event:TouchEvent):void {
				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_SIM_INPUT);
			});
//			inputWidget_.addEventListener(TouchEvent.TOUCH_OUT, function(event:TouchEvent):void {
//				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_UP);
//			});
//			inputWidget_.addEventListener(TouchEvent.TOUCH_END, function(event:TouchEvent):void {
//				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_UP);
//			});
		} else {
			inputWidget_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				mouseStates_.setActiveState("control");
				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_SIM_INPUT);
			});
			inputWidget_.addEventListener(MouseEvent.MOUSE_MOVE, function(event:MouseEvent):void {
				if (mouseStates_.isStateActive("control")) {
					onInputAction(event.localX, event.localY, PaddleEvent.ACTION_SIM_INPUT);
				}
			});
			inputWidget_.addEventListener(MouseEvent.MOUSE_OUT, function(event:MouseEvent):void {
				// don't do anything
			});
//			inputWidget_.addEventListener(MouseEvent.MOUSE_UP, function(event:MouseEvent):void {
//				mouseStates_.setActiveState("ignore");
//				onInputAction(event.localX, event.localY, PaddleEvent.ACTION_UP);
//			});
		}
	}
	
	public function show(tableRenderer:TableRenderer):void {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		
		var spaceX:int = levelWidth / 2 - tableRenderer.tableTopWidth_ / 2;
		var marginX:int = spaceX * INPUT_AREA_HORZ_MARGIN_PERCENT / 100;
		widgetWidth_ = spaceX - marginX * 2;
		widgetHeight_ = tableRenderer.tableLegHeight_ * INPUT_AREA_HEIGHT_TABLE_HEIGHT_RATIO;
		
		inputWidget_.graphics.clear();
		inputWidget_.graphics.lineStyle(1, 0, 0);
		inputWidget_.graphics.beginFill(0x777777, 0.1);
		inputWidget_.graphics.drawRoundRect(0, 0, widgetWidth_, widgetHeight_, 15, 15);
		inputWidget_.graphics.lineStyle(1, 0xCCCCCC, 0.2);
		inputWidget_.graphics.beginFill(0, 0);		
		inputWidget_.graphics.drawRoundRect(0, 0, widgetWidth_, widgetHeight_, 15, 15);
		inputWidget_.graphics.endFill();
		inputWidget_.cacheAsBitmap = true;
		
		if (side_ == SimBall.LEFT_SIDE) {
			inputWidget_.x = marginX;
		} else {
			inputWidget_.x = levelWidth - widgetWidth_ - marginX;
		}
		inputWidget_.y = tableRenderer.tableTopPosY_ - 
			simWorld_.worldToScreenY(SimTable.TABLE_TOP_PLAY_POS_DIST, true) - widgetHeight_ / 2;

		StageUtil.getSingleton().addToStage(inputWidget_, StageUtil.STAGE_OBJECT_GROUP_USERINPUT);
	}
	
	public function hide():void {
		StageUtil.getSingleton().removeFromStage(inputWidget_);
	}
	
	private function onInputAction(x:int, y:int, action:String):void {
		var xNorm:Number = x / widgetWidth_;
		xNorm = xNorm < 0 ? 0 : xNorm;
		xNorm = xNorm > 1 ? 1 : xNorm;
		var yNorm:Number = 1 - y  / widgetHeight_;
		yNorm = yNorm < 0 ? 0 : yNorm;
		yNorm = yNorm > 1 ? 1 : yNorm;
		var newPosX:int = x + inputWidget_.x;
		var newPosY:int = y + inputWidget_.y;
		var elapsedTimeMsec:int = getTimer() - lastPosTimeMsec_;
//		StageUtil.getSingleton().infoText_.setText("input " + side_, action + ", pos screen: " +
//			EngineUtil.roundBrief(newPosX) + ", " + EngineUtil.roundBrief(newPosY) + 
//			", pos norm:" + EngineUtil.roundBrief(xNorm) + ", " + EngineUtil.roundBrief(yNorm) + 
//			", timerMsec: " + EngineUtil.roundBrief(elapsedTimeMsec));
		if ((newPosX != lastPosX_) || (newPosY != lastPosY_)) {
			var eventPosX:Number = simWorld_.screenToWorldX(newPosX); 
			var eventPosY:Number = simWorld_.screenToWorldY(newPosY); 
			if (TRACE_DEBUG_MSG) {
				trace("I dispatch paddle event from user, pos: " + EngineUtil.roundBrief(eventPosX) + ", " + 
					EngineUtil.roundBrief(eventPosY) + " action: " + action);
			}
			simInputEventDispatcher_.dispatchEvent(new PaddleEvent(action, false, false, eventPosX, eventPosY));  
			lastPosX_ = newPosX;
			lastPosY_ = newPosY;
			lastPosTimeMsec_ = getTimer();
		}
	}
	
	// interface impl
	public function addEventListener(type:String, listner:Function):void {
		simInputEventDispatcher_.addEventListener(type, listner);
	}
	
	// interface impl
	public function removeEventListener(type:String, listner:Function):void {
		simInputEventDispatcher_.removeEventListener(type, listner);
	}
}
}
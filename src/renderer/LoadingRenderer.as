package renderer
{
import core.EngineUtil;
import core.GameConfig;
import core.IFrameUpdateObject;
import core.StageUtil;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class LoadingRenderer implements IFrameUpdateObject, IForegroundRenderer
{
	private var loadingText_:TextField;
	private var statusText_:TextField;
	
	public function LoadingRenderer() {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		
		loadingText_ = new TextField();
		
		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "Verdana";
		textFormat.size = 24;
		
		loadingText_.defaultTextFormat = textFormat;
		loadingText_.autoSize = TextFieldAutoSize.LEFT;
		loadingText_.text = "Loading...";
		loadingText_.x = levelWidth / 2 - loadingText_.width / 2;
		loadingText_.y = levelHeight * 1 / 2;
		
		statusText_ = new TextField();
		statusText_.autoSize = TextFieldAutoSize.LEFT;
		statusText_.text = "0%";
		statusText_.x = levelWidth / 2 - statusText_.width;
		statusText_.y = levelHeight * 3 / 5;
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		return true;
	}
	
	public function updateStatus(percent:int):void {
		statusText_.text = "" + EngineUtil.roundBrief(percent) + "%";
	}
	
	public function show():void {
		StageUtil.getSingleton().addToStage(loadingText_);
		StageUtil.getSingleton().addToStage(statusText_);
	}
	
	// interface impl
	public function hide():void {
		StageUtil.getSingleton().removeFromStage(loadingText_);
		StageUtil.getSingleton().removeFromStage(statusText_);
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "loading_renderer";
	}
}
}
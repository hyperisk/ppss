package renderer
{
import core.EngineUtil;
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;

import simulation.Collision;
import simulation.SimBall;
import simulation.SimPaddleBase;
import simulation.SimPaddleUser;
import simulation.SimWorld;

public class BallStatRenderer implements IFrameUpdateObject
{
	private static const BALL_DIAMETER_HEIGHT_PERCENT:int = 5;
	private static const BALL_MARGIN_TABLE_TOP_PERCENT:int = 50;
	private static const SPEED_BALL_MARGIN_PERCENT:int = 80;
	private static const SPEED_BALL_WIDTH_MAX_PERCENT:int = 500;
	private static const SPEED_BALL_TIP_PERCENT:int = 50;
	private static const SPIN_BALL_MARGIN_PERCENT:int = 150;
	private static const SPIN_BALL_DEG_PER_RPS:int = 90 / SimBall.BALL_MAX_SPIN;
	private static const ARROW_HEAD_ANGLE:int = 30;
	private static const BALL_AND_SPEED_COLOR:int = 0x333333;
	private var simWorld_:SimWorld;
	private var ballShape_:Sprite;
	private var speedShape_:Sprite;
	private var spinShape_:Sprite;
	private var ballHeight_:int;
	private var state_:SimpleStateMachine;
	
	public function BallStatRenderer(simWorld:SimWorld) {
		simWorld_ = simWorld;
		ballShape_ = new Sprite();
		speedShape_ = new Sprite();
		spinShape_ = new Sprite();
		state_ = new SimpleStateMachine([
			['hidden', 0],
			['showingForLeft', 2],
			['showingForRight', 2]
		]);
		ballHeight_ = 0;
	}

	public function init(tableRenderer:TableRenderer):void {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		ballHeight_ = levelHeight * BALL_DIAMETER_HEIGHT_PERCENT / 100;
		StageUtil.getSingleton().addFrameUpdateObject(this);
		ballShape_.graphics.clear();
		ballShape_.graphics.beginFill(BALL_AND_SPEED_COLOR);
		ballShape_.graphics.drawCircle(ballHeight_ / 2, ballHeight_ / 2, ballHeight_ / 2);
		ballShape_.graphics.endFill();
		ballShape_.cacheAsBitmap = true;
	}
	
	public function cleanUp():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
	}
	
	public function show():void {
		var posX:int = simWorld_.worldToScreenX(simWorld_.simBall_.posAtCollX_);
		var posY:int = simWorld_.worldToScreenY(simWorld_.simBall_.posAtCollY_);
		
		ballShape_.x = posX - ballHeight_ / 2;
		ballShape_.y = posY - ballHeight_ / 2;
		ballShape_.alpha = 1
		StageUtil.getSingleton().addToStage(ballShape_, StageUtil.STAGE_OBJECT_GROUP_VISUAL);
		
		speedShape_.graphics.clear();
		speedShape_.alpha = 1
		speedShape_.graphics.lineStyle(4, BALL_AND_SPEED_COLOR);
		var speedMarginLen:Number = ballHeight_ * SPEED_BALL_MARGIN_PERCENT / 100;
		var s1x:int = posX + simWorld_.simBall_.getVelUnitX() * speedMarginLen;
		var s1y:int = posY - simWorld_.simBall_.getVelUnitY() * speedMarginLen;
		speedShape_.graphics.moveTo(s1x, s1y);
		var speedVecLen:Number = simWorld_.simBall_.getSpeed() / SimBall.BALL_MAX_SPEED *
			ballHeight_ * SPEED_BALL_WIDTH_MAX_PERCENT / 100
		var s2x:int = s1x + simWorld_.simBall_.getVelUnitX() * speedVecLen;
		var s2y:int = s1y - simWorld_.simBall_.getVelUnitY() * speedVecLen;
		speedShape_.graphics.lineTo(s2x, s2y);
		var ballVelX:Number = simWorld_.simBall_.getSpeed() * simWorld_.simBall_.getVelUnitX();
		var ballVelY:Number = -1 * simWorld_.simBall_.getSpeed() * simWorld_.simBall_.getVelUnitY();
		var velAngleDeg:Number = Math.atan2(ballVelY, ballVelX) * 180 / Math.PI
		var headAngle1:Number = (velAngleDeg - 180 - ARROW_HEAD_ANGLE) * Math.PI / 180; 
		var headAngle2:Number = (velAngleDeg - 180 + ARROW_HEAD_ANGLE) * Math.PI / 180;
		var headLen:Number = ballHeight_ * SPEED_BALL_TIP_PERCENT / 100;
		headLen = Math.min(headLen, speedVecLen / 2);
		speedShape_.graphics.lineTo(s2x + headLen * Math.cos(headAngle1), s2y + headLen * Math.sin(headAngle1));
		speedShape_.graphics.moveTo(s2x, s2y);
		speedShape_.graphics.lineTo(s2x + headLen * Math.cos(headAngle2), s2y + headLen * Math.sin(headAngle2));
		StageUtil.getSingleton().addToStage(speedShape_, StageUtil.STAGE_OBJECT_GROUP_VISUAL);
		
		spinShape_.graphics.clear();
		spinShape_.alpha = 1;
		spinShape_.graphics.lineStyle(4, BALL_AND_SPEED_COLOR);
		var curveAngleDeg:Number = simWorld_.simBall_.spin_ * 
			-1 * simWorld_.simBall_.getVelSignX() * SPIN_BALL_DEG_PER_RPS;
		curveAngleDeg = Math.min(90, curveAngleDeg);
		curveAngleDeg = Math.max(-90, curveAngleDeg);
		var curveAngleRad:Number = curveAngleDeg * Math.PI / 180;
		var spinMargin:Number = ballHeight_ * SPIN_BALL_MARGIN_PERCENT / 100;
		var s1xd:Number = spinMargin * simWorld_.simBall_.getVelSignX();
		var curveLen:Number = spinMargin * curveAngleRad;
		headLen = Math.min(headLen, curveLen / 2);
		s1x = posX - s1xd;
		s1y = posY;
		EngineUtil.rotateVec(s1xd, 0, curveAngleRad);
		s2x = posX - EngineUtil.t1_;
		s2y = posY + EngineUtil.t2_;
		spinShape_.graphics.moveTo(s1x, s1y);
		var AB:Number = s1xd / Math.cos(curveAngleRad / 2);
		EngineUtil.rotateVec(AB, 0, curveAngleRad / 2);
		var c1x:int = posX - EngineUtil.t1_;
		var c1y:int = posY + EngineUtil.t2_;
		spinShape_.graphics.curveTo(c1x, c1y, s2x, s2y);
		if (simWorld_.simBall_.getVelSignX() > 0) {
			headAngle1 = (-curveAngleDeg - 90 - ARROW_HEAD_ANGLE) * Math.PI / 180;
			headAngle2 = (-curveAngleDeg - 90 + ARROW_HEAD_ANGLE) * Math.PI / 180;
		} else {
			headAngle1 = (-curveAngleDeg + 90 - ARROW_HEAD_ANGLE) * Math.PI / 180;
			headAngle2 = (-curveAngleDeg + 90 + ARROW_HEAD_ANGLE) * Math.PI / 180;
		}
		speedShape_.graphics.moveTo(s2x, s2y);
		speedShape_.graphics.lineTo(s2x + headLen * Math.cos(headAngle1), s2y + headLen * Math.sin(headAngle1));
		speedShape_.graphics.moveTo(s2x, s2y);
		speedShape_.graphics.lineTo(s2x + headLen * Math.cos(headAngle2), s2y + headLen * Math.sin(headAngle2));
		
		
		spinShape_.graphics.lineStyle(2, BALL_AND_SPEED_COLOR);
		spinShape_.graphics.moveTo(posX - 1.2 * s1xd, s1y);
		spinShape_.graphics.lineTo(posX, posY);
		
		StageUtil.getSingleton().addToStage(spinShape_, StageUtil.STAGE_OBJECT_GROUP_VISUAL);
	}
	
	private function updateShown(frameElapsedTime:Number):void {
		var stateElapsedTimeSec:Number = state_.update(frameElapsedTime);
		if (state_.isActiveStateExpired()) {
			hide();
			state_.setActiveState("hidden");
		} else {
			var newAlpha:Number = 1 - stateElapsedTimeSec / state_.getActiveStateTimeLen(); 
			ballShape_.alpha = newAlpha; 
			speedShape_.alpha = newAlpha; 
			spinShape_.alpha = newAlpha;
		}
	}
	
	public function hide():void {
		StageUtil.getSingleton().removeFromStage(ballShape_);
		StageUtil.getSingleton().removeFromStage(speedShape_);
		StageUtil.getSingleton().removeFromStage(spinShape_);
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		var simPaddleLeftInit:SimPaddleBase = simWorld_.getSimPaddleInitialized(SimBall.LEFT_SIDE);
		var simPaddleRightInit:SimPaddleBase = simWorld_.getSimPaddleInitialized(SimBall.RIGHT_SIDE);
		if (simPaddleLeftInit && 
			simPaddleLeftInit.ballProximity_.isStateActive(SimPaddleBase.BALL_PROXIMITY_HIT) && 
			!state_.isStateActive("showingForLeft")) {
			if (!state_.isStateActive("hidden")) {
				hide();
			}
			state_.setActiveState("showingForLeft");
			show();
		} else if (simPaddleRightInit && 
			simPaddleRightInit.ballProximity_.isStateActive(SimPaddleBase.BALL_PROXIMITY_HIT) &&
			!state_.isStateActive("showingForRight")) {
			if (!state_.isStateActive("hidden")) {
				hide();
			}
			state_.setActiveState("showingForRight");
			show();
		} else if (!state_.isStateActive("hidden")) {
			updateShown(frameElapsedTime);
		}	
		return true;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "ball_stat_renderer";
	}
}
}
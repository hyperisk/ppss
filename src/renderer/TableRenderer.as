package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.StageUtil;

import flash.display.GradientType;
import flash.display.Sprite;
import flash.geom.Matrix;

import simulation.SimTable;

public class TableRenderer implements IFrameUpdateObject
{
	private var table_:Sprite;
	private var tableShadow_:Sprite;

	public static const TABLE_WIDTH_PERCENT_MIN:int = 40;
	public static const TABLE_WIDTH_PERCENT_MAX:int = 50;
	public static const TABLE_HEIGHT_PERCENT_MIN:int = 25;
	public static const TABLE_HEIGHT_PERCENT_MAX:int = 45;
	public var tableWidthPercent_:int;
	public var tableTopWidth_:int;
	public var tableTopPosY_:int
	public var tableTopThickness_:int; // note: tableTopThickness is for rendering only. TableTop = TableLegHeight (do not add thickness)
	public var tableLegHeight_:int;
	
	public function TableRenderer()
	{
		tableShadow_ = new Sprite();
		table_ = new Sprite();
		StageUtil.getSingleton().addFrameUpdateObject(this);
		tableTopWidth_ = 0;
	}
	
	public function show(tableWidthPercent:int, tableHeightPercent:int):void {
		if (tableWidthPercent < TABLE_WIDTH_PERCENT_MIN) {
			throw new Error();
		}
		if (tableWidthPercent > TABLE_WIDTH_PERCENT_MAX) {
			throw new Error();
		}
		if (tableHeightPercent < TABLE_HEIGHT_PERCENT_MIN) {
			throw new Error();
		}
		if (tableHeightPercent > TABLE_HEIGHT_PERCENT_MAX) {
			throw new Error();
		}
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		tableWidthPercent_ = tableWidthPercent;
		tableTopWidth_ = levelWidth * tableWidthPercent / 100;
		tableTopPosY_ = levelHeight * (100 - tableHeightPercent) / 100;
		tableTopThickness_ = levelHeight * 3 / 100;
		var tableLegOffsetX:int = tableTopWidth_ * 2 / 10;
		tableLegHeight_ = tableTopWidth_ * SimTable.TABLE_LEG_HEIGHT / SimTable.TABLE_TOP_WIDTH;
		var netHeight:int = tableTopWidth_ * SimTable.TABLE_NET_HEIGHT / SimTable.TABLE_TOP_WIDTH;

		trace("I show table, tableTopWidth_: " + tableTopWidth_ + ", tableTopPosY_: " + tableTopPosY_ + 
			", tableLegHeight_: " + tableLegHeight_);
		var shadowWidth:int = levelWidth * 2;
		var shadowHeight:int = levelHeight - tableTopPosY_ - tableLegHeight_;
		var colors:Array = [0x555555, 0x777777, 0xAAAAAA];
		var alphas:Array = [1, 1, 0];
		var ratios:Array = [0, 180, 255];
		var mat:Matrix = new Matrix();
		mat.createGradientBox(shadowWidth, shadowHeight, Math.PI/2);
		tableShadow_.graphics.clear();
		tableShadow_.graphics.beginGradientFill(GradientType.RADIAL, colors, alphas, ratios, mat);
		tableShadow_.graphics.drawEllipse(0, 0, shadowWidth, shadowHeight);
		tableShadow_.graphics.endFill();
		tableShadow_.x = levelWidth / 2 - shadowWidth / 2;
		tableShadow_.y = levelHeight - shadowHeight ;
		tableShadow_.cacheAsBitmap = true;

		// draw table top
		table_.graphics.clear();
		table_.graphics.lineStyle(2 /* thickness */, 0x3A5A9F /* color */);
		table_.graphics.beginFill(0x335599 /* color */);
		table_.graphics.drawRoundRect(0, netHeight, tableTopWidth_, tableTopThickness_, 2, 2);
		table_.graphics.endFill();
		
		// legs
		table_.graphics.lineStyle(1 /* thickness */, 0x2A4A7F /* color */);
		table_.graphics.beginFill(0x224477 /* color */);
		table_.graphics.drawRect(tableLegOffsetX, tableTopThickness_ + netHeight, 
			tableTopThickness_, tableLegHeight_ - tableTopThickness_);
		table_.graphics.drawRect(tableTopWidth_ - tableLegOffsetX - tableTopThickness_, tableTopThickness_ + netHeight, 
			tableTopThickness_, tableLegHeight_ - tableTopThickness_);
		table_.graphics.endFill();
		
		// net
		table_.graphics.lineStyle(1 /* thickness */, 0xFFFFFF /* color */);
		table_.graphics.beginFill(0xCCCCCC /* color */);
		table_.graphics.drawRect(tableTopWidth_ / 2 - 2, 0, 3, netHeight);

		table_.x = levelWidth / 2 - tableTopWidth_ / 2;
		table_.y = tableTopPosY_ - netHeight;
		table_.cacheAsBitmap = true;
		
		StageUtil.getSingleton().addToStage(tableShadow_);
		StageUtil.getSingleton().addToStage(table_);
	}
	
	public function hide():void {
		StageUtil.getSingleton().removeFromStage(tableShadow_);
		StageUtil.getSingleton().removeFromStage(table_);
		tableTopWidth_ = 0;
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean
	{
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "TableRenderer";
	}		
}
}
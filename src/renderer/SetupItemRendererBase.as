package renderer
{
	import flash.utils.Dictionary;

	public class SetupItemRendererBase
	{
		public const ITEM_TYPE_RADIO:String = "item_type_radio";
		
		public var itemChangeLock_:Boolean;	// wait until DB transaction is complete
		protected var itemName_:String;
		protected var itemData_:Dictionary;
		protected var dialogWidth_:int;
		protected var dialogHeight_:int;
		
		public function SetupItemRendererBase(itemName:String, itemData:Dictionary) {
			itemName_ = itemName;
			itemData_ = itemData;
		}
		
		public function show(dialogWidth:int, dialogHeight:int):void {
			throw new Error("did not overrider show dialog item");
		}
		
		public function hide():void {
			throw new Error("did not overrider hide dialog item");
		}
	}
}
package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.ImageLoader;
import core.StageUtil;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;
import flash.utils.setInterval;
import flash.utils.setTimeout;

public class MainMenuRenderer implements IFrameUpdateObject, IForegroundRenderer
{
	private var backgroundRenderer_:BackgroundRenderer;
	private var titleText_:TextField;
	private var textFormat_:TextFormat;
	private var startBitmap_:Bitmap;
	private var startButton_:Sprite;
	private var helpBitmap_:Bitmap;
	private var helpButton_:Sprite;
	private var menuSelectedCallback_:Function;
	
	public static const MENU_ITEM_START_GAME:String = "menu_start_game";
	public static const MENU_ITEM_HELP:String = "menu_help";
	
	public function MainMenuRenderer(backgroundRenderer:BackgroundRenderer) {
		backgroundRenderer_ = backgroundRenderer;
	}
	
	public function setMenuSelectedCallback(menuSelectedCallback:Function):void {
		menuSelectedCallback_ = menuSelectedCallback;
	}
	
	public function show():void {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		backgroundRenderer_.show(BackgroundRenderer.TYPE_MAIN_MENU);
		
		titleText_ = new TextField();

		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "Verdana";
		textFormat.size = 24;
		
		titleText_.defaultTextFormat = textFormat;
		titleText_.autoSize = TextFieldAutoSize.LEFT;
		titleText_.text = GameConfig.GAME_TITLE_WITH_SPACE;
		titleText_.x = levelWidth / 2 - titleText_.width / 2;
		titleText_.y = levelHeight * 3 / 10;
		StageUtil.getSingleton().addToStage(titleText_);
		
		startBitmap_ = ImageLoader.getSingleton().getBitmap("mainmenu_button_start_72.png");
		startButton_ = new Sprite();
		startButton_.addChild(startBitmap_);
		startButton_.x = levelWidth * 4 / 10 - startBitmap_.width / 2;
		startButton_.y = levelHeight * 6 / 10;
		StageUtil.getSingleton().addToStage(startButton_);
		
		helpBitmap_ = ImageLoader.getSingleton().getBitmap("mainmenu_button_help_72.png");
		helpButton_ = new Sprite();
		helpButton_.addChild(helpBitmap_);
		helpButton_.x = levelWidth * 6 / 10 - helpBitmap_.width / 2;
		helpButton_.y = levelHeight * 6 / 10;
		StageUtil.getSingleton().addToStage(helpButton_);
		
		if (StageUtil.getSingleton().userInput_.touchPointSupported) {
			startButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				onMenuSelected(MENU_ITEM_START_GAME);
			});
			helpButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				onMenuSelected(MENU_ITEM_HELP);
			});
		} else {
			startButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				onMenuSelected(MENU_ITEM_START_GAME);
			});
			helpButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				onMenuSelected(MENU_ITEM_HELP);
			});
		}
		
		if (GameConfig.MAIN_MENU_AUTO_SELECT) {
			setTimeout(function():void {
				onMenuSelected(MENU_ITEM_START_GAME);
			}, 500);
		}
		
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	// interface impl
	public function hide():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this); 
		StageUtil.getSingleton().removeFromStage(titleText_);
		StageUtil.getSingleton().removeFromStage(startButton_);
		StageUtil.getSingleton().removeFromStage(helpButton_);
	}
	
	private function onMenuSelected(itemSelected:String):void {
		hide();
		if (itemSelected) {
			menuSelectedCallback_(itemSelected);
		}
	}

	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		if (StageUtil.getSingleton().userInput_.keyDownCharCode_ == 112) { // p key
			onMenuSelected(MENU_ITEM_START_GAME);
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "main_menu_renderer";
	}	
}
}
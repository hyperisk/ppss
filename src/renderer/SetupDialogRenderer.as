package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.ImageLoader;
import core.StageUtil;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.utils.Dictionary;

public class SetupDialogRenderer implements IFrameUpdateObject, IForegroundRenderer
{
	private var dialogClosedCallback_:Function;
	private var backgroundRenderer_:BackgroundRenderer;
	private var curItemIndex_:int;
	private var itemRenderers_:Dictionary;
	private var dialogWidth_:int;
	private var dialogHeight_:int;

	private var quitBitmap_:Bitmap;
	private var quitButton_:Sprite;
	
	private static const PAGE_BUTTON_WIDTH_PERCENT:int = 7;
	private static const PAGE_BUTTON_HEIGHT_PERCENT:int = 12;
	private static const PAGE_BUTTON_GAP_PERCENT:int = 10;
	private static const PAGE_BUTTON_BOTTOM_MARGIN_PERCENT:int = 8;
	private var prevPageButton_:Sprite;
	private var nextPageButton_:Sprite;

	public function SetupDialogRenderer(backgroundRenderer:BackgroundRenderer, dialogClosedCallback:Function) {
		backgroundRenderer_ = backgroundRenderer;
		dialogClosedCallback_ = dialogClosedCallback;

		quitButton_ = new Sprite();
		quitBitmap_ = null;
		prevPageButton_ = new Sprite();
		nextPageButton_ = new Sprite();
		if (StageUtil.getSingleton().userInput_.touchPointSupported) {
			quitButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				hide();
			});
			prevPageButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				prevPage();
			});
			nextPageButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				nextPage();
			});
		} else {
			quitButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				hide();
			});
			prevPageButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				prevPage();
			});
			nextPageButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				nextPage();
			});
		}
	}
	
	public function createAllRenderers():void {
		itemRenderers_ = new Dictionary();
		for (var i:int = 0; i < GameConfig.getSingleton().getNumMenuItems(); i++) {
			var curItemName:String = GameConfig.getSingleton().getMenuNameFromIndex(i);
			var newRenderer:SetupItemRendererBase = null;
			var itemData:Dictionary = null;
			switch (curItemName) {
				case GameConfig.BALL_SINGLE_PADDLE_SIDE:
					itemData = new Dictionary();
					itemData['bitmapName1'] = "setup_single_paddle_side_1.png";
					itemData['bitmapName2'] = "setup_single_paddle_side_2.png";
					itemData['itemName'] = curItemName;
					newRenderer = new SetupItemRadioRenderer(curItemName, itemData);
					itemRenderers_[i] = newRenderer;
					break;

				case GameConfig.BALL_DRAW_MAGNITUDE:
					itemData = new Dictionary();
					itemData['bitmapName1'] = "setup_ball_mag_limit_1.png";
					itemData['bitmapName2'] = "setup_ball_mag_limit_2.png";
					itemData['valueMin'] = 1;
					itemData['valueMax'] = 3;
					itemData['itemName'] = curItemName;
					newRenderer = new SetupItemSliderRenderer(curItemName, itemData);
					itemRenderers_[i] = newRenderer;
					break;

				case GameConfig.PADDLE_MAGNITUDE:
					itemData = new Dictionary();
					itemData['bitmapName1'] = "setup_paddle_mag_limit_1.png";
					itemData['bitmapName2'] = "setup_paddle_mag_limit_2.png";
					itemData['valueMin'] = 1;
					itemData['valueMax'] = 3;
					itemData['itemName'] = curItemName;
					newRenderer = new SetupItemSliderRenderer(curItemName, itemData);
					itemRenderers_[i] = newRenderer;
					break;
				
				case GameConfig.PADDLE_HANDLE_LENGTH:
					itemData = new Dictionary();
					itemData['bitmapName1'] = "setup_paddle_handle_length_limit_1.png";
					itemData['bitmapName2'] = "setup_paddle_handle_length_limit_2.png";
					itemData['valueMin'] = 0;
					itemData['valueMax'] = GameConfig.PADDLE_HANDLE_LENGTH_MAX_NUM_STEPS;
					itemData['itemName'] = curItemName;
					newRenderer = new SetupItemSliderRenderer(curItemName, itemData);
					itemRenderers_[i] = newRenderer;
					break;
				
				case GameConfig.PHYSICS_SLOW_DOWN_PERCENT:
					itemData = new Dictionary();
					itemData['bitmapName1'] = "setup_physics_slowdown_limit_1.png";
					itemData['bitmapName2'] = "setup_physics_slowdown_limit_2.png";
					itemData['valueMin'] = 20;
					itemData['valueMax'] = 100;
					itemData['valueStep'] = 20;
					itemData['itemName'] = curItemName;
					newRenderer = new SetupItemSliderRenderer(curItemName, itemData);
					itemRenderers_[i] = newRenderer;
					break;
				
				case GameConfig.RENDER_FPS_TARGET:
					break;
				
				case GameConfig.PHYSICS_SIM_FPS_TAGET:
					break;
				
				default:
					throw new Error("unhandled setup item name " + curItemName);
			}
		}
	}
	
	public function show():void {
		dialogWidth_ = StageUtil.getSingleton().stageWidth_;
		dialogHeight_ = StageUtil.getSingleton().stageHeight_;
		backgroundRenderer_.show(BackgroundRenderer.TYPE_SETUP_MENU);
		showQuitButton();
		curItemIndex_ = 0;
		showCurItem();
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	// interface impl
	public function hide():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		if (curItemIndex_ < 0) {
			return;	// for keyboard input case, not to crash
		}
		hideQuitButton();
		hideCurItem();
		curItemIndex_ = -1;
		dialogClosedCallback_();			
	}
	
	private function prevPage():void {
		if (!itemRenderers_[curItemIndex_].itemChangeLock_) {
			hideCurItem();
			curItemIndex_--;
			showCurItem();
		}
	}
	
	private function nextPage():void {
		if (!itemRenderers_[curItemIndex_].itemChangeLock_) {
			hideCurItem();
			curItemIndex_++;
			showCurItem();
		}
	}
	
	private function showPrevPageButton():void {
		prevPageButton_.graphics.clear();
		prevPageButton_.graphics.beginFill(0x222222);
		var edgeX:int = dialogWidth_ / 2 - PAGE_BUTTON_GAP_PERCENT * dialogWidth_ / 100 / 2;
		var pointX:int = edgeX - dialogWidth_ * PAGE_BUTTON_WIDTH_PERCENT / 100;
		var bottom:int = dialogHeight_ - dialogHeight_ * PAGE_BUTTON_BOTTOM_MARGIN_PERCENT / 100;
		var top:int = bottom - dialogHeight_ * PAGE_BUTTON_HEIGHT_PERCENT / 100;
		prevPageButton_.graphics.moveTo(edgeX, top);
		prevPageButton_.graphics.lineTo(edgeX, bottom);
		prevPageButton_.graphics.lineTo(pointX, (top + bottom) / 2);
		prevPageButton_.graphics.lineTo(edgeX, top);
		prevPageButton_.graphics.endFill();
		StageUtil.getSingleton().addToStage(prevPageButton_);
	}
	
	private function hidePrevPageButton():void {
		StageUtil.getSingleton().removeFromStage(prevPageButton_);
	}
	
	private function showNextPageButton():void {
		nextPageButton_.graphics.clear();
		nextPageButton_.graphics.beginFill(0x222222);
		var edgeX:int = dialogWidth_ / 2 + PAGE_BUTTON_GAP_PERCENT * dialogWidth_ / 100 / 2;
		var pointX:int = edgeX + dialogWidth_ * PAGE_BUTTON_WIDTH_PERCENT / 100;
		var bottom:int = dialogHeight_ - dialogHeight_ * PAGE_BUTTON_BOTTOM_MARGIN_PERCENT / 100;
		var top:int = bottom - dialogHeight_ * PAGE_BUTTON_HEIGHT_PERCENT / 100;
		nextPageButton_.graphics.moveTo(edgeX, top);
		nextPageButton_.graphics.lineTo(edgeX, bottom);
		nextPageButton_.graphics.lineTo(pointX, (top + bottom) / 2);
		nextPageButton_.graphics.lineTo(edgeX, top);
		nextPageButton_.graphics.endFill();
		StageUtil.getSingleton().addToStage(nextPageButton_);
	}
	
	private function hideNextPageButton():void {
		StageUtil.getSingleton().removeFromStage(nextPageButton_);
	}
	
	private function showQuitButton():void {
		if (!quitBitmap_) {
			quitBitmap_ = ImageLoader.getSingleton().getBitmap("level_button_quit_setup_72.png");
			quitButton_.addChild(quitBitmap_);
		}
		
		var gap:int = dialogHeight_ * GameConfig.BUTTON_GAP_HEIGHT_PERCENT / 100;
		var quitButtonAsRatio:Number = quitBitmap_.width / quitBitmap_.height;
		quitButton_.width = dialogHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100 * quitButtonAsRatio;
		quitButton_.height = dialogHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100;
		quitButton_.x = dialogWidth_ - (gap + quitButton_.width);
		quitButton_.y = gap;
		StageUtil.getSingleton().addToStage(quitButton_);
	}
	
	private function hideQuitButton():void {
		StageUtil.getSingleton().removeFromStage(quitButton_);
	}
	
	private function showCurItem():void {
		var itemRenderer:SetupItemRendererBase = itemRenderers_[curItemIndex_];
		itemRenderer.show(dialogWidth_, dialogHeight_);
		if (curItemIndex_ > 0) {
			showPrevPageButton();
		}
		if (curItemIndex_ < GameConfig.getSingleton().getNumMenuItems() - 1) {
			showNextPageButton();
		}
	}
	
	private function hideCurItem():void {
		var itemRenderer:SetupItemRendererBase = itemRenderers_[curItemIndex_];
		itemRenderer.hide();
		if (curItemIndex_ > 0) {
			hidePrevPageButton();
		}
		if (curItemIndex_ < GameConfig.getSingleton().getNumMenuItems() - 1) {
			hideNextPageButton();
		}
	}

	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		if (StageUtil.getSingleton().userInput_.keyDownCharCode_ == 27) { // ESC key for quit
			hide();
		} else if ((StageUtil.getSingleton().userInput_.keyDownCharCode_ == 0) && 
			(StageUtil.getSingleton().userInput_.keyDownKeyCode_ == 39)) { // arrow right, for next page
			if (curItemIndex_ < GameConfig.getSingleton().getNumMenuItems() - 1) {
				StageUtil.getSingleton().userInput_.keyDownKeyCode_ = 0;
				nextPage();
			}
		} else if ((StageUtil.getSingleton().userInput_.keyDownCharCode_ == 0) && 
			(StageUtil.getSingleton().userInput_.keyDownKeyCode_ == 37)) { // arrow left
			if (curItemIndex_ > 0) {
				StageUtil.getSingleton().userInput_.keyDownKeyCode_ = 0;
				prevPage();
			}
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "setup_dialog_renderer";
	}	
}
}
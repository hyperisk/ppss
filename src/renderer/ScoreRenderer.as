package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.StageUtil;

import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import simulation.SimBall;

public class ScoreRenderer implements IFrameUpdateObject
{
	private var scoreTextLeft_:TextField;
	private var scoreTextRight_:TextField;
	private var textFormat_:TextFormat;
	
	public function ScoreRenderer() {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		
		var textFormat:TextFormat = new TextFormat();
		textFormat.font = "Verdana";
		textFormat.size = 48;
		textFormat.color = 0xBBBBBB;
		
		scoreTextLeft_ = new TextField();
		scoreTextLeft_.defaultTextFormat = textFormat;
		scoreTextLeft_.autoSize = TextFieldAutoSize.LEFT;
		scoreTextLeft_.text = "";
		scoreTextLeft_.x = levelWidth / 2 - levelWidth / 10 - 20;
		scoreTextLeft_.y = levelHeight * 1 / 10;
		
		scoreTextRight_ = new TextField();
		scoreTextRight_.defaultTextFormat = textFormat;
		scoreTextRight_.autoSize = TextFieldAutoSize.LEFT;
		scoreTextRight_.text = "";
		scoreTextRight_.x = levelWidth / 2 + levelWidth / 10 - 20;
		scoreTextRight_.y = levelHeight * 1 / 10;
	}
	
	public function show():void {
		StageUtil.getSingleton().addToStage(scoreTextLeft_);		
		StageUtil.getSingleton().addToStage(scoreTextRight_);		
	}
	
	public function hide():void {
		StageUtil.getSingleton().removeFromStage(scoreTextLeft_);
		StageUtil.getSingleton().removeFromStage(scoreTextRight_);
	}
	
	public function drawScore(side:String, score:int):void {
		if (side == SimBall.LEFT_SIDE) {
			scoreTextLeft_.text = score.toString(10);
		} else {
			scoreTextRight_.text = score.toString(10);
		}
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		return true;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "score_renderer";
	}
}
}
package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;

import level.PaddleEvent;

import simulation.SimPaddleBase;
import simulation.SimWorld;

public class PaddleRenderer implements IFrameUpdateObject
{
	public static const LEFT_SIDE:String = "left_side";
	public static const RIGHT_SIDE:String = "right_side";
	private var side_:String;
	private var simWorld_:SimWorld;
	private var simPaddle_:SimPaddleBase;
	private static const PADDLE_ASPECT_RATIO:Number = 0.2;
	private static const VEL_VECTOR_LEN_RATIO:Number = 0.1;
	private var states_:SimpleStateMachine;
	private var widget_:Sprite;
	private var paddleHeight_:int;
	private var widgetWidth_:int;
	private var widgetHeight_:int;
	
	// set by frame update
	private var posScreenX_:int;
	private var posScreenY_:int;
	
	public function PaddleRenderer(simWorld:SimWorld, side:String) {
		simWorld_ = simWorld;
		side_ = side;
		simPaddle_ = null;
		widget_ = new Sprite();
		states_ = new SimpleStateMachine([
			['hidden', 0],
			['showing', 0],
			['invalidPos', 0],
		]);
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	public function show():void {
		simPaddle_ = simWorld_.getSimPaddleInitialized(side_);
		if (!simPaddle_) {
			throw new Error("paddle render show - invalid sim paddle");
		}
		simPaddle_.simOutputEventDispatcher_.addEventListener(PaddleEvent.ACTION_SIM_OUTPUT, onPaddleEvent);
		
		if (!states_.isStateActive("hidden")) {
			throw new Error("current state is not hidden -- paddneRenderer show");
		}
		
		var mag:int = GameConfig.getSingleton().getValue(GameConfig.PADDLE_MAGNITUDE); 
		paddleHeight_ = simWorld_.worldToScreenY(mag * SimPaddleBase.PADDLE_HEIGHT, true);
		var paddleWidth:int = paddleHeight_ * PADDLE_ASPECT_RATIO;
		widget_.graphics.clear();
		if (mag == 1) {
			widget_.graphics.lineStyle(1, 0xEE9999);
			widget_.graphics.beginFill(0x995555);
			widget_.graphics.drawEllipse(0, 0, paddleWidth, paddleHeight_); 
			widget_.graphics.endFill();
			widgetWidth_ = paddleWidth;
			widgetHeight_ = paddleHeight_;
		} else {
			widget_.graphics.beginFill(0xAA7788, 0.5);
			var padding:Number = paddleWidth / 2 * mag; 
			widget_.graphics.drawEllipse(0, 0, paddleWidth + padding * 2, paddleHeight_ + padding * 2); 
			
			widget_.graphics.lineStyle(1, 0xEE9999);
			widget_.graphics.beginFill(0x995555, 0.7);
			widget_.graphics.drawEllipse(padding, padding, paddleWidth, paddleHeight_); 
			widgetWidth_ = paddleWidth + padding * 2;
			widgetHeight_ = paddleHeight_ + padding * 2;
		}
		widget_.graphics.endFill();
		StageUtil.getSingleton().addToStage(widget_);
		
		states_.setActiveState("showing");
	}
	
	public function hide():void {
		if (!states_.isStateActive("hidden")) {
			StageUtil.getSingleton().removeFromStage(widget_);
			states_.setActiveState("hidden");
		}
		simPaddle_ = null;
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		if (states_.isStateActive("showing")) {
			if (!simPaddle_.isPosValid()) {
				states_.setActiveState("invalidPos");
				widget_.alpha = 0;
			}
		} else if (states_.isStateActive("invalidPos")) {
			if (simPaddle_.isPosValid()) {
				states_.setActiveState("showing");
				widget_.alpha = 1;
			}
		}
		
		return true;
	}
	
	public function onPaddleEvent(event:Event):void {
		var paddleEvent:PaddleEvent = event as PaddleEvent;
		if (isNaN(paddleEvent.posX_)) {
			throw new Error("paddleDirect event pos is invalid");
		}
		if (isNaN(paddleEvent.pitchDeg_)) {
			throw new Error("invalid paddle pitch"); 
		}
		posScreenX_ = simWorld_.worldToScreenX(paddleEvent.posX_);
		posScreenY_ = simWorld_.worldToScreenY(paddleEvent.posY_);
		var m:Matrix = widget_.transform.matrix;
		m.identity();
		m.translate(-widgetWidth_ / 2, -widgetHeight_ / 2);
		m.rotate(-simPaddle_.pitchDeg_ * Math.PI / 180);
		m.translate(posScreenX_, posScreenY_);
		widget_.transform.matrix = m;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "paddle_renderer";;
	}
}
}
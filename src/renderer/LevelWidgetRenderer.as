package renderer
{
import core.GameConfig;
import core.IFrameUpdateObject;
import core.ImageLoader;
import core.LevelManager;
import core.StageUtil;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TouchEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

public class LevelWidgetRenderer implements IFrameUpdateObject
{
	public static const BUTTON_REPLAY:String = "button_replay";
	public static const BUTTON_QUIT:String = "button_quit";
	
	private var levelManager_:LevelManager;
	private var levelWidth_:int;
	private var levelHeight_:int;
	
	private var titleText_:TextField;
	private var textFormat_:TextFormat;
	
	private var replayBitmap_:Bitmap;
	private var replayButton_:Sprite;
	private var quitBitmap_:Bitmap;
	private var quitButton_:Sprite;

	public function LevelWidgetRenderer(levelManager:LevelManager) {
		levelWidth_ = StageUtil.getSingleton().stageWidth_;
		levelHeight_ = StageUtil.getSingleton().stageHeight_;
		levelManager_ = levelManager;
		titleText_ = new TextField();
		textFormat_ = new TextFormat();
		replayButton_ = new Sprite();
		replayBitmap_ = ImageLoader.getSingleton().getBitmap("level_button_replay_72.png");
		replayButton_.addChild(replayBitmap_);
		quitButton_ = new Sprite();
		quitBitmap_ = ImageLoader.getSingleton().getBitmap("level_button_quit_level_72.png");
		quitButton_.addChild(quitBitmap_);
		if (quitButton_.width == 0) {
			throw new Error("quit button width is 0 after setting bitmap");
		}
		
		StageUtil.getSingleton().addFrameUpdateObject(this);
		if (StageUtil.getSingleton().userInput_.touchPointSupported) {
			replayButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				levelManager_.onLevelWidgetPressed(BUTTON_REPLAY);
			});
			quitButton_.addEventListener(TouchEvent.TOUCH_BEGIN, function(event:TouchEvent):void {
				levelManager_.onLevelWidgetPressed(BUTTON_QUIT);
			});
		} else {
			replayButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				levelManager_.onLevelWidgetPressed(BUTTON_REPLAY);
			});
			quitButton_.addEventListener(MouseEvent.MOUSE_DOWN, function(event:MouseEvent):void {
				levelManager_.onLevelWidgetPressed(BUTTON_QUIT);
			});
		}
	}
	
	public function showTitleText(text:String):void {
		textFormat_.font = "Verdana";
		textFormat_.size = 36;
		titleText_.defaultTextFormat = textFormat_;
		titleText_.autoSize = TextFieldAutoSize.LEFT;
		titleText_.text = text;
		titleText_.x = levelWidth_ / 2 - titleText_.width / 2;
		titleText_.y = levelHeight_ * 2 / 10;
		if (!titleText_.parent) {
			StageUtil.getSingleton().addToStage(titleText_);
		}
	}
	
	public function hideTitleText():void {
		if (titleText_.parent) {
			StageUtil.getSingleton().removeFromStage(titleText_);
		}
	}
	
	public function showReplayButton():void {
		var gap:int = levelHeight_ * GameConfig.BUTTON_GAP_HEIGHT_PERCENT / 100;
		var replayButtonAsRatio:Number = replayBitmap_.width / replayBitmap_.height;
		var buttonWidth:int = levelHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100 * replayButtonAsRatio;
		replayButton_.width = buttonWidth;
		replayButton_.height = levelHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100;
		if (!StageUtil.getSingleton().infoText_.isEnabled()) {
			replayButton_.x = gap;
		} else {
			replayButton_.x = levelWidth_ - (gap + buttonWidth) * 2;
		}
		replayButton_.y = gap;
		StageUtil.getSingleton().addToStage(replayButton_, StageUtil.STAGE_OBJECT_GROUP_USERINPUT);
	}
	
	public function hideReplayButton():void {
		StageUtil.getSingleton().removeFromStage(replayButton_);
	}
	
	public function showQuitButton():void {
		var gap:int = levelHeight_ * GameConfig.BUTTON_GAP_HEIGHT_PERCENT / 100;
		var quitButtonAsRatio:Number = quitBitmap_.width / quitBitmap_.height;
		var buttonWidth:int = levelHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100 * quitButtonAsRatio;
		if (quitButton_.width == 0) {
			throw new Error("quit button initial width is 0");
		}
		quitButton_.width = buttonWidth;
		quitButton_.height = levelHeight_ * GameConfig.BUTTON_SIZE_HEIGHT_PERCENT / 100;
		quitButton_.x = levelWidth_ - (gap + buttonWidth);
		quitButton_.y = gap;
		StageUtil.getSingleton().addToStage(quitButton_, StageUtil.STAGE_OBJECT_GROUP_USERINPUT);
	}
	
	public function hideQuitButton():void {
		StageUtil.getSingleton().removeFromStage(quitButton_);
	}

	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		if (replayButton_.parent && 
			(StageUtil.getSingleton().userInput_.keyDownCharCode_ == 114)) {	// r key
			levelManager_.onLevelWidgetPressed(BUTTON_REPLAY);
		} else if (quitButton_.parent && 
			(StageUtil.getSingleton().userInput_.keyDownCharCode_ == 27)) {	// esc key
			levelManager_.onLevelWidgetPressed(BUTTON_QUIT);
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "level_widget_renderer";
	}		
}
}
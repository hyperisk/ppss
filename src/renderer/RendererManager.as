package renderer
{
import core.IFrameUpdateObject;
import core.LevelManager;
import core.StageUtil;

import level.AiPlayerTableRenderer;

import simulation.SimBall;
import simulation.SimWorld;

public class RendererManager implements IFrameUpdateObject
{
	// background
	private var backgroundRenderer_:BackgroundRenderer;
	
	// foreground
	private var loadingRenderer_:LoadingRenderer; 
	private var mainMenuRenderer_:MainMenuRenderer;
	private var levelMenuRenderer_:LevelMenuRenderer;
	private var setupDialogRenderer_:SetupDialogRenderer;
	
	// widgets, objects etc. for levels
	public var levelWidgetRenderer_:LevelWidgetRenderer;
	public var tableRenderer_:TableRenderer;
	public var ballRenderer_:BallRenderer;
	public var ballStatRenderer_:BallStatRenderer;
	public var ballPathEstimateRenderer_:BallPathRenderer;
	public var launcherRendererLeft_:LauncherRenderer;
	public var launcherRendererRight_:LauncherRenderer;
	public var paddleRendererLeft_:PaddleRenderer;
	public var paddleRendererRight_:PaddleRenderer;
	public var aiPlayerTableRendererLeft_:AiPlayerTableRenderer;
	public var aiPlayerTableRendererRight_:AiPlayerTableRenderer;
	
	private var levelWidth_:int;
	private var levelHeight_:int;
	
	public function RendererManager() {
		backgroundRenderer_ = new BackgroundRenderer();
		loadingRenderer_ = new LoadingRenderer();
		mainMenuRenderer_ = new MainMenuRenderer(backgroundRenderer_);
		levelMenuRenderer_ = new LevelMenuRenderer(backgroundRenderer_);
		setupDialogRenderer_ = new SetupDialogRenderer(backgroundRenderer_, levelMenuRenderer_.show);
	}
	
	public function showBackground(type:String, tableHeightPercent:int = 0):void {
		backgroundRenderer_.show(type, tableHeightPercent);
	}
	
	public function createSetupItems():void {
		setupDialogRenderer_.createAllRenderers();
	}
	
	public function showLoading():void {
		showBackground(BackgroundRenderer.TYPE_LOADING);
		loadingRenderer_.show();
	}
	
	public function showMainMenu(menuSelectedCallback:Function):void {
		backgroundRenderer_.hide();
		mainMenuRenderer_.setMenuSelectedCallback(menuSelectedCallback);
		mainMenuRenderer_.show();
	}

	public function showLevelMenu(menuSelectedCallback:Function, numItems:int):void {
		levelMenuRenderer_.setMenuSelectedCallback(function(levelIndex:int):void {
			if (levelIndex == LevelMenuRenderer.MENU_INDEX_SETUP) {
				setupDialogRenderer_.show();
			} else {
				menuSelectedCallback(levelIndex);
			}
		});
		levelMenuRenderer_.show(numItems);
	}

	public function createLevelRenders(levelManager:LevelManager):void {
		var simWorld:SimWorld = levelManager.simWorld_;
		tableRenderer_ = new TableRenderer();
		ballRenderer_ = new BallRenderer(simWorld);
		ballStatRenderer_ = new BallStatRenderer(simWorld);
		ballPathEstimateRenderer_ = new BallPathRenderer(simWorld);
		levelWidgetRenderer_ = new LevelWidgetRenderer(levelManager);
		launcherRendererLeft_ = new LauncherRenderer(SimBall.LEFT_SIDE, tableRenderer_);
		launcherRendererRight_ = new LauncherRenderer(SimBall.RIGHT_SIDE, tableRenderer_);
		paddleRendererLeft_ = new PaddleRenderer(simWorld, PaddleRenderer.LEFT_SIDE);
		paddleRendererRight_ = new PaddleRenderer(simWorld, PaddleRenderer.RIGHT_SIDE);
		aiPlayerTableRendererLeft_ = new AiPlayerTableRenderer(simWorld, levelManager.aiPlayerTableLeft_);
		aiPlayerTableRendererRight_ = new AiPlayerTableRenderer(simWorld, levelManager.aiPlayerTableRight_);
	}
	
	public function updateLoadingStatus(percent:int):void {
		loadingRenderer_.updateStatus(percent);
	}
									   
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		return true;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "renderer_manager";
	}
}
}
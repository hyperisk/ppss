package core
{
import flash.filesystem.FileStream;
import flash.geom.Rectangle;
import flash.utils.ByteArray;
import flash.utils.getTimer;

import renderer.TableRenderer;
import renderer.UserControlWidget;

import simulation.BallCfg;
import simulation.EstimateHitPath;
import simulation.EstimatePath;
import simulation.SimBall;
import simulation.SimTable;

public class PaddlePitchDatabase
{
	private var boundRectLeftSide_:Rectangle;
	private var numEstimates_:int;
	private var numAbortEstimateUnderTable_:int;
	private var numValidCfgs_:int;
	private var estimateHitPath_:EstimateHitPath;
	private var foundPitchesByte_:ByteArray;
	public var estimateRangeErrorStr_:String;
	
	private var TRACE_DEBUG_MSG:Boolean = false;
	private const TRACE_DEBUG_MSG_CFGS:Boolean = false;
	private const TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH:Boolean = false;
	private const VERIFY_AFTER_READ:Boolean = false;
	
	private var ballPosMinX_:Number;
	private var ballPosStepX_:Number;
	private var ballPosNumX_:int;
	private var ballPosMaxCalcX_:Number;
	private var ballPosMinY_:Number;
	private var ballPosStepY_:Number;
	private var ballPosNumY_:int;
	private var ballPosMaxCalcY_:Number;
	private var ballVelMinX_:Number;
	private var ballVelStepX_:Number;
	private var ballVelNumX_:int;
	private var ballVelMaxCalcX_:Number;
	private var ballVelMinY_:Number;
	private var ballVelStepY_:Number;
	private var ballVelNumY_:int;
	private var ballVelMaxCalcY_:Number;
	private var paddleVelMinX_:Number;
	private var paddleVelStepX_:Number;
	private var paddleVelNumX_:int;
	private var paddleVelMaxCalcX_:Number;
	private var paddleVelMinY_:Number;
	private var paddleVelStepY_:Number;
	private var paddleVelNumY_:int;
	private var paddleVelMaxCalcY_:Number;
	private var paddlePitchMin_:int;
	private var paddlePitchMax_:int;
	private var paddlePitchStep_:int;
	
	public function PaddlePitchDatabase() {
		var maxLevelWidth:Number = SimTable.TABLE_TOP_WIDTH / (TableRenderer.TABLE_WIDTH_PERCENT_MIN / 100);
		var spaceX:Number = maxLevelWidth / 2 - SimTable.TABLE_TOP_WIDTH / 2;
		var marginX:Number = spaceX * UserControlWidget.INPUT_AREA_HORZ_MARGIN_PERCENT / 100;  
		var boundRectWidth:Number = spaceX - marginX / 2;	// note: no margin between boundRect and table, and min margin on the left
		var boundRectHeight:Number = SimTable.TABLE_LEG_HEIGHT * UserControlWidget.INPUT_AREA_HEIGHT_TABLE_HEIGHT_RATIO;
		var boundRectVertCenter:Number = SimTable.TABLE_LEG_HEIGHT + SimTable.TABLE_TOP_PLAY_POS_DIST;
		boundRectLeftSide_ = new Rectangle(-SimTable.TABLE_TOP_WIDTH / 2 - boundRectWidth, 
			boundRectVertCenter - boundRectHeight / 2, boundRectWidth, boundRectHeight);
		
		trace("I PaddlePitchDatabase, maxLevelWidth half: " + EngineUtil.roundBrief(maxLevelWidth / 2) + 
			", spaceX: " + EngineUtil.roundBrief(spaceX) + 
			", marginX: " + EngineUtil.roundBrief(marginX) + 
			", boundRect w,h: " + EngineUtil.roundBrief(boundRectWidth) + ", " + EngineUtil.roundBrief(boundRectHeight));

		estimateHitPath_ = new EstimateHitPath();
		estimateRangeErrorStr_ = new String();
	}
	
	public function buildAllBySim():void {
		if (!TRACE_DEBUG_MSG_CFGS) {
			EstimateHitPath.TRACE_DEBUG_MSG = false;
			EstimateHitPath.TRACE_DEBUG_MSG_ALL_STEPS = false;
			EstimateHitPath.TRACE_DEBUG_MSG_SEARCH = false;
		}
		
		var startTime:int;
		numEstimates_ = 0;
		numAbortEstimateUnderTable_ = 0;
		numValidCfgs_ = 0;
		var ballCfgIter:BallCfg = new BallCfg();

		var quick_step_mul:Number = 1;
		ballPosMinX_ = -2.8;
		ballPosStepX_ = 0.2 * quick_step_mul;
		ballPosNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballPosMinX_, -1.4, ballPosStepX_) + 1;
		ballPosMinY_ = 0.7;
		ballPosStepY_ = 0.1 * quick_step_mul;
		ballPosNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballPosMinY_, 1.5, ballPosStepY_) + 1; 
		ballVelMinX_ = -5.5;
		ballVelStepX_ = 0.5 * quick_step_mul;
		ballVelNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballVelMinX_, -0.5, ballVelStepX_) + 1;
		ballVelMinY_ = -2.7;
		ballVelStepY_ = 0.3 * quick_step_mul;
		ballVelNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballVelMinY_, 2.4, ballVelStepY_) + 1;
		paddleVelMinX_ = 2;
		paddleVelStepX_ = 0.5 * quick_step_mul;
		paddleVelNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(paddleVelMinX_, 9.5, paddleVelStepX_) + 1;
		paddleVelMinY_ = -5 
		paddleVelStepY_ = 0.5 * quick_step_mul;
		paddleVelNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(paddleVelMinY_, 5, paddleVelStepY_) + 1;
		/*
		ballPosMinX_ = -2.02;
		ballPosStepX_ = 0.2 * quick_step_mul;
		ballPosNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballPosMinX_, ballPosMinX_ + ballPosStepX_, ballPosStepX_) + 1;
		ballPosMinY_ = 0.95;
		ballPosStepY_ = 0.1 * quick_step_mul;
		ballPosNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballPosMinY_, ballPosMinY_ + ballPosStepY_, ballPosStepY_) + 1; 
		ballVelMinX_ = -3.53;
		ballVelStepX_ = 0.5 * quick_step_mul;
		ballVelNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballVelMinX_, ballVelMinX_ + ballVelStepX_, ballVelStepX_) + 1;
		ballVelMinY_ = -1.22;
		ballVelStepY_ = 0.3 * quick_step_mul;
		ballVelNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(ballVelMinY_, ballVelMinY_ + ballVelStepY_, ballVelStepY_) + 1;
		paddleVelMinX_ = 9.5;
		paddleVelStepX_ = 0.5 * quick_step_mul;
		paddleVelNumX_ = EngineUtil.getNumIntervalsFromMinMaxSafe(paddleVelMinX_, paddleVelMinX_ + paddleVelStepX_, paddleVelStepX_) + 1;
		paddleVelMinY_ = 0 
		paddleVelStepY_ = 0.5 * quick_step_mul;
		paddleVelNumY_ = EngineUtil.getNumIntervalsFromMinMaxSafe(paddleVelMinY_, paddleVelMinY_ + paddleVelStepY_, paddleVelStepY_) + 1;
		*/
		paddlePitchMin_ = -12;
		paddlePitchMax_ = 40;
		paddlePitchStep_ = 1;
		calcInclusiveMaxValues();

		var totalNumToEstimate:int = ballPosNumX_ * ballPosNumY_ * ballVelNumX_ * ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_;
		if (TRACE_DEBUG_MSG) {
			startTime = getTimer();
			trace("I   building pitch database, play area bounds, x:" + 
					EngineUtil.roundBrief(boundRectLeftSide_.left) + " ~ " + EngineUtil.roundBrief(boundRectLeftSide_.right) +
					", y: " + EngineUtil.roundBrief(boundRectLeftSide_.top) + " ~ " + EngineUtil.roundBrief(boundRectLeftSide_.bottom)
					);
			traceEstimateInitialValues();
			trace("    totalNumToEstimate: " + totalNumToEstimate);
			trace("    -----------------------------------");
		}

		foundPitchesByte_ = new ByteArray();
		for (var ballPosIntX:int = 0; ballPosIntX < ballPosNumX_; ballPosIntX++) {
			var ballPosX:Number = ballPosMinX_ + ballPosIntX * ballPosStepX_;
			trace("    ...ball pos x interval " + EngineUtil.roundBrief(ballPosX));
			for (var ballPosIntY:int = 0; ballPosIntY < ballPosNumY_; ballPosIntY++) {
				var ballPosY:Number = ballPosMinY_ + ballPosIntY * ballPosStepY_;
				trace("    ...  ball pos y interval " + EngineUtil.roundBrief(ballPosY));
				for (var ballVelIntX:int = 0; ballVelIntX < ballVelNumX_; ballVelIntX++) {
					var ballVelX:Number = ballVelMinX_ + ballVelIntX * ballVelStepX_;
					for (var ballVelIntY:int = 0; ballVelIntY < ballVelNumY_; ballVelIntY++) {
						var ballVelY:Number = ballVelMinY_ + ballVelIntY * ballVelStepY_;	
						for (var paddleVelIntX:int = 0; paddleVelIntX < paddleVelNumX_; paddleVelIntX++) {
							var paddleVelX:Number = paddleVelMinX_ + paddleVelIntX * paddleVelStepX_;
							for (var paddleVelIntY:int = 0; paddleVelIntY < paddleVelNumY_; paddleVelIntY++) {
								var paddleVelY:Number = paddleVelMinY_ + paddleVelIntY * paddleVelStepY_;
								ballCfgIter.posX_ = ballPosX;
								ballCfgIter.posY_ = ballPosY;
								ballCfgIter.velX_ = ballVelX;
								ballCfgIter.velY_ = ballVelY;
								ballCfgIter.spin_ = 0;
								var paddlePitchFound:Number = findValidPaddlePitch(ballCfgIter, paddleVelX, paddleVelY);
								addPitchToByteArray(paddlePitchFound);
								
								if (!isNaN(paddlePitchFound)) {
									numValidCfgs_++;
								}
								if (TRACE_DEBUG_MSG_CFGS) {
									trace("    est# " + numEstimates_ + ", ball cfg " + ballCfgIter.getInfoString() +
										" (table dist: " + EngineUtil.roundBrief(-ballPosX - SimTable.TABLE_TOP_WIDTH / 2) + 
										", " + EngineUtil.roundBrief(ballPosY - SimTable.TABLE_LEG_HEIGHT) +
										") paddleVel: " + EngineUtil.roundBrief(paddleVelX) + ", " + EngineUtil.roundBrief(paddleVelY) +
										" ==> " + EngineUtil.roundBrief(paddlePitchFound)
									);
								}
								numEstimates_++;
							}
						}
					}
				}
			}
		}
		
		if (totalNumToEstimate != numEstimates_) {
			throw new Error();
		}
		if (foundPitchesByte_.length != numEstimates_) {
			throw new Error();
		}
		
		if (TRACE_DEBUG_MSG) {
			trace("I   among " + numEstimates_ + " estimates, aborted " + numAbortEstimateUnderTable_ + 
				" and found " + numValidCfgs_ + " valid cfg(s), " + 
				EngineUtil.roundBrief(numValidCfgs_ / (numEstimates_ - numAbortEstimateUnderTable_) * 100) + "%" +
			", elapsed time: " + EngineUtil.roundBrief((getTimer() - startTime) / 1000 / 60) + "min\n\n");
		}

		if (TRACE_DEBUG_MSG) {
			//EstimateHitPath.TRACE_DEBUG_MSG = true;
		}
		trace("I verifying estimated pitches... step 1. in discrete space");
		verifyArrayGet(false);
		trace("I verifying estimated pitches... step 2. in continuous space");
		verifyArrayGet(true);
	}
	
	private function traceEstimateInitialValues():void {
		trace("    ball pos x:" + EngineUtil.roundBrief(ballPosMinX_) + " ~ " + EngineUtil.roundBrief(ballPosMaxCalcX_) + 
			" step: " + EngineUtil.roundBrief(ballPosStepX_) + " num data: " + ballPosNumX_ + "\n" +
			"    ball pos y:" + EngineUtil.roundBrief(ballPosMinY_) + " ~ " + EngineUtil.roundBrief(ballPosMaxCalcY_) + 
			" step: " + EngineUtil.roundBrief(ballPosStepY_) + " num data: " + ballPosNumY_ + "\n" +
			"    ball val x:" + EngineUtil.roundBrief(ballVelMinX_) + " ~ " + EngineUtil.roundBrief(ballVelMaxCalcX_) +  
			" step: " + EngineUtil.roundBrief(ballVelStepX_) + " num data: " + ballVelNumX_ + "\n" +
			"    ball vel y:" + EngineUtil.roundBrief(ballVelMinY_) + " ~ " + EngineUtil.roundBrief(ballVelMaxCalcY_) + 
			" step: " + EngineUtil.roundBrief(ballVelStepY_) + " num data: " + ballVelNumY_ + "\n" +
			"    paddle vel x: " + EngineUtil.roundBrief(paddleVelMinX_) + " ~ " + EngineUtil.roundBrief(paddleVelMaxCalcX_) +
			" step: " + EngineUtil.roundBrief(paddleVelStepX_) + " num data: " + paddleVelNumX_ + "\n" +
			"    paddle vel y: " + EngineUtil.roundBrief(paddleVelMinY_) + " ~ " + EngineUtil.roundBrief(paddleVelMaxCalcY_) +
			" step: " + EngineUtil.roundBrief(paddleVelStepY_) + " num data: " + paddleVelNumY_ + "\n"
		);
	}

	// if min = 0.1, step = 0.2, num intervals = 2, then max must be greater than 0.5
	// (without +delta, max can be 0.49999999 and it is not inclusive)
	// note: do not compute other value based on this. Use this only for comparing input data
	private function calcInclusiveMaxValues():void {
		ballPosMaxCalcX_ = ballPosMinX_ + ballPosStepX_ * ((ballPosNumX_ - 1) + 0.0001);
		ballPosMaxCalcY_ = ballPosMinY_ + ballPosStepY_ * ((ballPosNumY_ - 1) + 0.0001); 
		ballVelMaxCalcX_ = ballVelMinX_ + ballVelStepX_ * ((ballVelNumX_ - 1) + 0.0001); 
		ballVelMaxCalcY_ = ballVelMinY_ + ballVelStepY_ * ((ballVelNumY_ - 1) + 0.0001);
		paddleVelMaxCalcX_ = paddleVelMinX_ + paddleVelStepX_ * ((paddleVelNumX_ - 1) + 0.0001);  
		paddleVelMaxCalcY_ = paddleVelMinY_ + paddleVelStepY_ * ((paddleVelNumY_ - 1) + 0.0001);
	}
	
	private function addPitchToByteArray(p:Number):void {
		var pb:int;
		if (isNaN(p)) {
			pb = 127 * 2 + 1;
		} else {
			pb = Math.round((p - paddlePitchMin_) / (paddlePitchMax_ - paddlePitchMin_) * 127 * 2);
			pb = Math.min(127 * 2, pb);
			pb = Math.max(0, pb);
		}
		foundPitchesByte_.writeByte(pb);
	}
	
	private function getPitchFromByteArray(index:int):Number {
		var pb:int = foundPitchesByte_[index];
		if (pb == 127 * 2 + 1) {
			return NaN;
		} else {
			return pb / (127 * 2) * (paddlePitchMax_ - paddlePitchMin_) + paddlePitchMin_;
		}
	}
	
	private function findValidPaddlePitch(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number):Number {
		if (ballCfg.posX_ > -SimTable.TABLE_TOP_WIDTH / 2) {
			if (ballCfg.posY_ < SimTable.TABLE_LEG_HEIGHT) {
				numAbortEstimateUnderTable_++;
				return NaN;
			}
		} else {
			if (ballCfg.posY_ < SimTable.TABLE_LEG_HEIGHT) {
				var ballTableDist:Number = -SimTable.TABLE_TOP_WIDTH / 2 - ballCfg.posX_;
				var ballHitSlopeUnderTable:Number = paddleVelY / paddleVelX; 
				var possiblePosY:Number = SimTable.TABLE_LEG_HEIGHT - ballTableDist * ballHitSlopeUnderTable;
				if (ballCfg.posY_ < possiblePosY) {
					numAbortEstimateUnderTable_++;
					return NaN;
				}
			}
		}
		
		var ballCfgSim:BallCfg = new BallCfg();
		var pitchSearchLowerBound:Number = paddlePitchMin_;
		var pitchSearchUpperBound:Number = paddlePitchMax_;
		if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
			trace("  begin pitch binary search");
		}
		for (var bsi:int = 0; bsi < 10; bsi++) {
			ballCfgSim.updateFromBallCfg(ballCfg);
			var pitchSearchMid:Number = (pitchSearchLowerBound + pitchSearchUpperBound) / 2;  
			var returnType:int = estimateHitPath_.run(ballCfgSim, paddleVelX, paddleVelY, pitchSearchMid);
			if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
				trace("    estimate run result: " + returnType + " for pitch " + pitchSearchMid);
			}
			if (returnType == -1) {
				pitchSearchLowerBound = pitchSearchMid;
				if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
					trace("      set lower bound to " + pitchSearchLowerBound);
				}
			} else if (returnType == 1) {
				pitchSearchUpperBound = pitchSearchMid;
				if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
					trace("      set upper bound to " + pitchSearchUpperBound);
				}
			} else  {
				if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
					trace("      found :) " + pitchSearchMid);
				}
				return pitchSearchMid;
			}
			
			if (pitchSearchUpperBound - pitchSearchLowerBound < paddlePitchStep_) {
				if (TRACE_DEBUG_MSG_BINARY_PITCH_SEARCH) {
					trace("      end :(");
				}
				break;
			}
		}
		return NaN;
	}
	
	private function verifyArrayGet(continuousSpace:Boolean):Boolean {
		var TRACE_DEBUG_MSG_before:Boolean = TRACE_DEBUG_MSG;
		var TRACE_DEBUG_MSG_SEARCH_before:Boolean = TRACE_DEBUG_MSG;
		TRACE_DEBUG_MSG = false;
		EstimateHitPath.TRACE_DEBUG_MSG_SEARCH = false;
		EngineUtil.randomSeed_ = 57123;
		var ballCfg:BallCfg = new BallCfg();
		var numSuccess:int = 0;
		var numFail:int = 0;
		for (var vi:int = 0; vi < 100; vi++) {
			//trace(" verify #" + vi);
			ballCfg.spin_ = 0;
			var paddleVelX:Number;
			var paddleVelY:Number;
			if (continuousSpace) {
				ballCfg.posX_ = ballPosMinX_ + (ballPosNumX_ - 1) * EngineUtil.seededRandom() * ballPosStepX_;
				ballCfg.posY_ = ballPosMinY_ + (ballPosNumY_ - 1) * EngineUtil.seededRandom() * ballPosStepY_;
				ballCfg.velX_ = ballVelMinX_ + (ballVelNumX_ - 1) * EngineUtil.seededRandom() * ballVelStepX_;
				ballCfg.velY_ = ballVelMinY_ + (ballVelNumY_ - 1) * EngineUtil.seededRandom() * ballVelStepY_;
				paddleVelX = paddleVelMinX_ + (paddleVelNumX_ - 1) * EngineUtil.seededRandom() * paddleVelStepX_;
				paddleVelY = paddleVelMinY_ + (paddleVelNumY_ - 1) * EngineUtil.seededRandom() * paddleVelStepY_;
			} else {
				ballCfg.posX_ = ballPosMinX_ + Math.round((ballPosNumX_ - 1) * EngineUtil.seededRandom()) * ballPosStepX_;
				ballCfg.posY_ = ballPosMinY_ + Math.round((ballPosNumY_ - 1) * EngineUtil.seededRandom()) * ballPosStepY_;
				ballCfg.velX_ = ballVelMinX_ + Math.round((ballVelNumX_ - 1) * EngineUtil.seededRandom()) * ballVelStepX_;
				ballCfg.velY_ = ballVelMinY_ + Math.round((ballVelNumY_ - 1) * EngineUtil.seededRandom()) * ballVelStepY_;
				paddleVelX = paddleVelMinX_ + Math.round((paddleVelNumX_ - 1) * EngineUtil.seededRandom()) * paddleVelStepX_;
				paddleVelY = paddleVelMinY_ + Math.round((paddleVelNumY_ - 1) * EngineUtil.seededRandom()) * paddleVelStepY_;
			}
			var hasErrorStr:String = hasPaddlePitchEstimated(ballCfg, paddleVelX, paddleVelY);
			if (hasErrorStr != "") {
				numFail++;
				continue;
			}
			var pitchEstimated:Number = getPaddlePitchEstimated(ballCfg, paddleVelX, paddleVelY, continuousSpace);
			if (isNaN(pitchEstimated)) {
				continue;
			}
			var returnType:int = estimateHitPath_.run(ballCfg, paddleVelX, paddleVelY, pitchEstimated);
			if (returnType != 0) {
				trace("E   pitchEstimated: " + pitchEstimated + " estimate run return type: " + returnType + 
					" ballCfg: " + ballCfg.getInfoString() + " paddle vel " + paddleVelX + ", " + paddleVelY); 
				numFail++;
				continue;
			}
			numSuccess++;
		}
		trace("   verify success num: " + numSuccess + ", fail: " + numFail + " pitch(es)");
		TRACE_DEBUG_MSG = TRACE_DEBUG_MSG_before;
		EstimateHitPath.TRACE_DEBUG_MSG_SEARCH = TRACE_DEBUG_MSG_SEARCH_before;
		if (continuousSpace) {
			return numFail < numSuccess * 0.1;
		} else {
			return numFail == 0;
		}
	}

	public function hasPaddlePitchEstimated(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number):String {
		estimateRangeErrorStr_ = "";
		if (ballCfg.posX_ < ballPosMinX_) {
			estimateRangeErrorStr_ += " ballPosX: " + EngineUtil.roundBrief(ballCfg.posX_) + " < " + EngineUtil.roundBrief(ballPosMinX_);
		}
		if (ballCfg.posX_ > ballPosMaxCalcX_) {
			estimateRangeErrorStr_ += " ballPosX: " + EngineUtil.roundBrief(ballCfg.posX_) + " > " + EngineUtil.roundBrief(ballPosMaxCalcX_);
		}
		if (ballCfg.posY_ < ballPosMinY_) {
			estimateRangeErrorStr_ += " ballPosY: " + EngineUtil.roundBrief(ballCfg.posY_) + " < " + EngineUtil.roundBrief(ballPosMinY_);
		}
		if (ballCfg.posY_ > ballPosMaxCalcY_) {
			estimateRangeErrorStr_ += " ballPosY: " + EngineUtil.roundBrief(ballCfg.posY_) + " > " + EngineUtil.roundBrief(ballPosMaxCalcY_);
		}
		if (ballCfg.velX_ < ballVelMinX_) {
			estimateRangeErrorStr_ += " ballVelX: " + EngineUtil.roundBrief(ballCfg.velX_) + " < " + EngineUtil.roundBrief(ballVelMinX_);
		}
		if (ballCfg.velX_ > ballVelMaxCalcX_) {
			estimateRangeErrorStr_ += " ballVelX: " + EngineUtil.roundBrief(ballCfg.velX_) + " > " + EngineUtil.roundBrief(ballVelMaxCalcX_);
		}
		if (ballCfg.velY_ < ballVelMinY_) {
			estimateRangeErrorStr_ += " ballVelY: " + EngineUtil.roundBrief(ballCfg.velY_) + " < " + EngineUtil.roundBrief(ballVelMinY_);
		}
		if (ballCfg.velY_ > ballVelMaxCalcY_) {
			estimateRangeErrorStr_ += " ballVelY: " + EngineUtil.roundBrief(ballCfg.velY_) + " > " + EngineUtil.roundBrief(ballVelMaxCalcY_);
		}
		if (paddleVelX < paddleVelMinX_) {
			estimateRangeErrorStr_ += " paddleVelX: " + EngineUtil.roundBrief(paddleVelX) + " < " + EngineUtil.roundBrief(paddleVelMinX_);
		}
		if (paddleVelX > paddleVelMaxCalcX_) {
			estimateRangeErrorStr_ += " paddleVelX: " + EngineUtil.roundBrief(paddleVelX) + " > " + EngineUtil.roundBrief(paddleVelMaxCalcX_);
		}
		if (paddleVelY < paddleVelMinY_) {
			estimateRangeErrorStr_ += " paddleVelY: " + EngineUtil.roundBrief(paddleVelY) + " < " + EngineUtil.roundBrief(paddleVelMinY_);
		}
		if (paddleVelY > paddleVelMaxCalcY_) {
			estimateRangeErrorStr_ += " paddleVelY: " + EngineUtil.roundBrief(paddleVelY) + " > " + EngineUtil.roundBrief(paddleVelMaxCalcY_);
		}
		return estimateRangeErrorStr_;
	}
	
	public function getPaddlePitchEstimated(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number, searchNeighbor:Boolean):Number {
		// if min = 0.1, step = 0.2, max = 0.5, then
		// 0 ~ 0.2: interval 0, 0.2 ~ 0.4: interval 1, 0.4 ~ 0.6: interval 2
		var arrayIndexPosX:int = Math.round((ballCfg.posX_ - ballPosMinX_) / ballPosStepX_);
		if (arrayIndexPosX < 0 || arrayIndexPosX >= ballPosNumX_) {
			throw new Error();
		}
		var arrayIndexPosY:int = Math.round((ballCfg.posY_ - ballPosMinY_) / ballPosStepY_);
		if (arrayIndexPosY < 0 || arrayIndexPosY >= ballPosNumY_) {
			throw new Error();
		}
		var arrayIndexBallVelX:int = Math.round((ballCfg.velX_ - ballVelMinX_) / ballVelStepX_);
		if (arrayIndexBallVelX < 0 || arrayIndexBallVelX >= ballVelNumX_) {
			throw new Error();
		}
		var arrayIndexBallVelY:int = Math.round((ballCfg.velY_ - ballVelMinY_) / ballVelStepY_);
		if (arrayIndexBallVelY < 0 || arrayIndexBallVelY >= ballVelNumY_) {
			throw new Error();
		}
		var arrayIndexPaddleVelX:int = Math.round((paddleVelX - paddleVelMinX_) / paddleVelStepX_); 
		if (arrayIndexPaddleVelX < 0 || arrayIndexPaddleVelX >= paddleVelNumX_) {
			throw new Error();
		}
		var arrayIndexPaddleVelY:int = Math.round((paddleVelY - paddleVelMinY_) / paddleVelStepY_); 
		if (arrayIndexPaddleVelY < 0 || arrayIndexPaddleVelY >= paddleVelNumY_) {
			throw new Error();
		}
		var arrayIndex:int = 
			arrayIndexPosX * (ballPosNumY_ * ballVelNumX_ * ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_) + 
			arrayIndexPosY * (ballVelNumX_ * ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_) + 
			arrayIndexBallVelX * (ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_) + 
			arrayIndexBallVelY * (paddleVelNumX_ * paddleVelNumY_) +
			arrayIndexPaddleVelX * paddleVelNumY_ +
			arrayIndexPaddleVelY;
		if (arrayIndex > numEstimates_) {
			throw new Error();
		}
		
		var pitchFromArray:Number = getPitchFromByteArray(arrayIndex);
		if (TRACE_DEBUG_MSG) {
			trace("    estimate pitch step 1, pitchFromArray: " + EngineUtil.roundBrief(pitchFromArray)); 
		}
		if (searchNeighbor && !isNaN(pitchFromArray)) {
			var pitchEstimate:Number = estimateHitPath_.searchNeighbor(ballCfg, paddleVelX, paddleVelY, pitchFromArray, paddlePitchStep_);
			return pitchEstimate;
		} else {
			return pitchFromArray;
		}
	}
	
	public function writeToFileStream(fileStream:FileStream):void {
		var startTime:int = getTimer();
		trace("I writing " + numEstimates_ + " data to file stream");
		fileStream.writeInt(numEstimates_);
		fileStream.writeFloat(ballPosMinX_);
		fileStream.writeFloat(ballPosStepX_);
		fileStream.writeInt(ballPosNumX_);
		fileStream.writeFloat(ballPosMinY_);
		fileStream.writeFloat(ballPosStepY_);
		fileStream.writeInt(ballPosNumY_);
		fileStream.writeFloat(ballVelMinX_);
		fileStream.writeFloat(ballVelStepX_);
		fileStream.writeInt(ballVelNumX_);
		fileStream.writeFloat(ballVelMinY_);
		fileStream.writeFloat(ballVelStepY_);
		fileStream.writeInt(ballVelNumY_);
		fileStream.writeFloat(paddleVelMinX_);
		fileStream.writeFloat(paddleVelStepX_);
		fileStream.writeInt(paddleVelNumX_);
		fileStream.writeFloat(paddleVelMinY_);
		fileStream.writeFloat(paddleVelStepY_);
		fileStream.writeInt(paddleVelNumY_);
		fileStream.writeInt(paddlePitchMin_);
		fileStream.writeInt(paddlePitchMax_);
		fileStream.writeInt(paddlePitchStep_);
		var byteArraySizeBefore:int = foundPitchesByte_.length;
		trace("  compressing...");
		foundPitchesByte_.compress();
		trace("  done, byte array size: " + foundPitchesByte_.length + 
			", " + (foundPitchesByte_.length / byteArraySizeBefore * 100) + "%" + 
			", elapsed time: " + EngineUtil.roundBrief((getTimer() - startTime) / 1000) + "sec");
		fileStream.writeInt(foundPitchesByte_.length);
		fileStream.writeBytes(foundPitchesByte_);
		var sum:int = 0;
		for (var i:int = 0; i < foundPitchesByte_.length; i++) {
			sum += foundPitchesByte_[i];
		}
		fileStream.writeInt(sum);
		trace("  uncompressing...");
		foundPitchesByte_.uncompress();
		trace("   elapsed time: " + EngineUtil.roundBrief((getTimer() - startTime) / 1000) + "sec, pitch sum: " + sum);
	}
	
	public function readFromFileStream(fileStream:FileStream):Boolean {
		var startTime:int = getTimer();
		numEstimates_ = fileStream.readInt(); 
		if ((numEstimates_ < 100) || (numEstimates_ > 10000000)) {
			trace("E numEstimatesActual_ is " + numEstimates_ + " and seems to be wrong...");
			return false;
		}
		trace("I reading " + numEstimates_ + " paddle pitch data from file stream..."); 
		ballPosMinX_ = fileStream.readFloat(); 
		if (ballPosMinX_ < -10 || ballPosMinX_ > 0) {
			trace("E ballPosMinX_ is " + ballPosMinX_ + " and seems to be wrong...");
			return false;
		}
		ballPosStepX_ = fileStream.readFloat();
		if (ballPosStepX_ < 0.0001 || ballPosStepX_ > 1) {
			trace("E ballPosStepX_ is " + ballPosStepX_ + " and seems to be wrong...");
			return false;
		}
		ballPosNumX_ = fileStream.readInt();
		if (ballPosNumX_ < 2 || ballPosNumX_ > 1000) {
			trace("E ballPosNumX_ is " + ballPosNumX_ + " and seems to be wrong...");
			return false;
		}
		ballPosMinY_ = fileStream.readFloat();
		ballPosStepY_ = fileStream.readFloat();
		ballPosNumY_ = fileStream.readInt();
		ballVelMinX_ = fileStream.readFloat();
		ballVelStepX_ = fileStream.readFloat();
		ballVelNumX_ = fileStream.readInt();
		ballVelMinY_ = fileStream.readFloat();
		ballVelStepY_ = fileStream.readFloat();
		ballVelNumY_ = fileStream.readInt();
		paddleVelMinX_ = fileStream.readFloat();
		paddleVelStepX_ = fileStream.readFloat();
		paddleVelNumX_ = fileStream.readInt();
		paddleVelMinY_ = fileStream.readFloat();
		paddleVelStepY_ = fileStream.readFloat();
		paddleVelNumY_ = fileStream.readInt();
		if (ballPosNumX_ * ballPosNumY_ * ballVelNumX_ * ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_ != numEstimates_) {
			trace("E numEstimates_ " + numEstimates_ + " != " + ballPosNumX_ * ballPosNumY_ * ballVelNumX_ * ballVelNumY_ * paddleVelNumX_ * paddleVelNumY_);
			return false;
		}
		paddlePitchMin_ = fileStream.readInt(); 
		paddlePitchMax_ = fileStream.readInt(); 
		paddlePitchStep_ = fileStream.readInt();
		calcInclusiveMaxValues();
		foundPitchesByte_ = new ByteArray();
		var pitchByteArrayLen:int = fileStream.readInt();
		fileStream.readBytes(foundPitchesByte_, 0, pitchByteArrayLen);
		var sum:int = fileStream.readInt();
		trace("  pitch sum expected: " + sum);
		var sumActual:int = 0;
		for (var i:int = 0; i < foundPitchesByte_.length; i++) {
			sumActual += foundPitchesByte_[i];
		}
		if (sum != sumActual) {
			trace("E sum " + sum + " != sumActual " + sumActual);  
			return false;
		}
		
		trace("  uncompressing...");
		foundPitchesByte_.uncompress();
		trace("  done, byte array size: " + foundPitchesByte_.length + 
			", elapsed time: " + EngineUtil.roundBrief((getTimer() - startTime) / 1000) + "sec");
		
		if (TRACE_DEBUG_MSG) {
			trace("   -------------------------------------------------\n" +
				  "   basic tests passed after reading data from file. Initial values:");
			traceEstimateInitialValues();
		}

		if (VERIFY_AFTER_READ) {
			trace("I verifying estimated pitches from file... step 1. in discrete space");
			!verifyArrayGet(false);
			trace("I verifying estimated pitches from file... step 2. in continuous space");
			verifyArrayGet(true);
		}
		
		return true;
	}
	
	public function getPaddleVelMinX():Number {
		return paddleVelMinX_;
	}
	
	public function getPaddleVelMaxX():Number {
		return paddleVelMaxCalcX_;
	}
	
	public function getPaddleVelMinY():Number {
		return paddleVelMinY_;
	}
	
	public function getPaddleVelMaxY():Number {
		return paddleVelMaxCalcY_;
	}
	
	public function getPaddlePitchMin():Number {
		return paddlePitchMin_;
	}
	
	public function getPaddlePitchMax():Number {
		return paddlePitchMax_;
	}
}
}
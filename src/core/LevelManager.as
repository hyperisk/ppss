package core
{
import level.AiPlayerTable;
import level.ILevel;
import level.Level_1;
import level.Level_2;
import level.Level_3;
import level.Level_4;

import renderer.LevelWidgetRenderer;
import renderer.RendererManager;
import renderer.UserControlWidget;

import simulation.SimBall;
import simulation.SimWorld;

public class LevelManager implements IFrameUpdateObject
{
	public static const NUM_LEVELS:int = 4;
	public var rendererManager_:RendererManager;
	public var aiPlayerTableLeft_:AiPlayerTable;
	public var simWorld_:SimWorld;
	public var aiPlayerTableRight_:AiPlayerTable;
	public var totalNumLevelsCreated_:int; 
	public var levelUserInputLeft_:UserControlWidget;
	public var levelUserInputRight_:UserControlWidget;
	private var currentLevel_:ILevel;

	public function LevelManager(simWorld:SimWorld, rendererManager:RendererManager)
	{
		rendererManager_ = rendererManager;
		totalNumLevelsCreated_ = 0;
		simWorld_ = simWorld;
		currentLevel_ = null; 
		levelUserInputLeft_ = new UserControlWidget(simWorld_, SimBall.LEFT_SIDE);
		levelUserInputRight_ = new UserControlWidget(simWorld_, SimBall.RIGHT_SIDE);
		aiPlayerTableLeft_ = new AiPlayerTable(simWorld_, SimBall.LEFT_SIDE);
		aiPlayerTableRight_ = new AiPlayerTable(simWorld_, SimBall.RIGHT_SIDE);
		rendererManager_.createLevelRenders(this);
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}
	
	public function showLevelMenu():void {
		rendererManager_.showLevelMenu(onLevelSelected, NUM_LEVELS);
	}
	
	public function onLevelSelected(levelIndex:int):void {
		if (currentLevel_) {
			throw new Error("current level is not null in startLevel"); 
		}
		
		totalNumLevelsCreated_++;
		switch (levelIndex) {
			case 0:
				currentLevel_ = new Level_1();
				break;
			case 1:
				currentLevel_ = new Level_2();
				break;
			case 2:
				currentLevel_ = new Level_3();
				break;
			case 3:
				currentLevel_ = new Level_4();
				break;
			
			default:
				throw new Error("unhandled level index " + levelIndex);
		}
		currentLevel_.init(this);
	}
	
	public function quitCurrentLevel():void {
		if (!currentLevel_) {
			throw new Error("current level is null in quitLevel"); 
		}	
		
		currentLevel_.cleanUp();
		currentLevel_ = null;
	}
	
	public function onLevelWidgetPressed(widgetType:String):void {
		if (widgetType == LevelWidgetRenderer.BUTTON_REPLAY) {
			currentLevel_.restart();
		} else if (widgetType == LevelWidgetRenderer.BUTTON_QUIT) {
			quitCurrentLevel();
			showLevelMenu();
		}
	}
			
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "level_manager";
	}	
}
}
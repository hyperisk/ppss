package core
{
import renderer.LoadingRenderer;
import renderer.RendererManager;

import simulation.SimWorld;

public class LoadingManager implements IFrameUpdateObject
{
	private var rendererManager_:RendererManager;
	private var loadCompleteCallbackFunc_:Function;
	private var loadingRenderer_:LoadingRenderer;
	private static const TRACE_DEBUG_MSG:Boolean = false;
	
	public function LoadingManager(rendererManager:RendererManager) {
		rendererManager_ = rendererManager;
	}
	
	public function loadAll(completeCallback:Function):void {
		loadCompleteCallbackFunc_ = completeCallback;
		StageUtil.getSingleton().addFrameUpdateObject(this);
		rendererManager_.showLoading();
		ImageLoader.getSingleton().loadAllImages(onImagesLoaded);
	}
	
	private function onImagesLoaded():void {
		rendererManager_.updateLoadingStatus(10);
		DbUtil.getSingleton().open(onDbOpened);
	}

	private function onDbOpened():void {
		rendererManager_.updateLoadingStatus(20);
		GameConfig.getSingleton().init(onGameConfigInit);
	}

	private function onGameConfigInit(success:Boolean):void {
		rendererManager_.updateLoadingStatus(50);
		StageUtil.getSingleton().onRenderFpsTargetChanged();
		rendererManager_.createSetupItems();
		
		createPaddlePitchDatabase();
	}
	
	private function createPaddlePitchDatabase():void {
		var success:Boolean = PaddlePitchFileCache.getSingleton().getPitchData();
		if (!success && GameConfig.isWindowsOS_) {
			success = PaddlePitchFileCache.getSingleton().createNew();
			if (!success) {
				onLoadComplete(false);
			}
		}
		onLoadComplete(true);
	}
	
	private function onLoadComplete(success:Boolean):void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		loadCompleteCallbackFunc_();
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		if (TRACE_DEBUG_MSG) {
			trace("I loading ... onFrameEnter");
		}
		return true;
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_SIM + "loading_manager";
	}
}
}
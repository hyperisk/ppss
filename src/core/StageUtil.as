package core
{
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.display.Stage;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.system.System;
import flash.utils.Dictionary;
import flash.utils.getTimer;

// to add debugging message to stage:
// StageUtil.getSingleton().infoText_.setText(title, message)

public class StageUtil
{
	static private var instance_ : StageUtil = null;
	
	public var stageWidth_:int;	// to check if stage resize event actually changed the size
	public var stageHeight_:int;
	private var stage_:Stage;
	
	public var userInput_:UserInputUtil;
	public var infoText_:InfoText;
	private static const TRACE_DEBUG_MSG:Boolean = false;
	
	private var frameNumber_:int;
	private var lastFrameUpdateTimeMsec_:int;
	public var fpsAvg_:int;
	public var curFrameStartTimeMsec_:int;
	public var lastFpsUpdateTimeMsec_:int;
	private var sumFrameSpentTimeMsec_:int;
	private var maxFrameSpentTimeMsec_:int;
	private var numFramesSinceLastUpdate_:int;
	private var sumElapsedTimesMsec_:Number;
	public var pauseAutoRegulateTargetFps_:Boolean;
	
	private var frameUpdateObjects_:Array;
	public static const FRAMEUPDATE_KEY_USER:String = "1_user_";
	public static const FRAMEUPDATE_KEY_SIM:String = "2_sim_";
	public static const FRAMEUPDATE_KEY_RENDER:String = "3_render_";

	private var displayObjectsVisual_:Sprite;
	private var displayObjectsUserInput_:Sprite;
	private var displayObjectsDebugInfo_:Sprite;
	public static const STAGE_OBJECT_GROUP_VISUAL:int = 1;
	public static const STAGE_OBJECT_GROUP_USERINPUT:int = 2;
	public static const STAGE_OBJECT_GROUP_DEBUGINFO:int = 3;
	
	static public function getSingleton() : StageUtil {
		if ( instance_ == null ) instance_ = new StageUtil( new Lock() );
		return instance_;
	}
	
	public function StageUtil(lock : Lock) {
		if ( lock == null ) throw new Error("Singleton not allowed bla bla bla");
		frameUpdateObjects_ = new Array();
	}		
	
	public function init(stage:Stage):void
	{
		stage_ = stage;
		if (stage.stageWidth == 0) {
			throw new Error("stage width is 0");
		}
		stageWidth_ = stage.stageWidth;
		stageHeight_ = stage.stageHeight;
		trace("I initializing StageUtil, width: " + stageWidth_ + ", height: " + stageHeight_);

		displayObjectsVisual_ = new Sprite();
		stage_.addChild(displayObjectsVisual_);
		displayObjectsUserInput_ = new Sprite();
		stage_.addChild(displayObjectsUserInput_);
		displayObjectsDebugInfo_ = new Sprite();
		stage_.addChild(displayObjectsDebugInfo_);

		infoText_ = new InfoText();
		userInput_ = new UserInputUtil(stage_, infoText_);
		
		frameNumber_ = 0;
		numFramesSinceLastUpdate_ = 0;
		lastFrameUpdateTimeMsec_ = getTimer();
		lastFpsUpdateTimeMsec_ = lastFrameUpdateTimeMsec_;
		fpsAvg_ = 0;
		sumElapsedTimesMsec_ = 0;
		sumFrameSpentTimeMsec_ = 0;
		maxFrameSpentTimeMsec_ = 0;
		pauseAutoRegulateTargetFps_ = true;
		
		stage_.align = StageAlign.TOP_LEFT;
		stage_.scaleMode = StageScaleMode.NO_SCALE;
		stage_.addEventListener(Event.ENTER_FRAME, onFrameEnter);
		
		stage_.addEventListener(Event.ADDED, onObjectAddedToStage);
		stage_.addEventListener(Event.REMOVED, onObjectRemovedFromStage);
	}
	
	public function onStageResizedAfterInit(event:Event):void {
		var stage:Stage = event.target as Stage;
		if ((stage.stageWidth != StageUtil.getSingleton().stageWidth_) ||
			(stage.stageHeight != StageUtil.getSingleton().stageHeight_)) {
			
			// TODO: do not throw if in release mode
			throw new Error("unhandled event stage resized (after game start), new width: " + stage.stageWidth
				+ ", new height: " + stage.stageHeight);
		}
	}
	
	public function onStageOrientationChanged(event:Event):void {
		// TODO: do not throw if in release mode
		throw new Error("unhandled event stage ori changed (after game start)");
	}
	
	public function addToStage(o:DisplayObject, group:int=STAGE_OBJECT_GROUP_VISUAL):void {
		if (group == STAGE_OBJECT_GROUP_VISUAL) {
			displayObjectsVisual_.addChild(o);
		} else if (group == STAGE_OBJECT_GROUP_USERINPUT) {
			displayObjectsUserInput_.addChild(o);
		} else if (group == STAGE_OBJECT_GROUP_DEBUGINFO) {
			displayObjectsDebugInfo_.addChild(o);
		} else {
			throw new Error("addToStage invalid group");
		}
	}
	
	public function removeFromStage(o:DisplayObject):void {
		if (displayObjectsVisual_.contains(o)) {
			displayObjectsVisual_.removeChild(o);
		} else if (displayObjectsUserInput_.contains(o)) {
			displayObjectsUserInput_.removeChild(o);
		} else if (displayObjectsDebugInfo_.contains(o)) {
			displayObjectsDebugInfo_.removeChild(o);
		} else {
			throw new Error("removeFromStage invalid object");
		}
	}
	
	private function onObjectAddedToStage(event:Event):void {
		var obj:DisplayObject = event.target as DisplayObject;
		//trace("I added to stage: " + obj); 
	}
	
	private function onObjectRemovedFromStage(event:Event):void {
		var obj:DisplayObject = event.target as DisplayObject;
		//trace("I removed from stage: " + obj); 
	}
	
	public function onRenderFpsTargetChanged():void {
		stage_.frameRate = GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET); 
	}
	
	public function addFrameUpdateObject(o:IFrameUpdateObject):void {
		if (!o.toString()) {
			throw new Error("frame update object invalid key string"); 
		}
		if (frameUpdateObjects_.indexOf(o) >= 0) {
			throw new Error("frame update object already added: " + o.toString()); 
		}
		frameUpdateObjects_.push(o);
		frameUpdateObjects_.sort();
	}
	
	public function removeFrameUpdateObject(o:IFrameUpdateObject):void {
		var index:int = frameUpdateObjects_.indexOf(o); 
		if (index == -1) {
			throw new Error("frame update object not found: " + o.toString()); 
		}
		frameUpdateObjects_.splice(index, 1);
	}
	
	private function onFrameEnter(event:Event):void {
		if (userInput_.keyDownCharCode_ == 32) {
			var pauseLenMsec:int = getTimer() - lastFrameUpdateTimeMsec_; 
			lastFrameUpdateTimeMsec_ += pauseLenMsec;
			lastFpsUpdateTimeMsec_ += pauseLenMsec;
			return;
		}
		frameNumber_++;
		curFrameStartTimeMsec_ = getTimer();
		var frameElapsedTimeMsec:int = curFrameStartTimeMsec_ - lastFrameUpdateTimeMsec_;
		var fpsUpdateElapsedMsec:int = curFrameStartTimeMsec_ - lastFpsUpdateTimeMsec_;
		updateFrameAllObjects(curFrameStartTimeMsec_, frameElapsedTimeMsec / 1000);
		
		if (curFrameStartTimeMsec_ - lastFpsUpdateTimeMsec_ > 1000) {
			lastFpsUpdateTimeMsec_ = curFrameStartTimeMsec_;
			fpsAvg_ = numFramesSinceLastUpdate_;
			var totalElapsedTimeAccuracyPercent:int = sumElapsedTimesMsec_ / 10;
			var highlight:Boolean = fpsAvg_ < 0.9 * GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET);
			var frameSpentTimeMsecAvg:Number = sumFrameSpentTimeMsec_ / numFramesSinceLastUpdate_; 
			var frameSpentTimeFrameTimePercent:Number = frameSpentTimeMsecAvg / 1000 / (1/fpsAvg_) * 100; 
			infoText_.setText("frame updater", "fps avg: " + fpsAvg_ + ", spent time: " + 
				EngineUtil.roundBrief(frameSpentTimeMsecAvg) + 
				"msec (" + EngineUtil.roundBrief(frameSpentTimeFrameTimePercent)  + "%)" + 
				", max spent: " + maxFrameSpentTimeMsec_ + "msec" +
				", accuracy: " +  totalElapsedTimeAccuracyPercent + "%" + 
				", phys fps: " + GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET)
				, highlight);
			if (GameConfig.USE_AUTO_FPS_REGULATING && !pauseAutoRegulateTargetFps_) {
				regulateTargetFps(totalElapsedTimeAccuracyPercent);
			}
			numFramesSinceLastUpdate_ = 0;
			sumElapsedTimesMsec_ = 0;
			sumFrameSpentTimeMsec_ = 0;
			maxFrameSpentTimeMsec_ = 0;
		} else {
			numFramesSinceLastUpdate_++;
			sumElapsedTimesMsec_ += frameElapsedTimeMsec;
		}
		lastFrameUpdateTimeMsec_ = curFrameStartTimeMsec_;
	}
	
	private function updateFrameAllObjects(frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		// iterate throw values in dictionary!
		// compared to foreach (var k:KeyType...)
		for each (var o:Object in frameUpdateObjects_) {
			var fuo:IFrameUpdateObject = o as IFrameUpdateObject;
			if (!fuo) {
				continue;
				//throw new Error("frame update object cast failed");
			}
			if (!fuo.onFrameUpdate(frameNumber_, frameStartTimeMsec, frameElapsedTime)) {
				trace("I abort frame update by " + fuo);
				return false;
			}
		}
		var thisFrameSpentTimeMsec:int = getTimer() - frameStartTimeMsec;
		sumFrameSpentTimeMsec_ += thisFrameSpentTimeMsec;
		maxFrameSpentTimeMsec_ = (thisFrameSpentTimeMsec > maxFrameSpentTimeMsec_) ? 
			thisFrameSpentTimeMsec : maxFrameSpentTimeMsec_;
		return true;
	}
	
	private function regulateTargetFps(totalElapsedTimeAccuracyPercent:int):void {
		var renderFpsTarget:int = GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET);
		var physicsFpsTarget:int = GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET);
		if (DbUtil.getSingleton().isExecuting()) {
			return;
		}
		if (totalElapsedTimeAccuracyPercent > GameConfig.RENDER_FPS_ACCURACY_HIGH) {
			if (renderFpsTarget < (GameConfig.RENDER_FPS_TARGET_MAX - GameConfig.FPS_TARGET_UP)) {
				GameConfig.getSingleton().setValue(GameConfig.RENDER_FPS_TARGET,
					renderFpsTarget + GameConfig.FPS_TARGET_UP,
					function():void {
						if (TRACE_DEBUG_MSG) {
							trace("I changed render fps target to " + 
								GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET));
						}
						onRenderFpsTargetChanged();
					}
				);
			} else if (physicsFpsTarget < renderFpsTarget * GameConfig.PHYSICS_FPS_RENDER_FPS_RATIO_HIGH) {
				GameConfig.getSingleton().setValue(GameConfig.PHYSICS_SIM_FPS_TAGET,
					physicsFpsTarget + 1,
					function():void {
						if (TRACE_DEBUG_MSG) {
							trace("I changed physics fps target to " + 
								GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET));
						}
					}
				);
			}
		} else if (totalElapsedTimeAccuracyPercent <  GameConfig.RENDER_FPS_ACCURACY_LOW) {
			if (physicsFpsTarget > renderFpsTarget * GameConfig.PHYSICS_FPS_RENDER_FPS_RATIO_LOW) {
				GameConfig.getSingleton().setValue(GameConfig.PHYSICS_SIM_FPS_TAGET,
					renderFpsTarget * GameConfig.PHYSICS_FPS_RENDER_FPS_RATIO_LOW,
					function():void {
						if (TRACE_DEBUG_MSG) {
							trace("I changed physics fps target to " + 
								GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET));
						}
					}
				);
			} else if (renderFpsTarget > GameConfig.RENDER_FPS_TARGET_MIN) {
				GameConfig.getSingleton().setValue(GameConfig.RENDER_FPS_TARGET,
					renderFpsTarget - GameConfig.FPS_TARGET_DOWN,
					function():void {
						if (TRACE_DEBUG_MSG) {
							trace("I changed render fps target to " + 
								GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET));
						}
						onRenderFpsTargetChanged();
					}
				);
			}
		} else if (physicsFpsTarget > GameConfig.RENDER_FPS_TARGET_MAX) {
			GameConfig.getSingleton().setValue(GameConfig.PHYSICS_SIM_FPS_TAGET,
				GameConfig.RENDER_FPS_TARGET_MAX,
				function():void {
					if (TRACE_DEBUG_MSG) {
						trace("I (debug) changed physics fps target to " + 
							GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET));
					}
				}
			);
		} else if (renderFpsTarget > GameConfig.RENDER_FPS_TARGET_MAX) {
			GameConfig.getSingleton().setValue(GameConfig.RENDER_FPS_TARGET,
				GameConfig.RENDER_FPS_TARGET_MAX,
				function():void {
					if (TRACE_DEBUG_MSG) {
						trace("I (debug) changed render fps target to " + 
							GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET));
					}
					onRenderFpsTargetChanged();
				}
			);
		} else if (renderFpsTarget < GameConfig.RENDER_FPS_TARGET_MIN) {
			GameConfig.getSingleton().setValue(GameConfig.RENDER_FPS_TARGET,
				GameConfig.RENDER_FPS_TARGET_MIN,
				function():void {
					if (TRACE_DEBUG_MSG) {
						trace("I (debug) changed render fps target to " + 
							GameConfig.getSingleton().getValue(GameConfig.RENDER_FPS_TARGET));
					}
					onRenderFpsTargetChanged();
				}
			);
		} else if (physicsFpsTarget < GameConfig.RENDER_FPS_TARGET_MIN) {
			GameConfig.getSingleton().setValue(GameConfig.PHYSICS_SIM_FPS_TAGET,
				GameConfig.RENDER_FPS_TARGET_MIN,
				function():void {
					if (TRACE_DEBUG_MSG) {
						trace("I (debug) changed physics fps target to " + 
							GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET));
					}
				}
			);
		} 
	}
}
}

internal class Lock{}
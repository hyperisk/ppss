package core
{
import flash.geom.Point;

public class EngineUtil {
	public static var t1_:Number;
	public static var t2_:Number;
	public static var randomSeed_:int = 777;

	public static const IntNaN:int = -99999;
	
	public static function rgbToInt(r:int, g:int, b:int):int {
		return r << 16 | g << 8 | b << 0;
	}
	
	public static function childrenToString(obj:Object):String {
		var s:String;
		for (var i:int = 0; i < obj.numChildren; i++) {
			var childStr:String = obj.getChildAt(i).toString();
			childStr = childStr.substring(childStr.indexOf(" "), (childStr.length - 1));
			s += childStr + " ";
		}
		return s;
	}
	
	public static function roundBrief(input:Number):Number {
		var mulStep:Number;
		if (Math.abs(input) < 0.0001) {
			mulStep = 1000000;
		} else if (Math.abs(input) < 0.01) {
			mulStep = 10000;
		} else {
			mulStep = 100;
		}
		input = input * mulStep;
		input = Math.round(input);
		input = input / mulStep;
		return input;
	}
	
	public static function getNumObjectKeys(object:Object, type:Class):int {
		var num:int = 0;
		for (var key:String in object) {
			var classObj:Object = object[key];
			var classObjCast:Object = classObj as type;
			if (classObjCast) {
				num++;
			}
		}
		return num;
	}
	
	public static function rotateVec(x:Number, y:Number, rad:Number):void {
		var sin:Number;
		var cos:Number
		if (Math.abs(rad - 90 * Math.PI / 180) < 0.01) {
			sin = 1;
			cos = 0;
		} else if (Math.abs(rad + 90 * Math.PI / 180) < 0.01) {
			sin = -1;
			cos = 0;
		} else {
			sin = Math.sin(rad); 
			cos = Math.cos(rad);
		}
		t1_ = cos * x - sin * y;
		t2_ = sin * x + cos * y;
	}
	
	public static function getRoughCos(rad:Number):Number {
		return 1 - (rad * rad / 2);
	}
	
	public static function lerpPoints(dataPoints:Array, input:Number):Number {
		if (dataPoints.length < 2) {
			throw new Error();
		}
		for (var i:int = 0; i < dataPoints.length - 1; i++) {
			var data1:Point = dataPoints[i];
			var data2:Point = dataPoints[i + 1];
			if (data1.x >= data2.x) {
				throw new Error();
			}
			if ((i == 0) && (input < data1.x)) {
				return data1.y;
			} else if ((i == dataPoints.length - 2) && (input >= data2.x)) {
				return data2.y;
			} else if ((data1.x <= input) && (input < data2.x)) {
				var f:Number = (data2.x - input) / (data2.x - data1.x); 
				return Point.interpolate(data1, data2, f).y;
			}
		}
		throw new Error();
	}
	
	public static function seededRandom():Number {
		const MAX_RATIO:Number = 1 / int.MAX_VALUE;
		const MIN_MAX_RATIO:Number = -MAX_RATIO;
		
		randomSeed_ ^= (randomSeed_ << 21);
		randomSeed_ ^= (randomSeed_ >>> 35);
		randomSeed_ ^= (randomSeed_ << 4);
		if (randomSeed_ < 0) return Math.abs(randomSeed_ * MAX_RATIO);
		return Math.abs(randomSeed_ * MIN_MAX_RATIO);
	}
	
	// help set initial min max values (float) so that num interval (integer) is numerically stable
	// example: min 0.1 max 0.5 step 0.2 is sate -- with num intervals 2, error will be 0 or -0.00000001 or 0.00000001 or so (always rounded to 2)
	//          min 0.1 max 0.4 step 0.2 is unsafe -- with num intervals 2, error will be 50% or step, 49.9999% of step (rounded to 1), 50.000001% (rounded to 2)
	public static function getNumIntervalsFromMinMaxSafe(min:Number, max:Number, step:Number):int {
		var numIntervals:int = Math.round((max - min) / step);
		if (numIntervals < 0) {
			throw new Error();
		}
		if (numIntervals > 1000) {
			throw new Error();
		}
		var error:Number = max - (min + step * numIntervals);
		if (Math.abs(error) > Math.abs(step)) {
			throw new Error();
		}
		return numIntervals;
	}
}
}

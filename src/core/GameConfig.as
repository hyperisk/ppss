package core
{
import flash.data.SQLResult;
import flash.system.Capabilities;
import flash.utils.Dictionary;

public class GameConfig implements IFrameUpdateObject
{
	private static var instance_: GameConfig = null;
	private var configDict_:Dictionary;
	private var configMenuItems_:Array;
	private var configSystemItems_:Array;
	private const TRACE_DEBUG_MSG_C:Boolean = false;

	// un-configurable values (compile time fixed)
	public static const DEBUGGING_ENABLED:Boolean = true;
	public static const MAIN_MENU_AUTO_SELECT:Boolean = false;
	public static const BUTTON_SIZE_HEIGHT_PERCENT:int = 12;
	public static const BUTTON_GAP_HEIGHT_PERCENT:int = 7;
	public static const GAME_TITLE_WITH_SPACE:String = "Ping Pong: Spin Shot";
	public static const GAME_TITLE_WITHOUT_SPACE:String = "ping_pong_spin_shot";
	public static const USE_AUTO_FPS_REGULATING:Boolean = false;
	public static const ASSETS_DIR:String = "assets";
	
	// user configurable values
	public static const BALL_SINGLE_PADDLE_SIDE:String = "ball_single_paddle_side";
	public static const BALL_DRAW_MAGNITUDE:String = "ball_draw_magnitude";
	public static const PADDLE_MAGNITUDE:String = "paddle_magnitude";
	public static const PADDLE_HANDLE_LENGTH:String = "paddle_handle_length";
	public static const PADDLE_HANDLE_LENGTH_MAX_NUM_STEPS:int = 4;
	public static const PADDLE_HANDLE_LENGTH_TABLE_LEG_RATIO:Number = 0.5;
	public static const PHYSICS_SLOW_DOWN_PERCENT:String = "physics_slow_down_percent";
	private var states_:SimpleStateMachine;
	private var finishedCallback_:Function;
	
	// system auto config
	public static var INITIAL_FPS:int;
	public static const RENDER_FPS_TARGET:String = "render_fps_target";
	public static const RENDER_FPS_DEFAULT:int = 48;
	public static const RENDER_FPS_ACCURACY_HIGH:int = 98;
	public static const RENDER_FPS_ACCURACY_LOW:int = 90;
	public static const RENDER_FPS_TARGET_MIN:int = 20;
	public static var RENDER_FPS_TARGET_MAX:int = 60;
	public static const FPS_TARGET_UP:int = 1;
	public static const FPS_TARGET_DOWN:int = 5;
	public static const PHYSICS_SIM_FPS_TAGET:String = "physics_sim_fps_target";
	public static const PHYSICS_FPS_RENDER_FPS_DEFAULT:Number = 30;
	public static const PHYSICS_FPS_RENDER_FPS_RATIO_HIGH:Number = 1;
	public static const PHYSICS_FPS_RENDER_FPS_RATIO_LOW:Number = 0.5;
	public static var isWindowsOS_:Boolean;

	static public function getSingleton(): GameConfig {
		if ( instance_ == null ) instance_ = new GameConfig( new Lock() );
		return instance_;
	}
	
	public function GameConfig(lock: Lock) {
		if ( lock == null ) throw new Error("Singleton not allowed bla bla bla");
		
		configDict_ = new Dictionary();
		configMenuItems_ = new Array();
		configSystemItems_ = new Array();

		states_ = new SimpleStateMachine([
			['idle', 0],
			['init_item_array_and_start', 0],
			['init_next_item_busy', 0],
			['init_next_item_ready', 0]
		]);
		StageUtil.getSingleton().addFrameUpdateObject(this);
		
		trace("I capabilities, os: " + Capabilities.os + ", player type: " + Capabilities.playerType + 
			", cpu: " + Capabilities.cpuArchitecture);
		isWindowsOS_ = Capabilities.os == "Windows 7"; 
		if (isWindowsOS_) {
			RENDER_FPS_TARGET_MAX = 30;
		}
	}
	
	public function init(finishedCallback:Function):void {
		finishedCallback_ = finishedCallback;
		trace("I loading all GameConfig items...");
		states_.setActiveState('init_item_array_and_start');
	}
	
	private function readFromDbOrSetDefault(key:String, defaultValue:int):void {
		states_.setActiveState('init_next_item_busy');
		DbUtil.getSingleton().executeSql(DbUtil.SQL_GET_CONFIG, function(success:Boolean, sqlResult:SQLResult):void {
			if (success && sqlResult.complete) {
				if (sqlResult.data == null) {
					trace("I   no config value found in DB: " + valueStr + " for " + key);
					setValue(key, defaultValue, function(success:Boolean, sqlResult:SQLResult):void {
						if (!success) {
							states_.setActiveState('idle');
							finishedCallback_(false);
						} else {
							states_.setActiveState('init_next_item_ready');
						}
					});
				} else {
					var valueStr:String = sqlResult.data[0]['value'];
					configDict_[key] = parseInt(valueStr);
					if (TRACE_DEBUG_MSG_C) {
						trace("I   got config value " + valueStr + " for " + key);
					}
					states_.setActiveState('init_next_item_ready');
				}
			} else {
				trace("I   error reading from in DB: " + valueStr + " for " + key);
				states_.setActiveState('idle');
				finishedCallback_(false);
			}
		}, [key]);
	}
	
	private function skipDbAndSetDefault(key:String, defaultValue:int):void {
		configDict_[key] = defaultValue;
		states_.setActiveState('init_next_item_ready');
	}
	
	public function setValue(key:String, value:int, finishedCallback:Function):void {
		if (TRACE_DEBUG_MSG_C) {
			trace("I   set config value " + value + " for key " + key);
		}
		if (configDict_[key] != value) {
			configDict_[key] = value;
			DbUtil.getSingleton().executeSql(DbUtil.SQL_REPLACE_CONFIG, finishedCallback, [key, value]);
		} else {
			finishedCallback();
		}
	}
	
	public function getValue(key:String):int {
		if (!key) {
			throw new Error("invalid config key string");
		}
		return configDict_[key];
	}
	
	public function getNumMenuItems():int {
		return configMenuItems_.length; 
	}
	
	public function getMenuNameFromIndex(index:int):String {
		return configMenuItems_[index];
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		if (states_.isStateActive('init_item_array_and_start')) {
			configMenuItems_.push(BALL_SINGLE_PADDLE_SIDE);
			configMenuItems_.push(BALL_DRAW_MAGNITUDE);
			configMenuItems_.push(PADDLE_MAGNITUDE);
			configMenuItems_.push(PADDLE_HANDLE_LENGTH);
			configMenuItems_.push(PHYSICS_SLOW_DOWN_PERCENT);
			configSystemItems_.push(RENDER_FPS_TARGET);
			configSystemItems_.push(PHYSICS_SIM_FPS_TAGET);
			states_.setActiveState('init_next_item_ready');
		} else if (states_.isStateActive('init_next_item_ready')) {
			if (!configDict_.hasOwnProperty(BALL_SINGLE_PADDLE_SIDE)) {
				// 1: left, 2: right
				readFromDbOrSetDefault(BALL_SINGLE_PADDLE_SIDE, 2);
			} else if (!configDict_.hasOwnProperty(BALL_DRAW_MAGNITUDE)) {
				// 1: real ball size, 2: twice diameter when draw
				readFromDbOrSetDefault(BALL_DRAW_MAGNITUDE, 1);
			} else if (!configDict_.hasOwnProperty(PADDLE_MAGNITUDE)) {
				// 1: real paddle size
				readFromDbOrSetDefault(PADDLE_MAGNITUDE, 2);
			} else if (!configDict_.hasOwnProperty(PADDLE_HANDLE_LENGTH)) {
				// 0: right on finger, MAX: finger-paddle distance of xx% of table height 
				readFromDbOrSetDefault(PADDLE_HANDLE_LENGTH, 2);
			} else if (!configDict_.hasOwnProperty(PHYSICS_SLOW_DOWN_PERCENT)) {
				// 100: no slow down, 50: half speed, 0: frozen --> actually not slow down O_o
				readFromDbOrSetDefault(PHYSICS_SLOW_DOWN_PERCENT, 80);
			} else if (!configDict_.hasOwnProperty(RENDER_FPS_TARGET)) {
				if (USE_AUTO_FPS_REGULATING) {
					readFromDbOrSetDefault(RENDER_FPS_TARGET, RENDER_FPS_DEFAULT);
				} else {
					skipDbAndSetDefault(RENDER_FPS_TARGET, RENDER_FPS_DEFAULT);
				}
			} else if (!configDict_.hasOwnProperty(PHYSICS_SIM_FPS_TAGET)) {
				if (USE_AUTO_FPS_REGULATING) {
					readFromDbOrSetDefault(PHYSICS_SIM_FPS_TAGET, PHYSICS_FPS_RENDER_FPS_DEFAULT);
				} else {
					skipDbAndSetDefault(PHYSICS_SIM_FPS_TAGET, PHYSICS_FPS_RENDER_FPS_DEFAULT);
				}
			} else {
				states_.setActiveState('idle');
				finishedCallback_(true);
			}
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "game_config";
	}	
}
}

internal class Lock{}
package core
{
import flash.errors.IOError;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;

public class PaddlePitchFileCache
{
	private static var instance_:PaddlePitchFileCache = null;
	public var paddlePitchDatabase_:PaddlePitchDatabase;
	private const DATA_FILE_NAME:String = GameConfig.ASSETS_DIR + File.separator + "paddlePitchData.bin"; 
	private const COPY_BAT_FILE_NAME:String = GameConfig.ASSETS_DIR + File.separator + "copy_data.bat"; 

	public function PaddlePitchFileCache(lock: Lock) {
		if ( lock == null ) throw new Error("Singleton not allowed bla bla bla");
		paddlePitchDatabase_ = null;
	}
	
	static public function getSingleton() : PaddlePitchFileCache {
		if ( instance_ == null ) instance_ = new PaddlePitchFileCache( new Lock() );
		return instance_;
	}
	
	public function getPitchData():Boolean {
		var dataFile:File = File.applicationDirectory;
		dataFile = dataFile.resolvePath(DATA_FILE_NAME);
		trace("I try opening PaddlePitchCache file for READ at " + dataFile.nativePath);
		var dataFileStream:FileStream = new FileStream();
		try {
			dataFileStream.open(dataFile, FileMode.READ);
		} catch (error:IOError) {
			if (error.errorID == 0xbbb) {
				trace("  ==> file not found");
				
				var appStorageFile:File = File.applicationStorageDirectory;
				appStorageFile = appStorageFile.resolvePath(DATA_FILE_NAME);
				trace("  ==> create new and copy (use batch file) from: " + appStorageFile.nativePath);
				
			} else {
				trace("E ==> " + error);
			}
			return false;
		}
		trace("  file opened " + DATA_FILE_NAME);

		if (!paddlePitchDatabase_) {
			paddlePitchDatabase_ = new PaddlePitchDatabase();
		}
		var success:Boolean = paddlePitchDatabase_.readFromFileStream(dataFileStream);
		if (!success) {
			paddlePitchDatabase_ = null;
		}
		
		dataFileStream.close();
		return success;
	}
	
	public function createNew():Boolean {
		var file:File = File.applicationStorageDirectory;
		file = file.resolvePath(DATA_FILE_NAME);
		trace("I try opening PaddlePitchCache file for WRITE at " + file.nativePath);
		var fileStream:FileStream = new FileStream();
		try {
			fileStream.open(file, FileMode.WRITE);
		} catch (error:IOError) {
			trace("E ==> " + error);
			return false;
		}

		if (!paddlePitchDatabase_) {
			paddlePitchDatabase_ = new PaddlePitchDatabase();
		}
		paddlePitchDatabase_.buildAllBySim();
		paddlePitchDatabase_.writeToFileStream(fileStream);
		// TODO: compress binary file?
		trace("I PaddlePitchCache file created");
		fileStream.close();
		return true;
	}
}
}

internal class Lock{}

package core
{
import flash.data.SQLConnection;
import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.errors.SQLError;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

public class DbUtil
{
	private static var instance_:DbUtil = null;
	private var sqlConnection_:SQLConnection;
	private var sql_:SQLStatementWithSerialNum;
	private var sqlSerialNum_:int;
	private var sqlExecuteCallback_:Function;
	private static const TRACE_DEBUG_MSG:Boolean = false;
	
	static public function getSingleton() : DbUtil {
		if ( instance_ == null ) instance_ = new DbUtil( new Lock() );
		return instance_;
	}
	
	public function DbUtil(lock: Lock) {
		if ( lock == null ) throw new Error("Singleton not allowed bla bla bla");
		sqlConnection_ = null;
	}
	
	public function open(finishedCallback:Function):void {
		sqlConnection_ = new SQLConnection();
		sqlConnection_.addEventListener(SQLEvent.OPEN, function(event:SQLEvent):void {
			trace("I sqlConnection success: " + event.type);
			onDatabaseOpen(finishedCallback);
		});
		sqlConnection_.addEventListener(SQLErrorEvent.ERROR, function(event:SQLErrorEvent):void {
			trace("E sqlConnection openAsync error message: " + event.error.message + ", details:", event.error.details);
			sqlConnection_ = null;
		});
		
		var dbFile:File = File.applicationStorageDirectory.resolvePath("settingsAndStats.db");
		trace("I using dbFile, url: " + dbFile.url + ", nativePath: " + dbFile.nativePath);
		sqlConnection_.openAsync(dbFile);
	}
	
	private function onDatabaseOpen(finishedCallback:Function):void {
		if (TRACE_DEBUG_MSG) {
			trace("I creating DB tables (if necessary)");
		}
		sql_ = new SQLStatementWithSerialNum();
		sql_.sqlConnection = sqlConnection_;
		sql_.addEventListener(SQLEvent.RESULT, onSqlExecuteSuccess);
		sql_.addEventListener(SQLErrorEvent.ERROR, onSqlExecuteError);
		sqlSerialNum_ = 0;

		executeSqlArray(SQL_CREATE_TABLES, function (success:Boolean, sqlResult:SQLResult):void {
			finishedCallback();
		});
	}
	
	public function isExecuting():Boolean {
		return sql_.executing;
	}
	
	public function executeSql(sql:String, finishedCallback:Function, args:Array=null):void {
		if (!sqlConnection_) {
			finishedCallback(false);
		}
		if (sql_.executing) {
			throw new Error("sql is still executing, old: " + sql_.text + ",  new: " + sql);
		}
		sql_.text = sql;
		var i:int;
		sql_.clearParameters();
		if (args != null) {
			for (i = 0; i < args.length; i++) {
				sql_.parameters[i] = args[i];
			}
		}
		if (TRACE_DEBUG_MSG) {
			if (args != null) {
				//trace("I   for sql " + sql);
				for (i = 0; i < args.length; i++) {
					sql = sql.replace(/\?/, args[i]);
					//trace("I     set sql_.parameter: " + sql_.parameters[i]);
				}
			}
			trace("I   executing sql #" + sqlSerialNum_ + ": " + sql);
		}
		sql_.setSerialNum(sqlSerialNum_);
		sqlExecuteCallback_ = finishedCallback;
		sql_.execute();
		sqlSerialNum_++;
	}
	
	private function onSqlExecuteSuccess(event:SQLEvent):void {
		var eventSql:SQLStatementWithSerialNum = event.target as SQLStatementWithSerialNum;
		var sqlResult:SQLResult = eventSql.getResult();
		var sqlSerialNum:int = eventSql.getSerialNum();
		if (TRACE_DEBUG_MSG) {
			trace("I   sql #" + sqlSerialNum + " complete: " + 
				(sqlResult ? sqlResult.complete : "unknown"));
		}
		sqlExecuteCallback_(true, sqlResult);
	}

	private function onSqlExecuteError(event:SQLErrorEvent):void {
		var eventSql:SQLStatementWithSerialNum = event.target as SQLStatementWithSerialNum;
		var sqlSerialNum:int = eventSql.getSerialNum();
		trace("E   sql #" + sqlSerialNum + " error: " + event.error.message + ", details:", event.error.details);
		sqlConnection_.close();
		sqlConnection_ = null;
		sql_ = null;
		sqlExecuteCallback_(false, null);
	}
		
	public function executeSqlArray(sqlArray:Array, finishedCallback:Function):void {
		if (sqlArray.length == 0) {
			finishedCallback(true, null);
			return;
		}
		if (TRACE_DEBUG_MSG) {
			trace("I executeSqlArray, array length: " + sqlArray.length);
		}
		executeSql(sqlArray.shift(), function (success:Boolean, sqlResult:SQLResult):void {
			if (success) {
				executeSqlArray(sqlArray, finishedCallback);
			} else {
				finishedCallback(false, null);
			}
		});
	}
	
	private static const SQL_CREATE_TABLES:Array = [
		"CREATE TABLE IF NOT EXISTS game_config (" +
		  "key TEXT PRIMARY KEY, " +
		  "value INTEGER " + 
		  ")",
	    "CREATE TABLE IF NOT EXISTS user_stat (" +
		  "name TEXT PRIMARY KEY, " +
		  "highscore INTEGER" + 
		  ")"
	];
	
	public static const SQL_GET_CONFIG:String = 
		"SELECT value FROM game_config WHERE key = ?";
		
	public static const SQL_REPLACE_CONFIG:String = 
		"REPLACE INTO game_config (key, value) VALUES (?, ?)";
}
}
import flash.data.SQLStatement;

internal class Lock{}

internal class SQLStatementWithSerialNum extends SQLStatement {
	public function setSerialNum(n:int):void {
		this.text = this.text + " /* SQL_SERIAL_NUM: " + n + " */";
	}
	
	public function getSerialNum():int {
		var sqlSerialNumArr:Array = this.text.match(/\w*[0-9]+/);
		return parseInt(sqlSerialNumArr[0]);
	}
}

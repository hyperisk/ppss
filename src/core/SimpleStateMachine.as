package core
{
import flash.utils.Dictionary;

public class SimpleStateMachine
{
	private var states_:Dictionary;
	private var activeStateName_:String;
	private var activeStateElapsedTimeSec_:Number;
	
	public function SimpleStateMachine(stateDataArray:Array)
	{
		states_ = new Dictionary;
		activeStateName_ = null;
		for each (var stateData:Object in stateDataArray) {
			var state:SimpleState = new SimpleState;
			state.timeLengthSec_ = stateData[1];
			var name:String = stateData[0] as String;
			if (name.length == 0) {
				throw new Error("state key cannot be empty string");
			}
			states_[name] = state;
			if (!activeStateName_) {
				setActiveState(name);
			}
		}
	}
	
	public function setActiveState(name:String):void {
		if (!states_.hasOwnProperty(name)) {
			throw new Error("state " + name + " not found");
		}
		activeStateName_ = name;
		activeStateElapsedTimeSec_ = 0;
	}
	
	public function update(elapsedTime:Number):Number {
		if (activeStateName_.length > 0) {
			activeStateElapsedTimeSec_ += elapsedTime;
		}
		return activeStateElapsedTimeSec_;
	}
	
	public function isStateActive(name:String):Boolean {
		if (!states_.hasOwnProperty(name)) {
			throw new Error("state " + name + " not found");
		}
		return activeStateName_ == name;
	}
	
	public function isActiveStateExpired():Boolean {
		if (activeStateName_.length > 0) {
			var state:SimpleState = states_[activeStateName_];
			if (state.timeLengthSec_ <= 0) {
				throw new Error("cannot determine expired if time length is 0"); 
			}
			if (activeStateElapsedTimeSec_ > state.timeLengthSec_) {
				return true;
			}
		}
		return false;
	}
	
	public function getActiveStateTimeLen():Number {
		var state:SimpleState = states_[activeStateName_];
		return state.timeLengthSec_;
	}
	
	public function getInfoText():String {
		if (activeStateName_.length > 0) {
			return "state: " + activeStateName_;
		} else {
			return "no active state";
		}
	}
}
}

class SimpleState {
	public var timeLengthSec_:Number;
	
	function SimpleState() {
		timeLengthSec_ = 0;
	}
}
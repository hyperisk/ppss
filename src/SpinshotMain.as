package
{
import core.EngineUtil;
import core.GameConfig;
import core.LevelManager;
import core.LoadingManager;
import core.StageUtil;

import flash.display.Sprite;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.StageOrientationEvent;
import flash.utils.getTimer;

import renderer.MainMenuRenderer;
import renderer.RendererManager;

import simulation.SimWorld;

public class SpinshotMain extends Sprite
{
	private var rendererManager_:RendererManager;
	private var simWorld_:SimWorld;
	private var loadingManager_:LoadingManager;
	private var levelManager_:LevelManager;
	private var appStartTimeMsec_:int;
	
	public function SpinshotMain() {
		super();
		trace("*******************************************************************");
		trace("main app started");
		appStartTimeMsec_ = getTimer();
		stage.scaleMode = StageScaleMode.NO_SCALE;
		trace("  frame rate (before setting): " + stage.frameRate); 
		GameConfig.INITIAL_FPS = stage.frameRate; 
		stage.frameRate = GameConfig.RENDER_FPS_DEFAULT; 
		stage.addEventListener(Event.RESIZE, stageResizedInitial);
	}
	
	private function stageResizedInitial(event:Event):void {
		var elapsedTime:Number = (getTimer() - appStartTimeMsec_) / 1000;
		trace("I SpinshotMain: stage resized (initial), elapsed time: " + elapsedTime);
		stage.removeEventListener(Event.RESIZE, stageResizedInitial);
		stage.addEventListener(Event.RESIZE, StageUtil.getSingleton().onStageResizedAfterInit);
		stage.addEventListener(StageOrientationEvent.ORIENTATION_CHANGE, StageUtil.getSingleton().onStageOrientationChanged);
		StageUtil.getSingleton().init(this.stage);
		simWorld_ = new SimWorld();
		rendererManager_ = new RendererManager();
		loadingManager_ = new LoadingManager(rendererManager_);
		loadingManager_.loadAll(onLoadComplete);
	}
	
	private function onLoadComplete():void {
		levelManager_ = new LevelManager(simWorld_, rendererManager_);
		var initElapsedTime:Number = (getTimer() - appStartTimeMsec_) / 1000;
		trace("*******************************************************************");
		trace("I loaded all modules, and showing menu");
		trace("  (elapsed time from main app start: " + EngineUtil.roundBrief(initElapsedTime) + "sec)");
		rendererManager_.showMainMenu(onMainMenuSelected);
	}
	
	private function onMainMenuSelected(menuItem:String):void {
		if (menuItem == MainMenuRenderer.MENU_ITEM_START_GAME) {
			levelManager_.showLevelMenu();
		} else {
			throw new Error("WIP");
		}
	}
}
}
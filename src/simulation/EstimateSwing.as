package simulation
{
import core.EngineUtil;
import core.GameConfig;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.geom.Point;

import level.PaddleEvent;

public class EstimateSwing implements ISimUpdateObject
{
	private var side_:String;
	
	public static const SWING_STYLE_SHORT_HIT:String = "swing_style_short_hit";
	public static const SWING_STYLE_LONG_HIT:String = "swing_style_long_hit";
	private var swingStyle_:String; 

	public static const SWING_SPEED_SLOW:String = "swing_speed_slow";
	public static const SWING_SPEED_MEDIUM:String = "swing_speed_medium";
	public static const SWING_SPEED_FAST:String = "swing_speed_fast";
	private var swingSpeed_:String;

	private var simWorld_:SimWorld;
	private var state_:SimpleStateMachine;
	private static const STATE_SWING_SEARCHING:String = "swing_searching";
	private static const STATE_SWING_TRACKING:String = "swing_tracking";
	private static const STATE_SWING_BACK:String = "swing_back";
	private static const STATE_SWING_TO_HIT:String = "swing_to_hit";
	private static const STATE_SWING_RETURN:String = "swing_return";
	public var estimatedCfg_:BallCfg;
	private static const TRACE_DEBUG_MSG:Boolean = false;
	private static const TRACE_DEBUG_MSG_SW:Boolean = false;
	private static const TRACE_DEBUG_MSG_SC:Boolean = false;
	private static const TRACE_DEBUG_MSG_SC_ALL:Boolean = false;
	
	private var playAreaLeftSideLeft_:Number;
	private var playAreaLeftSideRight_:Number;
	private var playPosLeftSideX_:Number;
	private var playPosX_:Number;
	private var playAreaTop_:Number;
	private var playAreaBottom_:Number;
	private var playPosY_:Number;

	public var hitTargetPosX_:Number;
	public var hitTargetPosY_:Number;
	private var hitTargetUnitVelX_:Number;
	private var hitTargetUnitVelY_:Number;
	private var hitTargetSpeed_:Number;
	private var hitTargetVelX_:Number;
	private var hitTargetTimeRemaining_:Number;
	private static const TARGET_ESTIMATE_INTERVAL:int = 1;
	private var lastTargetEstimateSimNumber_:int;
	private var returnStartPosX_:Number;
	private var returnStartPosY_:Number;
	
	private var hitTargetPosXscoreLerpPoints_:Array;
	private var hitTargetPosYscoreLerpPoints_:Array;
	private var hitTargetSlopeScoreLerpPoints_:Array;
	private var hitSpeedTargetSpeedLerpPoints_:Array;
	private var hitSpeedTargetHeightLerpPoints_:Array;
	private var swingAngleRatioHitSpeedLerpPoints_:Array;
	
	private var swingStartLateTime_:Number; 
	private var swingBackTimeLen_:Number;
	private var swingBackTimeLenIncludingLate_:Number;
	private var swingToHitTimeLen_:Number; 
	private var swingAfterHitTimeLen_:Number; 
	private var swingStartPosDiffX_:Number;
	private var swingStartPosDiffY_:Number;
	private var swingEndPosDiffX_:Number;
	private var swingEndPosDiffY_:Number;
	private var simTimeSinceNewSwing_:Number;
	public var swingTargetCfg_:BallCfg;

	public function EstimateSwing(simWorld:SimWorld, side:String) {
		simWorld_ = simWorld;
		side_ = side;
		state_ = new SimpleStateMachine([
			[STATE_SWING_SEARCHING, 0],
			[STATE_SWING_TRACKING, 0],
			[STATE_SWING_BACK, 0],
			[STATE_SWING_TO_HIT, 0],
			[STATE_SWING_RETURN, 0],
		]);
		estimatedCfg_ = new BallCfg;
		swingTargetCfg_ = new BallCfg;
	}

	public function init():void {
		swingStyle_ = SWING_STYLE_SHORT_HIT;
		swingSpeed_ = SWING_SPEED_MEDIUM;
		
		playAreaLeftSideLeft_ = -SimTable.TABLE_TOP_WIDTH / 2 - 1; 
		playAreaLeftSideRight_ = -SimTable.TABLE_TOP_WIDTH / 2 + 0.2;
		playPosLeftSideX_ = -SimTable.TABLE_TOP_WIDTH / 2 - 0.1;
		playPosX_ = playPosLeftSideX_ * (side_ == SimBall.LEFT_SIDE ? 1 : -1);

		var playAreaBottom:Number = SimTable.TABLE_LEG_HEIGHT;
		var playAreaTop:Number = SimTable.TABLE_LEG_HEIGHT + SimTable.TABLE_NET_HEIGHT * 4;
		playPosY_ = SimTable.TABLE_LEG_HEIGHT + SimTable.TABLE_TOP_PLAY_POS_DIST; 
		
		hitTargetPosXscoreLerpPoints_ = [
			new Point(playAreaLeftSideLeft_ - 1, 100),
			new Point(playAreaLeftSideLeft_, 2),
			new Point(playPosLeftSideX_, 0),
			new Point(playAreaLeftSideRight_, 4),
			new Point(playAreaLeftSideRight_ + 1, 100),
		];
		hitTargetPosYscoreLerpPoints_ = [
			new Point(playAreaBottom - 1, 100),
			new Point(playAreaBottom, 8),
			new Point(playPosY_, 0),
			new Point(playAreaTop, 2),
			new Point(playAreaTop + 1, 10)
		];
		hitTargetSlopeScoreLerpPoints_ = [
			new Point(0, 0),
			new Point(0.2, 1),
			new Point(1, 10),
			new Point(9999, 10)
		];
		hitSpeedTargetSpeedLerpPoints_ = [
			new Point(0, 0.7),
			new Point(0.2, 0.9),
			new Point(0.5, 0.95),
			new Point(1, 1)
		];
		hitSpeedTargetHeightLerpPoints_ = [
			new Point(-1, 1.5),
			new Point(0, 1.1),
			new Point(0.2, 1),
			new Point(0.4, 1),
			new Point(0.6, 0.95),
			new Point(1, 0.6)
		];
		swingAngleRatioHitSpeedLerpPoints_ = [
			new Point(0, 1),
			new Point(5, 0.5),
			new Point(10, 0.2)
		];
	}
	
	public function setSwingParams(swingStyle:String, swingSpeed:String):void {
		if (state_.isStateActive(EstimateSwing.STATE_SWING_BACK) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_TO_HIT) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_RETURN)) {
			trace("W set swing style while state is " + state_.getInfoText());
		}
		swingStyle_ = swingStyle;
		swingSpeed_ = swingSpeed;
		if (TRACE_DEBUG_MSG) {
			trace("I /\\/\\ swing params: " + swingStyle + ", " + swingSpeed);
		}
	}
	
	public function onStartPlaying():void {
		state_.setActiveState(STATE_SWING_SEARCHING);
		lastTargetEstimateSimNumber_ = EngineUtil.IntNaN;
		estimatedCfg_.init();
	}

	// interface impl
	public function onSimStart():void {
		
	}
	
	// interface impl
	public function onSimStop():void {		
		if (!state_.isStateActive(STATE_SWING_SEARCHING)) {
			state_.setActiveState(STATE_SWING_SEARCHING);
			if (TRACE_DEBUG_MSG) {
				trace("I   ^^ swing state change to SWING_SEARCHING (stop sim)");
			}
		}
	}

	// interface impl
	public function onSimUpdate(simNumber:int, simElapsedTime:Number):void {
		var ballPosLeftSideX:Number = simWorld_.simBall_.posX_ * (side_ == SimBall.LEFT_SIDE ? 1 : -1);
		var ballDistToPlayPos:Number = ballPosLeftSideX - playPosLeftSideX_;
		var estimateTargetNow:Boolean = false;
		
		if ((lastTargetEstimateSimNumber_ == EngineUtil.IntNaN) ||
			(simNumber - lastTargetEstimateSimNumber_ > TARGET_ESTIMATE_INTERVAL)) {
				estimateTargetNow = true;
				lastTargetEstimateSimNumber_ = simNumber; 
		}
		
		//StageUtil.getSingleton().infoText_.setText("estimate swing", state_.getInfoText());
		if (state_.isStateActive(STATE_SWING_SEARCHING)) {
			if (estimateTargetNow && simWorld_.states_.isStateActive(SimWorld.STATE_SIMULATING)) {
				estimateHitTargetData();
				if (!isNaN(hitTargetPosX_)) {
					state_.setActiveState(STATE_SWING_TRACKING);					
					if (TRACE_DEBUG_MSG) {
						trace("I   ^^ swing state change to SWING_TRACK, hitTargetPosX_: " + 
							EngineUtil.roundBrief(hitTargetPosX_));
					}
					updateForTrack();
				}
			}
		} else if (state_.isStateActive(STATE_SWING_TRACKING)) {
			if (estimateTargetNow) {
				estimateHitTargetData();
				if (isNaN(hitTargetPosX_)) {
					state_.setActiveState(STATE_SWING_SEARCHING);
					if (TRACE_DEBUG_MSG) {
						trace("I   ^^ swing state change to SWING_SEARCHING (invalid hit target)");
						return;
					}
				}
			} else {
				hitTargetTimeRemaining_ -= simElapsedTime;
			}
			if (!isNaN(hitTargetPosX_) &&
				(swingToHitTimeLen_ + swingBackTimeLen_) > hitTargetTimeRemaining_) {
				state_.setActiveState(STATE_SWING_BACK);
				simTimeSinceNewSwing_ = 0;
				swingStartLateTime_ = swingToHitTimeLen_ + swingBackTimeLen_ - hitTargetTimeRemaining_;
				estimateSwingData();	// to set swingAfterHitTimeLen_
				if (TRACE_DEBUG_MSG) {
					trace("I   ^^ swing state change to SWING_BACK, swing time len: " + 
						EngineUtil.roundBrief(swingBackTimeLen_) +
					", time late: " + EngineUtil.roundBrief(swingStartLateTime_));
				}
			} else {
				updateForTrack();
			}
		} else if (state_.isStateActive(STATE_SWING_BACK)) {
			if (estimateTargetNow) {
				estimateHitTargetData();
				if (isNaN(hitTargetPosX_)) {
					state_.setActiveState(STATE_SWING_SEARCHING);
					if (TRACE_DEBUG_MSG) {
						trace("I   ^^ swing state change to SWING_SEARCHING (invalid hit target)");
						return;
					}
				}
			}
			simTimeSinceNewSwing_ += simElapsedTime;
			if (simTimeSinceNewSwing_ > swingBackTimeLenIncludingLate_) {
				simTimeSinceNewSwing_ = 0;
				state_.setActiveState(STATE_SWING_TO_HIT);
				if (TRACE_DEBUG_MSG) {
					trace("I   ^^ swing state change to SWING_TO_HIT, swing time len: " + 
						EngineUtil.roundBrief(swingToHitTimeLen_ + swingAfterHitTimeLen_));
				}
			} else {
				updateForSwingBack();
			}
		} else if (state_.isStateActive(STATE_SWING_TO_HIT)) {
			// do not estimate - hit target must stay same for stable swing (until actually hit)
			simTimeSinceNewSwing_ += simElapsedTime;
			if (simTimeSinceNewSwing_ > (swingToHitTimeLen_ + swingAfterHitTimeLen_)) {
				state_.setActiveState(STATE_SWING_RETURN);
				simTimeSinceNewSwing_ = 0;
				if (TRACE_DEBUG_MSG) {
					var ballBounced:Boolean = simWorld_.simBall_.velX_ * (side_ == SimBall.LEFT_SIDE ? -1 : 1) < 0; 
					trace("I   ^^ swing state change to SWING_RETURN, ball bounced: " + ballBounced);
				}
			} else {
				updateForSwingToHit();
			}
		} else if (state_.isStateActive(STATE_SWING_RETURN)) {
			simTimeSinceNewSwing_ += simElapsedTime;
			if (simTimeSinceNewSwing_ > swingBackTimeLen_) {
				state_.setActiveState(STATE_SWING_SEARCHING);
				if (TRACE_DEBUG_MSG) {
					trace("I   ^^ swing state change to SWING_SEARCHING");
				}
			} else {
				updateForSwingReturn();
			}
		}
		
		if (state_.isStateActive(STATE_SWING_TRACKING) || 
			state_.isStateActive(STATE_SWING_BACK) || 
			state_.isStateActive(STATE_SWING_TO_HIT)) {
			returnStartPosX_ = estimatedCfg_.posX_;
			returnStartPosY_ = estimatedCfg_.posY_;
		}
		if (isNaN(estimatedCfg_.posX_) || isNaN(estimatedCfg_.posY_)) {
			if (TRACE_DEBUG_MSG_SW) {
				trace("W estimageCfg pos is NaN, state: " + state_.getInfoText());
			}
		}
	}
	
	public function setEventPosFromEstimate(paddleEvent:PaddleEvent):void {
		if (state_.isStateActive(EstimateSwing.STATE_SWING_SEARCHING)) {
			paddleEvent.posX_ = playPosX_;
			paddleEvent.posY_ = playPosY_;
		} else if (state_.isStateActive(EstimateSwing.STATE_SWING_TRACKING)) {
			paddleEvent.posX_ = estimatedCfg_.posX_;
			paddleEvent.posY_ = estimatedCfg_.posY_;
		} else if (state_.isStateActive(EstimateSwing.STATE_SWING_BACK)) {
			paddleEvent.posX_ = estimatedCfg_.posX_;
			paddleEvent.posY_ = estimatedCfg_.posY_;
		} else if (state_.isStateActive(EstimateSwing.STATE_SWING_TO_HIT)) {
			paddleEvent.posX_ = estimatedCfg_.posX_;
			paddleEvent.posY_ = estimatedCfg_.posY_;
		} else if (state_.isStateActive(EstimateSwing.STATE_SWING_RETURN)) {
			paddleEvent.posX_ = estimatedCfg_.posX_;
			paddleEvent.posY_ = estimatedCfg_.posY_;
		} else {
			throw new Error();
		}
	}
	
	public function hasHitTarget():Boolean {
		return state_.isStateActive(EstimateSwing.STATE_SWING_TRACKING) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_BACK) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_TO_HIT);
	}
	
	public function hasEstimatedCfg():Boolean {
		return state_.isStateActive(EstimateSwing.STATE_SWING_TRACKING) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_BACK) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_TO_HIT) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_RETURN);
	}
	
	public function hasTargetSwing():Boolean {
		return state_.isStateActive(EstimateSwing.STATE_SWING_BACK) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_TO_HIT) ||
			state_.isStateActive(EstimateSwing.STATE_SWING_RETURN);
	}
	
	private function estimateHitTargetData():void {
		hitTargetPosX_ = NaN;
		hitTargetTimeRemaining_ = 0;
		
		var ballBounced:Boolean = simWorld_.simBall_.velX_ * (side_ == SimBall.LEFT_SIDE ? -1 : 1) < 0;
		if (ballBounced) {
			return;
		}
		
		var simTimeStepType:String;
		if (simWorld_.simBall_.inSameSide(side_)) {
			if (simWorld_.simBall_.velY_ > 0) {
				simTimeStepType = EstimatePath.SIM_TIME_STEP_SMALL;
			} else {
				simTimeStepType = EstimatePath.SIM_TIME_STEP_MEDIUM;
			}
		} else {
			simTimeStepType = EstimatePath.SIM_TIME_STEP_LARGE;
		}
		simWorld_.estimatePath_.run(simTimeStepType);
		
		var maxScore:Number = 0;
		var maxScoreSimStep:int = EngineUtil.IntNaN;
		for (var simStep:int = 0; simStep < simWorld_.estimatePath_.numSimSteps_; simStep++) {
			var cfg:BallCfg = simWorld_.estimatePath_.estimatedCfgs_[simStep];
			var cfgLeftSideX:Number = cfg.posX_ * (side_ == SimBall.LEFT_SIDE ? 1 : -1);
			if ((cfgLeftSideX < playAreaLeftSideLeft_) || (cfgLeftSideX > playAreaLeftSideRight_)) {
				if (TRACE_DEBUG_MSG_SC_ALL) {
					trace("        ...... no score, cfgLeftSideX: " + EngineUtil.roundBrief(cfgLeftSideX));
				}
				continue;
			}

			var posScorePosDiffX:Number = 0;
			var posScorePosDiffY:Number = 0;
			var posScorePosWeightY:Number = 1;
			var slopeScoreWeight:Number = 1;
			if (swingStyle_ == SWING_STYLE_SHORT_HIT) {
				posScorePosDiffX = 0.1;
				posScorePosDiffY = 0;
				slopeScoreWeight = 1;
			} else if (swingStyle_ == SWING_STYLE_LONG_HIT) {
				posScorePosDiffX = -1;
				posScorePosDiffY = -0.1;
				posScorePosWeightY = 0.1;
				slopeScoreWeight = 0.1;
			}
			
			// step 1. how far from desired hit pos?
			var score:Number = 100;
			var relPosXscore:Number = EngineUtil.lerpPoints(hitTargetPosXscoreLerpPoints_, cfgLeftSideX - posScorePosDiffX); 
			score -= relPosXscore
			var relPosYscore:Number = EngineUtil.lerpPoints(hitTargetPosYscoreLerpPoints_, cfg.posY_ + posScorePosDiffY) * posScorePosWeightY;
			//trace("??? relPosYscore " + relPosYscore + " from Y pos diff " + (playPosY_ - (cfg.posY_ + posScorePosDiffY)));
			score -= relPosYscore;
			
			// step 2. slope of ball estimated trajectory?
			var trajSlopeScore:Number = 0;
			if (simStep < simWorld_.estimatePath_.numSimSteps_ - 1) {
				var cfgNext:BallCfg = simWorld_.estimatePath_.estimatedCfgs_[simStep + 1];
				if (Math.abs(cfgNext.posX_ - cfg.posX_) > 0.001) {
					var trajSlope:Number = Math.abs((cfgNext.posY_ - cfg.posY_) / (cfgNext.posX_ - cfg.posX_));
					var lastPoint:Point = hitTargetSlopeScoreLerpPoints_[hitTargetSlopeScoreLerpPoints_.length-1]; 
					trajSlopeScore = EngineUtil.lerpPoints(hitTargetSlopeScoreLerpPoints_, trajSlope) * slopeScoreWeight;
					score -= trajSlopeScore;
				}
			}
			
			if (score > maxScore) {
				maxScoreSimStep = simStep;
				maxScore = score;
				if (TRACE_DEBUG_MSG_SC) {
					trace("       new max score " + EngineUtil.roundBrief(maxScore)  
						+ " with position-based score x,y: " + EngineUtil.roundBrief(relPosXscore) + ", " + 
						EngineUtil.roundBrief(relPosYscore) + " (from cfg pos " +
						EngineUtil.roundBrief(cfg.posX_) + ", " + EngineUtil.roundBrief(cfg.posY_) + ")" +
						" and with slope-based: " + EngineUtil.roundBrief(trajSlopeScore) +
						" at simStep " + simStep + ", totalSimSteps: " + simWorld_.estimatePath_.numSimSteps_
					); 
				}
			} else {
				if (TRACE_DEBUG_MSG_SC_ALL) {
					trace("        ...... score " + EngineUtil.roundBrief(maxScore)  
						+ " with position-based score x,y: " + EngineUtil.roundBrief(relPosXscore) + ", " + 
						EngineUtil.roundBrief(relPosYscore) + " (from cfg pos " +
						EngineUtil.roundBrief(cfg.posX_) + ", " + EngineUtil.roundBrief(cfg.posY_) + ")" +
						" and with slope-based: " + EngineUtil.roundBrief(trajSlopeScore) +
						" at simStep " + simStep
					); 
				}
			}
		}
		
		if (maxScoreSimStep != EngineUtil.IntNaN) {
			var cfgMaxScore:BallCfg = simWorld_.estimatePath_.estimatedCfgs_[maxScoreSimStep];
			hitTargetPosX_ = cfgMaxScore.posX_;
			hitTargetPosY_ = cfgMaxScore.posY_ + cfgMaxScore.velY_ * (2 / GameConfig.PHYSICS_FPS_RENDER_FPS_DEFAULT);
			hitTargetSpeed_ = Math.sqrt(cfgMaxScore.velX_ * cfgMaxScore.velX_ + cfgMaxScore.velY_ * cfgMaxScore.velY_);
			hitTargetVelX_ = cfgMaxScore.velX_;
			hitTargetUnitVelX_ = cfgMaxScore.velX_ / hitTargetSpeed_; 
			hitTargetUnitVelY_ = cfgMaxScore.velY_ / hitTargetSpeed_;
			hitTargetTimeRemaining_ = simWorld_.estimatePath_.simTimeStep_ * maxScoreSimStep; 
			
			estimateSwingData();
		} else {
			if (TRACE_DEBUG_MSG_SW) {
				trace("I  ?? estimate swing hit target: not found"); 
			}
		}
	}
	
	private function estimateSwingData():void {
		var swingBackSpeed:Number;
		var swingToHitSpeedIncludingBounce:Number;
		var swingToHitSpeedMin:Number;
		var swingDistAfterHit:Number;
		var swingDistBeforeHit:Number;
		var swingToHitAngleDegMax:Number;
		
		// step 1. get default swing data
		if (swingStyle_ == SWING_STYLE_SHORT_HIT) {
			swingDistBeforeHit = 0.5;
			swingDistAfterHit = 0.2;
		} else if (swingStyle_ == SWING_STYLE_LONG_HIT) {
			swingDistBeforeHit = 0.7;
			swingDistAfterHit = 0.2;
		}
		
		if (swingSpeed_ == SWING_SPEED_SLOW) {
			swingBackSpeed = 2;
			swingToHitSpeedIncludingBounce = 5;
			swingToHitSpeedMin = 2;
			swingToHitAngleDegMax = 30;
		} else if (swingSpeed_ == SWING_SPEED_MEDIUM) {
			swingBackSpeed = 2;
			swingToHitSpeedIncludingBounce = 6;
			swingToHitSpeedMin = 4;
			swingToHitAngleDegMax = 25;
		} else if (swingSpeed_ == SWING_SPEED_FAST) {
			swingBackSpeed = 3;
			swingToHitSpeedIncludingBounce = 8;
			swingToHitSpeedMin = 7;
			swingToHitAngleDegMax = 15;
			
			// prevent premature return
			swingDistBeforeHit *= 1.4;
			swingDistAfterHit *= 1.8;
		} else {
			throw new Error("unhandled swing speed type");
		}
		
		// step 2. adjust for position and angle
		var swingToHitSpeedRatio:Number = EngineUtil.lerpPoints(hitSpeedTargetSpeedLerpPoints_, 
			hitTargetSpeed_ / SimBall.BALL_MAX_SPEED);
		swingToHitSpeedIncludingBounce *= swingToHitSpeedRatio;
		swingToHitSpeedRatio = EngineUtil.lerpPoints(hitSpeedTargetHeightLerpPoints_,
			hitTargetPosY_ - SimTable.TABLE_LEG_HEIGHT);
		swingToHitSpeedIncludingBounce *= swingToHitSpeedRatio;
		
		var swingForeSpeed:Number = swingToHitSpeedIncludingBounce - Math.abs(hitTargetVelX_ * SimBall.BALL_RESTITUTION_COEF * 0.5);
		swingForeSpeed = Math.max(swingForeSpeed, swingToHitSpeedMin);
		swingToHitTimeLen_ = swingDistBeforeHit / swingForeSpeed;
		var swingToHitAngleDeg:Number = swingToHitAngleDegMax * 
			EngineUtil.lerpPoints(swingAngleRatioHitSpeedLerpPoints_, swingToHitSpeedIncludingBounce);
		var swingToHitCompX:Number = Math.cos(swingToHitAngleDeg * Math.PI / 180);
		var swingToHitCompY:Number = -Math.sin(swingToHitAngleDeg * Math.PI / 180);
		swingStartPosDiffX_ = swingDistBeforeHit * swingToHitCompX * (side_ == SimBall.LEFT_SIDE ? -1 : 1);
		swingStartPosDiffY_ = swingDistBeforeHit * swingToHitCompY;
		swingEndPosDiffX_ = swingDistAfterHit * -swingToHitCompX * (side_ == SimBall.LEFT_SIDE ? -1 : 1);
		swingEndPosDiffY_ = swingDistAfterHit * -swingToHitCompY;
		swingBackTimeLen_ = swingDistBeforeHit / swingBackSpeed;
		var swingStartLateTimeMin:Number = Math.min(swingBackTimeLen_ / 3, swingStartLateTime_);
		swingBackTimeLenIncludingLate_ = swingBackTimeLen_ - swingStartLateTimeMin;
		swingAfterHitTimeLen_ = swingDistAfterHit / swingForeSpeed;
		
		if (TRACE_DEBUG_MSG_SW) {
			trace("   DATA swing speed (hit: " + EngineUtil.roundBrief(swingToHitSpeedIncludingBounce) +
				", fore: " + EngineUtil.roundBrief(swingForeSpeed) +
				"), pos diff (" + 
					EngineUtil.roundBrief(swingStartPosDiffX_) + ", " + EngineUtil.roundBrief(swingStartPosDiffY_) + 
				" ===> " + EngineUtil.roundBrief(swingEndPosDiffX_) + ", " + EngineUtil.roundBrief(swingEndPosDiffY_) + 
				"), time len (" + EngineUtil.roundBrief(swingBackTimeLen_) + " ~~> " + EngineUtil.roundBrief(swingBackTimeLenIncludingLate_) +   
				" ===> " + EngineUtil.roundBrief(swingToHitTimeLen_) +
					" + " + EngineUtil.roundBrief(swingAfterHitTimeLen_) +
				", remaining: " + EngineUtil.roundBrief(hitTargetTimeRemaining_) +
				"), angle: " + EngineUtil.roundBrief(swingToHitAngleDeg)
			);
		}
	}
	
	private function updateForTrack():void {
		estimatedCfg_.posX_ = hitTargetPosX_;
		estimatedCfg_.posY_ = hitTargetPosY_; 
		if (TRACE_DEBUG_MSG_SW) {
			trace("I    update swing TRACK est cfg: " + EngineUtil.roundBrief(estimatedCfg_.posX_) + ", " + 
				EngineUtil.roundBrief(estimatedCfg_.posY_) + " from hit target " + 
				EngineUtil.roundBrief(hitTargetPosX_) + ", " + EngineUtil.roundBrief(hitTargetPosY_) 
			); 
		}
	}
	
	private function updateForSwingBack():void {
		var swingRatio:Number = simTimeSinceNewSwing_ / swingBackTimeLenIncludingLate_;
		swingTargetCfg_.posX_ = hitTargetPosX_ + swingStartPosDiffX_;
		swingTargetCfg_.posY_ = hitTargetPosY_ + swingStartPosDiffY_;
		estimatedCfg_.posX_ = hitTargetPosX_ + swingStartPosDiffX_ * swingRatio; 
		estimatedCfg_.posY_ = hitTargetPosY_ + swingStartPosDiffY_ * swingRatio; 
		if (TRACE_DEBUG_MSG_SW) {
			trace("I    update swing BACK cfg: " + EngineUtil.roundBrief(estimatedCfg_.posX_) + ", " + 
				EngineUtil.roundBrief(estimatedCfg_.posY_) + " from hit target " + 
				EngineUtil.roundBrief(hitTargetPosX_) + ", " + EngineUtil.roundBrief(hitTargetPosY_)
				+ " and pos diff " + 
				EngineUtil.roundBrief(swingStartPosDiffX_) + ", " + EngineUtil.roundBrief(swingStartPosDiffY_)
				+ ", swing elapsed time: " + EngineUtil.roundBrief(simTimeSinceNewSwing_)
			); 
		}
	}
	
	private function updateForSwingToHit():void {
		var swingRatio:Number = simTimeSinceNewSwing_ / (swingToHitTimeLen_ + swingAfterHitTimeLen_);
		swingRatio = Math.min(1, swingRatio);
		swingRatio = Math.max(0, swingRatio);
		swingTargetCfg_.posX_ = hitTargetPosX_ + swingEndPosDiffX_;
		swingTargetCfg_.posY_ = hitTargetPosY_ + swingEndPosDiffY_;
		estimatedCfg_.posX_ = (hitTargetPosX_ + swingStartPosDiffX_) + (swingEndPosDiffX_ - swingStartPosDiffX_) * swingRatio;
		estimatedCfg_.posY_ = (hitTargetPosY_ + swingStartPosDiffY_) + (swingEndPosDiffY_ - swingStartPosDiffY_) * swingRatio; 
		if (TRACE_DEBUG_MSG_SW) {
			trace("I    update swing TO HIT cfg: " + EngineUtil.roundBrief(estimatedCfg_.posX_) + ", " + 
				EngineUtil.roundBrief(estimatedCfg_.posY_) + " from hit target " + 
				EngineUtil.roundBrief(hitTargetPosX_) + ", " + EngineUtil.roundBrief(hitTargetPosY_)
				+ ", pos diff: " + 
				EngineUtil.roundBrief(swingEndPosDiffX_) + ", " + EngineUtil.roundBrief(swingEndPosDiffY_) 
				+ ", swing elapsed time: " + EngineUtil.roundBrief(simTimeSinceNewSwing_)  
			); 
		}
	}
	
	private function updateForSwingReturn():void {
		var returnRatio:Number = simTimeSinceNewSwing_ / swingBackTimeLen_;
		swingTargetCfg_.posX_ = playPosX_;
		swingTargetCfg_.posY_ = playPosY_;
		estimatedCfg_.posX_ = returnStartPosX_ + (playPosX_ - returnStartPosX_) * returnRatio;  
		estimatedCfg_.posY_ = returnStartPosY_ + (playPosY_ - returnStartPosY_) * returnRatio;  
		if (TRACE_DEBUG_MSG_SW) {
			trace("I    update swing TO RETURN cfg: " + EngineUtil.roundBrief(estimatedCfg_.posX_) + ", " + 
				EngineUtil.roundBrief(estimatedCfg_.posY_) + " from play pos " + 
				EngineUtil.roundBrief(playPosX_) + ", " + EngineUtil.roundBrief(playPosY_)
				);
		}
	}
}
}
package simulation
{
import core.EngineUtil;

public class Collision
{
	private static var t1_:Number;
	private static var t2_:Number;
	private static var ballCfg_:BallCfg = new BallCfg();
	
	private static const TRACE_DEBUG_MSG:Boolean = false;
	
	/* 
	 case 1. bounce upper surface
	
	             c          +   
	                        +
    		               +
	             =r     +++
    		         +++
	             ++++
                 ==========================================

     case 2. bounce left surface
	
	 case 3. bounce left edge
	
	     c          +   
	                +
	        <=r    +
	            +++
	         +++ ==========================================
	     ++++
	
	 etc.
	*/
	
	// bx1 < bx2, by1 < by2
	public static function ballCollBounceBox(simBall:SimBall, bx1:Number, bx2:Number, by1:Number, by2:Number,
			cRestPercent:int):Boolean {
		if (bx2 <= bx1) {
			throw new Error("coll box invalid x");
		}
		if (by2 <= by1) {
			throw new Error("coll box invalid y");
		}
		if ((cRestPercent <= 0) || (cRestPercent > 100)) {
			throw new Error("coll box invalid cRestPerc");
		}
		if (simBall.velY_ > 0) {
			if (ballCollBounceHorzEdgelessLineSeg(simBall, bx1, bx2, by1, cRestPercent)) {
				return true;
			}
		} else if (simBall.velY_ < 0) {
			if (ballCollBounceHorzEdgelessLineSeg(simBall, bx1, bx2, by2, cRestPercent)) {
				return true;
			}
		}
		var bpx:Number = simBall.posX_ < (bx1 + bx2) / 2 ? bx1 : bx2;
		var bpy:Number = simBall.posY_ < (by1 + by2) / 2 ? by1 : by2;
		var scoreMulX:Number = simBall.velX_ < 0 ? -1 : 1;
		var scoreMulY:Number = simBall.velY_ < 0 ? -1 : 1;
		if (ballCollBouncePoint(simBall, bpx, bpy, scoreMulX, scoreMulY, cRestPercent)) {
			return true;
		}
		return false;
	}
	
	public static function ballCollBounceVertLineSeg(simBall:SimBall, bx:Number, by1:Number, by2:Number,
			cRestPercent:int):Boolean {
		if (by2 <= by1) {
			throw new Error("coll line invalid y");
		}
		if ((cRestPercent <= 0) || (cRestPercent > 100)) {
			throw new Error("coll box invalid cRestPerc");
		}
		if (ballCollBounceVertEdgelessLineSeg(simBall, bx, by1, by2, cRestPercent)) {
			return true;
		}
		var bpy:Number = simBall.posY_ < (by1 + by2) / 2 ? by1 : by2;  
		var scoreMulY:Number = simBall.velY_ < 0 ? -1 : 1;
		if (ballCollBouncePoint(simBall, bx, bpy, 0, scoreMulY, cRestPercent)) {
			return true;
		}
		return false;
	}
	
	// b1: paddle bottom, b2: paddle top
	public static function ballCollBounceLineSeg(simBall:SimBall, bx1:Number, by1:Number, bx2:Number, by2:Number,
			velX:Number, velY:Number, cRestPercent:int, cFriction:Number):Boolean {
		if (by2 <= by1) {
			throw new Error("coll line invalid y, " + by2 + ", " + by1);
		}
		if ((cRestPercent <= 0) || (cRestPercent > 100)) {
			throw new Error("coll box invalid cRestPerc");
		}
		if (ballCollBounceEdgelessLineSeg(simBall, bx1, by1, bx2, by2, velX, velY, cRestPercent, cFriction)) {
			return true;
		}
		return false;
	}
	
	// line seg moved from b1-b2 to b3-b4 while ball stayed at the pos
	public static function lineSegCollBounceBall(bx1:Number, by1:Number, bx2:Number, by2:Number,
			 bx3:Number, by3:Number, bx4:Number, by4:Number, velX:Number, velY:Number,   
			 simBall:SimBall, cRestPercent:int, cFriction:Number):Boolean {
		if (by2 <= by1) {
			throw new Error("coll line invalid y, " + by2 + ", " + by1);
		}
		if ((cRestPercent <= 0) || (cRestPercent > 100)) {
			throw new Error("coll box invalid cRestPerc");
		}
		if (lineSegBallCollBounceEdgeless(bx1, by1, bx2, by2, bx3, by3, bx4, by4, velX, velY, 
				simBall, cRestPercent, cFriction)) {
			return true;
		}
		return false;
	}
	
	private static function ballCollBounceHorzEdgelessLineSeg(simBall:SimBall, bx1:Number, bx2:Number, by:Number, 
			cRestPercent:int):Boolean {
		if (simBall.velY_ > 0) {
			// to do
			return false;
		} else if (simBall.velY_ < 0) {
			var bLineIntX:Number = getLineIntersectionHorz(
				simBall.lastPosX_, simBall.lastPosY_ - SimBall.BALL_RADIUS_WORLD,
				simBall.posX_, simBall.posY_ - SimBall.BALL_RADIUS_WORLD, by);
			if (isNaN(bLineIntX)) {
				return false; // did not cross line
			}
			if (TRACE_DEBUG_MSG) {
				trace("  ballCollBounceHorzEdgelessLineSeg, bLineIntX: " + EngineUtil.roundBrief(bLineIntX) + 
					", ball pos: " + EngineUtil.roundBrief(simBall.posX_) + 
					", " + EngineUtil.roundBrief(simBall.posY_ - SimBall.BALL_RADIUS_WORLD));
			}
			if ((bLineIntX >= bx1) && (bLineIntX <= bx2))  {
				handleBounceHorz(simBall, cRestPercent, by);
				return true;
			}
		}
		return false;
	}
	
	private static function ballCollBounceVertEdgelessLineSeg(simBall:SimBall, bx:Number, by1:Number, by2:Number,
			cRestPercent:int):Boolean {
		var bLineIntY:Number;
		if (simBall.velX_ > 0) {
			bLineIntY = getLineIntersectionVert(
				simBall.lastPosX_ + SimBall.BALL_RADIUS_WORLD, simBall.lastPosY_,
				simBall.posX_ + SimBall.BALL_RADIUS_WORLD, simBall.posY_, bx);
			if (isNaN(bLineIntY)) {
				return false; // did not cross line
			}
			if (TRACE_DEBUG_MSG) {
				trace("  ballCollBounceVertEdgelessLineSeg, bLineIntY: " + EngineUtil.roundBrief(bLineIntY) + 
					", ball pos: " + EngineUtil.roundBrief(simBall.posX_ + SimBall.BALL_RADIUS_WORLD) + 
					", " + EngineUtil.roundBrief(simBall.posY_));
			}
		} else {
			if (simBall.lastPosX_ - SimBall.BALL_RADIUS_WORLD > 0 &&
				simBall.posX_ - SimBall.BALL_RADIUS_WORLD < 0) {
			}
			
			bLineIntY = getLineIntersectionVert(
				simBall.lastPosX_ - SimBall.BALL_RADIUS_WORLD, simBall.lastPosY_,
				simBall.posX_ - SimBall.BALL_RADIUS_WORLD, simBall.posY_, bx);
			if (isNaN(bLineIntY)) {
				return false; // did not cross line
			}
			if (TRACE_DEBUG_MSG) {
				trace("  ballCollBounceVertEdgelessLineSeg, bLineIntY: " + EngineUtil.roundBrief(bLineIntY) + 
					", ball pos: " + EngineUtil.roundBrief(simBall.posX_ - SimBall.BALL_RADIUS_WORLD) + 
					", " + EngineUtil.roundBrief(simBall.posY_));
			}
		}
		if ((bLineIntY >= by1) && (bLineIntY <= by2))  {
			handleBounceVert(simBall, cRestPercent, bx);
			return true;
		}
		return false;
	}
											  
	private static function ballCollBounceEdgelessLineSeg(simBall:SimBall, bx1:Number, by1:Number, bx2:Number, by2:Number, 
			velX:Number, velY:Number, cRestPercent:int, cFriction:Number):Boolean {
		var bp1x:Number = simBall.lastPosX_ + simBall.getVelUnitX() * SimBall.BALL_RADIUS_WORLD;
		var bp1y:Number = simBall.lastPosY_ + simBall.getVelUnitY() * SimBall.BALL_RADIUS_WORLD; 
		var bp2x:Number = simBall.posX_ + simBall.getVelUnitX() * SimBall.BALL_RADIUS_WORLD;
		var bp2y:Number = simBall.posY_ + simBall.getVelUnitY() * SimBall.BALL_RADIUS_WORLD; 
		var coll:Boolean = getLineIntersection(bp1x, bp1y, bp2x, bp2y, bx1, by1, bx2, by2);
		if (coll) {
			if (TRACE_DEBUG_MSG) {
				var bOriDeg:Number = Math.atan2(by2 - by1, bx2 - bx1) * 180 / Math.PI;
				var bpOriDeg:Number = Math.atan2(bp2y - bp1y, bp2x - bp1x) * 180 / Math.PI;
				var itOriDeg:Number = Math.atan2(t2_ - bp1y, t1_ - bp1x) * 180 / Math.PI;
				var bpLen:Number = Math.sqrt((bp2x - bp1x) * (bp2x - bp1x) + (bp2y - bp1y) * (bp2y - bp1y));
				var biLen:Number = Math.sqrt((t1_ - bp1x) * (t1_ - bp1x) + (t2_ - bp1y) * (t2_ - bp1y));
				trace("  >> ballCollBounceEdgelessLineSeg, obstacle ori: " + EngineUtil.roundBrief(bOriDeg) + 
					"deg in world (should be " + EngineUtil.roundBrief(90 - bOriDeg) + "deg in controller)" +
					"\n    ball traj ori: " + EngineUtil.roundBrief(bpOriDeg) + "deg" +
					", ball-intersection ori: " + EngineUtil.roundBrief(itOriDeg) + "deg (must be same)" +
					"\n    ball traj len: " + EngineUtil.roundBrief(bpLen) + 
					", ball intersecton len: " + EngineUtil.roundBrief(biLen) +
					", diff: " + EngineUtil.roundBrief(bpLen - biLen) + " (must be > 0)"
				);
			}
			handleBounceLine(simBall, cRestPercent, cFriction, bx2 - bx1, by2 - by1, velX, velY);
		}
		return coll;
	}
	
	private static function lineSegBallCollBounceEdgeless(bx1:Number, by1:Number, bx2:Number, by2:Number, 
			  bx3:Number, by3:Number, bx4:Number, by4:Number, velX:Number, velY:Number,  
			  simBall:SimBall, cRestPercent:int, cFriction:Number):Boolean {
		var b12cx:Number = (bx1 + bx2) / 2;
		var b12cy:Number = (by1 + by2) / 2;
		var b34cx:Number = (bx3 + bx4) / 2;
		var b34cy:Number = (by3 + by4) / 2;
		var bDispX:Number = b34cx - b12cx;
		var bDispY:Number = b34cy - b12cy;
		var bp1x:Number = simBall.lastPosX_;
		var bp1y:Number = simBall.lastPosY_; 
		var bp2x:Number = simBall.posX_ - bDispX; 
		var bp2y:Number = simBall.posY_ - bDispY;
		var unused:Number = simBall.posY_ - bDispY;
			
		var coll:Boolean = getLineIntersection(bp1x, bp1y, bp2x, bp2y, bx1, by1, bx2, by2);
		if (coll) {
			handleBounceLine(simBall, cRestPercent, cFriction, bx2 - bx1, by2 - by1, velX, velY);
		}
		return coll;			
	}
	
	public static function ballCollBouncePoint(simBall:SimBall, bpx:Number, bpy:Number, 
			scoreMulX:Number, scoreMulY:Number, cRestPercent:int):Boolean {
		if (getCircleLineInt(bpx, bpy, 
			bpx + (simBall.posX_ - simBall.lastPosX_), bpy + (simBall.posY_ - simBall.lastPosY_),
			simBall.posX_, simBall.posY_, SimBall.BALL_RADIUS_WORLD, scoreMulX, scoreMulY)) {
			var pointIntRelX:Number = t1_ - simBall.posX_;
			var pointIntRelY:Number = t2_ - simBall.posY_;
			if (TRACE_DEBUG_MSG) {
				var pointIntAngle:Number = Math.atan2(pointIntRelY, pointIntRelX);
				trace ("    circleLineInt rel pos: " + 
					EngineUtil.roundBrief(pointIntRelX) + ", " + 
					EngineUtil.roundBrief(pointIntRelY) + 
					", angle: " + EngineUtil.roundBrief(pointIntAngle * 180 / Math.PI) + "deg" +
					"\n                  vel: " + EngineUtil.roundBrief(simBall.velX_) + ", " + 
						EngineUtil.roundBrief(simBall.velY_));
			}
			handleBouncePoint(simBall, cRestPercent, bpx, bpy, pointIntRelX, pointIntRelY);
			return true;
		} else {
			if (TRACE_DEBUG_MSG) {
				var distRadiusDiff:Number = Math.sqrt((simBall.posX_ - bpx) * (simBall.posX_ - bpx) + 
					(simBall.posY_ - bpy) * (simBall.posY_ - bpy)) - SimBall.BALL_RADIUS_WORLD;
				var lastDistRadiusDiff:Number = Math.sqrt((simBall.lastPosX_ - bpx) * (simBall.lastPosX_ - bpx) + 
					(simBall.lastPosY_ - bpy) * (simBall.lastPosY_ - bpy)) - SimBall.BALL_RADIUS_WORLD;
				if ((distRadiusDiff < 0) || (lastDistRadiusDiff < 0)) {
					trace("    circleLineInt false, distRadiusDiff: " + EngineUtil.roundBrief(distRadiusDiff) + 
						", lastDistRadiusDiff: " + EngineUtil.roundBrief(lastDistRadiusDiff));
					throw new Error("!");
				}
			}
			return false;
		}
	}
	
	private static function handleBounceHorz(simBall:SimBall, cRestPercent:int, by:Number):void {
		var cRest:Number = SimBall.BALL_RESTITUTION_COEF * (cRestPercent / 100);
		var posBeforeY:Number = simBall.posY_;
		var contactPosY:Number = simBall.posY_  + 
			(simBall.velY_ > 0 ? SimBall.BALL_RADIUS_WORLD : -SimBall.BALL_RADIUS_WORLD);
		simBall.posY_ = by + (by - contactPosY) * cRest + 
			(simBall.velY_ > 0 ? -SimBall.BALL_RADIUS_WORLD : SimBall.BALL_RADIUS_WORLD);
		var velBeforeY:Number = simBall.velY_; 
		simBall.velX_ += simBall.spin_ * SimBall.BALL_CIRCUMFERENCE * SimBall.BALL_SPIN_TABLE_REACTION_RATIO * 
			(simBall.velY_ > 0 ? 1 : -1);
		simBall.velY_ *= -1 * cRest;
		simBall.spin_ *= (1 - SimBall.BALL_SPIN_TABLE_REACTION_RATIO);
		if (TRACE_DEBUG_MSG) {
			trace("    bounce horz, velY before: " + EngineUtil.roundBrief(velBeforeY) + 
				", after: " + EngineUtil.roundBrief(simBall.velY_) +
				", posY before: " + EngineUtil.roundBrief(posBeforeY) + 
				", posY after: " + EngineUtil.roundBrief(simBall.posY_));
		}
	}
	
	private static function handleBounceVert(simBall:SimBall, cRestPercent:int, bx:Number):void {
		var cRest:Number = SimBall.BALL_RESTITUTION_COEF * (cRestPercent / 100);
		var contactPosX:Number = simBall.posX_ + 
			(simBall.velX_ > 0 ? SimBall.BALL_RADIUS_WORLD : -SimBall.BALL_RADIUS_WORLD);
		simBall.posX_ = bx + (bx - contactPosX) * cRest + 
			(simBall.velX_ > 0 ? -SimBall.BALL_RADIUS_WORLD : SimBall.BALL_RADIUS_WORLD);
		simBall.velX_ *= -1 * cRest;
		if (TRACE_DEBUG_MSG) {
			trace("    bounce horz, velX after: " + EngineUtil.roundBrief(simBall.velX_) + 
				", posX after: " + EngineUtil.roundBrief(simBall.posX_));
		}
	}
	
	// ball intersected with line which moved with vel (and speed)
	private static function handleBounceLine(simBall:SimBall, cRestPercent:int, cFriction:Number, 
            lineX:Number, lineY:Number, velX:Number, velY:Number):void {
		ballCfg_.updateFromSimBall(simBall);
		handleBounceLineCfg(ballCfg_, cRestPercent, cFriction, lineX, lineY, velX, velY);
		simBall.updateFromBallCfg(ballCfg_);
	}		
		
	public static function handleBounceLineCfg(ballCfg:BallCfg, cRestPercent:int, cFriction:Number, 
											 lineX:Number, lineY:Number, velX:Number, velY:Number):void {
		var lineAngle:Number = Math.atan2(lineY, lineX);
		EngineUtil.rotateVec(velX, velY, -lineAngle);
		
		// spin component & lateral speed component
		var lineVelCompX:Number = EngineUtil.t1_ * cFriction; 
		
		// This is approximation. A correct way is to compute how long the ball contacted the paddle,
		// apply force (with friction), change rotation using rotational inertia, blah blah
		var spinBefore:Number = ballCfg.spin_; 
		ballCfg.spin_ = lineVelCompX / SimBall.BALL_CIRCUMFERENCE;
		ballCfg.spin_ = Math.min(SimBall.BALL_MAX_SPIN, ballCfg.spin_);
		ballCfg.spin_ = Math.max(-SimBall.BALL_MAX_SPIN, ballCfg.spin_);
		
		var lineVelCompY:Number = EngineUtil.t2_ * cRestPercent / 100; // bounce coomponent
		EngineUtil.rotateVec(lineVelCompX, lineVelCompY, lineAngle); 
		var lineVelBouncingX:Number = EngineUtil.t1_;
		var lineVelBouncingY:Number = EngineUtil.t2_;
		
		EngineUtil.rotateVec(ballCfg.velX_, ballCfg.velY_, -lineAngle); // relative to line
		var ballVelCompX:Number = EngineUtil.t1_ - spinBefore * SimBall.BALL_CIRCUMFERENCE * cFriction * 
			SimBall.BALL_SPIN_PADDLE_REACTION_RATIO * (ballCfg.velX_ > 0 ? 1 : -1); 
		var ballVelCompY:Number = EngineUtil.t2_;
		EngineUtil.rotateVec(ballVelCompX, -ballVelCompY, lineAngle);
		var ballVelBeforeX:Number = ballCfg.velX_;
		var ballVelBeforeY:Number = ballCfg.velY_; 
		ballCfg.velX_ = EngineUtil.t1_;
		ballCfg.velY_ = EngineUtil.t2_;
	    ballCfg.velX_ += lineVelBouncingX;
	    ballCfg.velY_ += lineVelBouncingY;
		
		if (TRACE_DEBUG_MSG) {
			trace("    handleBounceLine, line vel bouncing: " + EngineUtil.roundBrief(lineVelBouncingX) + 
				", " + EngineUtil.roundBrief(lineVelBouncingY) + 
				" ball vel changed from " + EngineUtil.roundBrief(ballVelBeforeX) + 
				", " + EngineUtil.roundBrief(ballVelBeforeY) + " to " + EngineUtil.roundBrief(ballCfg.velX_) + 
				", " + EngineUtil.roundBrief(ballCfg.velY_) + 
				" spin: " + EngineUtil.roundBrief(ballCfg.spin_));
		}
	}
	
	private static function handleBouncePoint(simBall:SimBall, cRestPercent:int, bpx:Number, bpy:Number, 
			pointIntRelX:Number, pointIntRelY:Number):void {
		var cRest:Number = SimBall.BALL_RESTITUTION_COEF * (cRestPercent / 100);
		var newSpeed:Number = simBall.getSpeed() * cRest;
		var reactionVelUnitX:Number = -1 * pointIntRelX / SimBall.BALL_RADIUS_WORLD;
		var reactionVelUnitY:Number = -1 * pointIntRelY / SimBall.BALL_RADIUS_WORLD;
		simBall.velX_ = reactionVelUnitX * newSpeed; 
		simBall.velY_ = reactionVelUnitY * newSpeed;
		if (TRACE_DEBUG_MSG) {
			trace("    bounce point, vel after: " + EngineUtil.roundBrief(simBall.velX_) + 
				", " + EngineUtil.roundBrief(simBall.velY_) + " pos before:" +
				EngineUtil.roundBrief(simBall.posX_) + ", " + EngineUtil.roundBrief(simBall.posY_) 
			);
		}
		simBall.posX_ = bpx - pointIntRelX * 1.2;
		simBall.posY_ = bpy - pointIntRelY * 1.2;
		simBall.lastPosX_ = NaN;
		if (TRACE_DEBUG_MSG) {
			trace("                  pos after: " + 
				EngineUtil.roundBrief(simBall.posX_) + ", " + EngineUtil.roundBrief(simBall.posY_) 
			);
		}
	}

	/*
	                   + a1 (or a2)
	                  +
	                 +
	            ====*===================== b (line)
	               +
	              + a2 (or a1)
	
	 return: x coord of *
	*/
	private static function getLineIntersectionHorz(ax1:Number, ay1:Number, ax2:Number, ay2:Number,
			by:Number):Number {
		if (Math.abs(ay1 - ay2) < 0.001) {
			if (Math.abs(ay1 - by) < 0.001) {
				return (ax1 + ax2) / 2;
			} else {
				return NaN;
			}
		}
		if (ay1 > ay2) {
			if (by < ay2) {
				return NaN;
			}
			if (by > ay1) {
				return NaN;
			}
			return ax2 + (ax1 - ax2) * (by - ay2) / (ay1 - ay2);
		} else {
			if (by < ay1) {
				// line a12 not intersecting b, too low
				return NaN;
			}
			if (by > ay2) {
				// line a12 not intersecting b, too high
				return NaN;
			}
			return ax1 + (ax2 - ax1) * (by - ay1) / (ay2 - ay1);
		}
	}
	
	public static function didBallHitNet(bc1:BallCfg, bc2:BallCfg, netX:Number, netY1:Number, netY2:Number):Boolean {
		if ((bc1.velX_ > 0) && (bc2.posX_ < netX)) {
			return false;
		}
		if ((bc1.velX_ < 0) && (bc2.posX_ > netX)) {
			return false;
		}
		var bLineIntY:Number = getLineIntersectionVert(bc1.posX_, bc1.posY_, bc2.posX_, bc2.posY_, netX);
		if (!isNaN(bLineIntY) && (bLineIntY >= netY1) && (bLineIntY <= netY2)) {
			return true;
		}
		return false;
	}
	
	private static function getLineIntersectionVert(ax1:Number, ay1:Number, ax2:Number, ay2:Number, bx:Number):Number {
		if (Math.abs(ax1 - ax2) < 0.001) {
			if (Math.abs(ax1 - bx) < 0.001) {
				return (ay1 + ay2) / 2;
			} else {
				return NaN;
			}
		}
		if (ax1 > ax2) {
			if (bx < ax2) {
				return NaN;
			}
			if (bx > ax1) {
				return NaN;
			}
			return ay2 + (ay1 - ay2) * (bx - ax2) / (ax1 - ax2);
		} else {
			if (bx < ax1) {
				return NaN;
			}
			if (bx > ax2) {
				return NaN;
			}
			return ay1 + (ay2 - ay1) * (bx - ax1) / (ax2 - ax1);
		}
	}
	
	// http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
	private static function getLineIntersection(p0x:Number, p0y:Number, p1x:Number, p1y:Number, 
            p2x:Number, p2y:Number, p3x:Number, p3y:Number):Boolean {
		t1_ = NaN;
		t2_ = NaN;
		var s1x:Number = p1x - p0x;
		var s1y:Number = p1y - p0y; 
		var s2x:Number = p3x - p2x;
		var s2y:Number = p3y - p2y;
		
		var s:Number = (-s1y * (p0x - p2x) + s1x * (p0y - p2y)) / (-s2x * s1y + s1x * s2y);
		var t:Number = ( s2x * (p0y - p2y) - s2y * (p0x - p2x)) / (-s2x * s1y + s1x * s2y);
		
		if (TRACE_DEBUG_MSG) {
			trace("  getLineIntersection " + EngineUtil.roundBrief(p0x) + ", " +
				EngineUtil.roundBrief(p0y) + " ~ " +
				EngineUtil.roundBrief(p1x) + ", " +
				EngineUtil.roundBrief(p1y) + " // " +
				EngineUtil.roundBrief(p2x) + ", " +
				EngineUtil.roundBrief(p2y) + " ~ " +
				EngineUtil.roundBrief(p3x) + ", " +
				EngineUtil.roundBrief(p3y) + ", s&t: " + EngineUtil.roundBrief(s) + ", " + EngineUtil.roundBrief(t));
		}
		if (s >= 0 && s <= 1 && t >= 0 && t <= 1) { // Collision detected
			t1_ = p0x + (t * s1x);
			t2_ = p0y + (t * s1y);
			if (TRACE_DEBUG_MSG) {
				trace("   -> COLL at " +
					EngineUtil.roundBrief(t1_) + ", " + EngineUtil.roundBrief(t2_) +
					", line A: " + 
					EngineUtil.roundBrief(p0x) + ", " + EngineUtil.roundBrief(p0y) + " ~ " + 
					EngineUtil.roundBrief(p1x) + ", " + EngineUtil.roundBrief(p1y) + ", line A:" + 
					EngineUtil.roundBrief(p2x) + ", " + EngineUtil.roundBrief(p2y) + " ~ " + 
					EngineUtil.roundBrief(p3x) + ", " + EngineUtil.roundBrief(p3y));
			}
			return true;
		}
		return false;
	}
	
	// line circle intersection
	// http://stackoverflow.com/questions/1073336/circle-line-collision-detection
	private static function getCircleLineInt(ex:Number, ey:Number, lx:Number, ly:Number,
			cx:Number, cy:Number, r:Number, scoreMulX:Number, scoreMulY:Number):Boolean {
		t1_ = NaN;
		t2_ = NaN;
		var dx:Number = lx - ex;
		var dy:Number = ly - ey;
		var fx:Number = ex - cx;
		var fy:Number = ey - cy;
		var a:Number = dx * dx + dy * dy;
		var b:Number = 2 * (ex * dx + ey * dy - dx * cx - dy * cy);
		var c:Number = ex * ex + ey * ey - 2 * ex * cx - 2 * ey * cy + cx * cx + cy * cy - r * r;
		var discriminantSq:Number = b * b - 4 * a * c;
		if (discriminantSq < 0) {
			// no intersection
			return false;
		}
		var discriminant:Number = Math.sqrt(discriminantSq);
		var t1:Number = (-b + discriminant) / (2 * a);
		var t2:Number = (-b - discriminant) / (2 * a);
		var intX1:Number = ex + t1 * dx;
		var intY1:Number = ey + t1 * dy;
		var intX2:Number = ex + t2 * dx;
		var intY2:Number = ey + t2 * dy;
		if ((t1 >= 0 && t1 <= 1) && (t2 >= 0 && t2 <= 1)) {
			var score1:Number = scoreMulX * intX1 + scoreMulY * intY1;
			var score2:Number = scoreMulX * intX2 + scoreMulY * intY2;
			if (score1 >= score2) {
				t1_ = intX1;
				t2_ = intY1;
			} else {
				t1_ = intX2;
				t2_ = intY2;
			}
			return true;
		} else if (t1 >= 0 && t1 <= 1) {
			t1_ = intX1;
			t2_ = intY1;
			return true;
		} else if (t2 >= 0 && t2 <= 1) {
			t1_ = intX2;
			t2_ = intY2;
			return true;
		} else {
			return false;
		}
	}
	
	// http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
	private static function pointNearLine(vx:Number, vy:Number, wx:Number, wy:Number, px:Number, py:Number):void {
		var l2:Number = (wx - vx) * (wx - vx) + (wy - vy) * (wy - vy);
		if (l2 < 0.0001) {
			throw new Error("pointNearLine: line is too short");
		}
		var t:Number = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2;
		t1_ = vx + t * (wx - vx);
		t2_ = vy + t * (wy - vy);
	}
}
}
package simulation
{
import core.EngineUtil;

import level.BallEvent;

import renderer.TableRenderer;

public class SimTable
{
	public static const TABLE_TOP_WIDTH:Number = 2.74;	// meters
	public static const TABLE_LEG_HEIGHT:Number = 0.76;	// meters
	public static const TABLE_NET_HEIGHT:Number = 0.15;	// meters
	public static const TABLE_TOP_PLAY_POS_DIST:Number = 0.3;
	private static const NET_RESTITUTION_COEF_PERCENT:Number = 30;
	private static const PATH_ESTIMATE_TABLE_WIDTH_MARGIN_RATIO:Number = 0.5;
	
	private var simWorld_:SimWorld;
	private var tableX1_:Number;
	private var tableX2_:Number;
	private var tableY1_:Number;
	private var tableY2_:Number;
	private var netX_:Number;
	private var netY1_:Number;
	private var netY2_:Number;

	public function SimTable(simWorld:SimWorld) {
		simWorld_ = simWorld;
	}
	
	public function init(levelWidth:int, levelHeight:int, tableRenderer:TableRenderer):void {
		tableX1_ = simWorld_.screenToWorldX(levelWidth / 2 - tableRenderer.tableTopWidth_ / 2);
		tableX2_ = simWorld_.screenToWorldX(levelWidth / 2 + tableRenderer.tableTopWidth_ / 2);
		tableY1_ = simWorld_.screenToWorldY(tableRenderer.tableTopPosY_ + tableRenderer.tableTopThickness_);
		tableY2_ = simWorld_.screenToWorldY(tableRenderer.tableTopPosY_);
		
		netX_ = simWorld_.screenToWorldX(levelWidth / 2);
		netY1_ = tableY2_ - simWorld_.screenToWorld_ * tableRenderer.tableTopThickness_;
		netY2_ = tableY2_ + TABLE_NET_HEIGHT;
		trace("I SimTable, table x: " + 
			EngineUtil.roundBrief(tableX1_) + " ~ " + EngineUtil.roundBrief(tableX2_) + 
			", y: " + EngineUtil.roundBrief(tableY1_) + " ~ " + EngineUtil.roundBrief(tableY2_) +
			", net y: " + EngineUtil.roundBrief(netY2_)
		);
	}
	
	public function cleanUp():void {
		
	}
	
	public function handleCollision(simBall:SimBall, frameStartTimeMsec:int):Boolean {
		const TRACE_DEBUG_MSG:Boolean = false;

		if (isNaN(simBall.posFrameX_)) {
			var collNum:int = 0;
			if (Collision.ballCollBounceBox(simBall, tableX1_, tableX2_, tableY1_, tableY2_, 100)) {
				collNum++;
				simBall.ballEventDispatcher_.dispatchEvent(new BallEvent(SimBall.EVENT_COLLISION, false, false, 
					simBall.posX_, simBall.posY_, simBall.velX_, simBall.velY_, SimBall.COLLISION_TYPE_TABLE));
			}
			if (Collision.ballCollBounceVertLineSeg(simBall, netX_, netY1_, netY2_, NET_RESTITUTION_COEF_PERCENT)) {
				collNum++;
				simBall.ballEventDispatcher_.dispatchEvent(new BallEvent(SimBall.EVENT_COLLISION, false, false, 
					simBall.posX_, simBall.posY_, simBall.velX_, simBall.velY_, SimBall.COLLISION_TYPE_NET));
			}
			if (collNum > 0) {
				simBall.invalidateSpeedCache();
				return true;
			}
		}
		return false;
	}
	
	// return 0: nothing, 1: collision with net, 2: collision with table
	static public function estimateCollision(curCfg:BallCfg, nextCfg:BallCfg, simCollSafetyMarginPrecent:int):int {
		if (Collision.didBallHitNet(curCfg, nextCfg, 0, TABLE_LEG_HEIGHT, 
				TABLE_LEG_HEIGHT + TABLE_NET_HEIGHT * (100 + simCollSafetyMarginPrecent) / 100)) {
			return 1;
		}
		var tableTopWidthSafe:Number = TABLE_TOP_WIDTH * (100 + simCollSafetyMarginPrecent) / 100;
		if ((curCfg.posY_ > TABLE_LEG_HEIGHT) && (nextCfg.posY_ < TABLE_LEG_HEIGHT) &&
			(nextCfg.posX_ > -tableTopWidthSafe / 2) && (nextCfg.posX_ < tableTopWidthSafe / 2)) {
			return 2;
		}
		return 0;
	}
	
	public function handleCollisionToSwing(curCfg:BallCfg, nextCfg:BallCfg):int {
		if ((nextCfg.posY_ < tableY2_) && (curCfg.velY_ < 0)) {
			var tableWidthMargin:Number = (tableX2_ - tableX1_) * PATH_ESTIMATE_TABLE_WIDTH_MARGIN_RATIO;
			if ((nextCfg.posX_ > (tableX1_ - tableWidthMargin)) && (nextCfg.posX_ < (tableX2_ + tableWidthMargin))) {
				var velAtCollY:Number = curCfg.velY_ + (nextCfg.velY_ - curCfg.velY_) * 
					(curCfg.posY_ - tableY2_) / (curCfg.posY_ - nextCfg.posY_);
				nextCfg.posX_ = curCfg.posX_ + (nextCfg.posX_ - curCfg.posX_) *
					(curCfg.posY_ - tableY2_) / (curCfg.posY_ - nextCfg.posY_);
				nextCfg.posY_ = tableY2_ + 0.01;
				nextCfg.velY_ = - velAtCollY;
				return 2;
			}
		}
		return 0;
	}
	
}
}
package simulation
{
public interface ISimUpdateObject
{
	function onSimUpdate(simNumber:int, simElapsedTime:Number):void;
	function onSimStart():void;
	function onSimStop():void;
}
}
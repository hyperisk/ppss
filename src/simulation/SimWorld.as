package simulation
{
import core.EngineUtil;
import core.GameConfig;
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import level.BallEvent;

import renderer.TableRenderer;

/*
   world coord system: origin at (screne center, table leg bottom), in meters
   +----------------------------------------+
   |                                        |
   |                   y                    |
   |                   ^                    |
   |                   |                    |
   |        @@@@@@@@@@@@@@@@@@@@@@@@@       |
   |           @       |         @          |
   |           @       +---> x   @          |
   |                                        |
   +----------------------------------------+
*/

public class SimWorld implements IFrameUpdateObject {
	public static const STATE_INACTIVE:String = "inactive";
	public static const STATE_SIMULATING:String = "simulating";
	public var states_: SimpleStateMachine;
	private var slowElapsedTimeMsec_:int;
	private var lastSimTimeMsec_:int;
	public var frameStartTimeAtFirstSim_:int;
	private var simNumber_:int;
	private static const BALL_PATH_ESTIMATE_TIME_INTERVAL:Number = 0.2;
	
	private static const TRACE_DEBUG_MSG:Boolean = false;

	public var simBall_:SimBall;
	public var simTable_:SimTable;
	public var simPaddleUserLeft_:SimPaddleUser;
	public var simPaddleDirectLeft_:SimPaddleDirect;
	public var simPaddleUserRight_:SimPaddleUser;
	public var simPaddleDirectRight_:SimPaddleDirect;
	public var estimatePath_:EstimatePath;
	private var simUpdateObject_:ISimUpdateObject; 
	
	// conversion helpers
	public var screenToWorld_:Number;
	public var worldToScreen_:Number;
	private var screenOrigX_:int;
	private var screenOrigY_:int;
	
	private var worldLevelWidthHalf_:Number; 	// half of level width
	private var worldLevelHeight_:Number; 		// from floor, i.e, table leg bottom (not from screen bottom)	
	public static const LEFT_RIGHT_MARGIN:Number = 0.01;
	
	public function SimWorld() {
		simBall_ = new SimBall(this);
		simTable_ = new SimTable(this);
		simPaddleUserLeft_ = new SimPaddleUser(this, SimBall.LEFT_SIDE); 
		simPaddleUserRight_ = new SimPaddleUser(this, SimBall.RIGHT_SIDE);
		simPaddleDirectLeft_ = new SimPaddleDirect(this, SimBall.LEFT_SIDE); 
		simPaddleDirectRight_ = new SimPaddleDirect(this, SimBall.RIGHT_SIDE);
		estimatePath_ = new EstimatePath(this);
		screenToWorld_ = 0;
		states_ = new SimpleStateMachine([
			[STATE_INACTIVE, 0],
			[STATE_SIMULATING, 0]
		]);
		StageUtil.getSingleton().addFrameUpdateObject(this);
	}

	// note: ball is not initialized at this time
	public function init(tableRenderer:TableRenderer):void {
		var levelWidth:int = StageUtil.getSingleton().stageWidth_;
		var levelHeight:int = StageUtil.getSingleton().stageHeight_;
		screenOrigX_ = levelWidth / 2;
		if (tableRenderer.tableWidthPercent_ <= 1) {
			throw new Error("invalid table width data");
		}
		worldLevelWidthHalf_ = SimTable.TABLE_TOP_WIDTH / (tableRenderer.tableWidthPercent_ / 100) / 2;
		worldToScreen_ = levelWidth / (worldLevelWidthHalf_ * 2);
		screenToWorld_ = 1 / worldToScreen_;
		screenOrigY_ = tableRenderer.tableTopPosY_ + tableRenderer.tableLegHeight_; 
		worldLevelHeight_ = screenOrigY_ * screenToWorld_; 
		trace("I init world, worldLevelWidthSide_: " + EngineUtil.roundBrief(worldLevelWidthHalf_) + 
			", worldLevelHeight_: " + EngineUtil.roundBrief(worldLevelHeight_) + 
			"\n  worldToScreen_: " + EngineUtil.roundBrief(worldToScreen_) +
			", screenOrigX_: " + screenOrigX_ + ", screenOrigY_: " + screenOrigY_);
		simTable_.init(levelWidth, levelHeight, tableRenderer);
		simUpdateObject_ = null;
	}
	
	public function cleanUp():void {
		stopSim();
		simTable_.cleanUp();
		simBall_.cleanUp();
	}
	
	public function setSimUpdateObject(o:ISimUpdateObject):void {
		if (simUpdateObject_) {
			throw new Error("sim update object already set");
		}
		simUpdateObject_ = o;
	}
	
	public function clearSimUpdateObject():void {
		if (!simUpdateObject_) {
			throw new Error("sim update object is not set");
		}
		simUpdateObject_ = null;
	}
	
	public function screenToWorld(a:int):Number {
		return a * screenToWorld_;
	}
	
	public function screenToWorldX(x:int, atOrigin:Boolean=false):Number {
		if (!atOrigin) {
			return (x - screenOrigX_) * screenToWorld_;
		} else {
			return x * screenToWorld_; 
		}
	}
	
	public function screenToWorldY(y:int):Number {
		return (screenOrigY_ - y) * screenToWorld_;
	}
	
	public function worldToScreen(a:Number):int {
		return a * worldToScreen_; 
	}
	
	public function worldToScreenX(x:Number, atOrigin:Boolean=false):int {
		if (!atOrigin) {
			return x * worldToScreen_ + screenOrigX_; 
		} else {
			return x * worldToScreen_; 
		}
	}
	
	public function worldToScreenY(y:Number, atOrigin:Boolean=false):int {
		if (!atOrigin) {
			return -1 * y * worldToScreen_ + screenOrigY_; 
		} else {
			return y * worldToScreen_; 
		}
	}
	
	//
	// start when ball becomes present, and stop and ball goes out of valid position
	//
	public function startSim(frameNumber:int, frameStartTimeMsec:int):void {
		if (isNaN(simBall_.posX_)) {
			throw new Error("start sim - ball pos is NaN");
		}
		slowElapsedTimeMsec_ = 0;
		lastSimTimeMsec_ = slowElapsedTimeMsec_;
		states_.setActiveState(STATE_SIMULATING);
		if (simUpdateObject_) {
			simUpdateObject_.onSimStart();
		}
		frameStartTimeAtFirstSim_ = frameStartTimeMsec;
		simNumber_ = 0;
		StageUtil.getSingleton().pauseAutoRegulateTargetFps_ = false;
	}
	
	public function stopSim():void {
		simBall_.ballEventDispatcher_.dispatchEvent(new BallEvent(SimBall.EVENT_CHANGE_SIDE, false, false,
			NaN, NaN, NaN, NaN, SimBall.INVALID_SIDE));
		simBall_.posX_ = NaN;
		if (simUpdateObject_) {
			simUpdateObject_.onSimStop();
		}
		states_.setActiveState(STATE_INACTIVE);
		StageUtil.getSingleton().pauseAutoRegulateTargetFps_ = true;
	}

	// interface impl 
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		if (states_.isStateActive(STATE_INACTIVE)) {
			return true;
		}
		
		slowElapsedTimeMsec_ += frameElapsedTime * 1000 *
			GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SLOW_DOWN_PERCENT) / 100;
		var targetSimElapsedTime:Number = getSimUpdateIntervel();
		var simElapsedTimeMsec:int = slowElapsedTimeMsec_ - lastSimTimeMsec_;
		if (simElapsedTimeMsec < targetSimElapsedTime * 1000) {
			if (TRACE_DEBUG_MSG) {
				trace("I sim world onFrameUpdate (frame " + frameStartTimeMsec + "), simElapsedTime: " + 
					EngineUtil.roundBrief(simElapsedTimeMsec) + 
					"msec is less than targetSimElapsedTime " + EngineUtil.roundBrief(targetSimElapsedTime * 1000));
			}
			simBall_.updateFrame(simElapsedTimeMsec / 1000);
			if (!isPositionValid(simBall_.posFrameX_, simBall_.posFrameY_)) {
				stopSim();
			}
			return true;
		}
		if (TRACE_DEBUG_MSG) {
			trace("I sim world onFrameUpdate (physics " + frameStartTimeMsec + "), simElapsedTime: " + 
				EngineUtil.roundBrief(simElapsedTimeMsec) + 
				"msec is greater than targetSimElapsedTime " + EngineUtil.roundBrief(targetSimElapsedTime * 1000));
		}
		lastSimTimeMsec_ += targetSimElapsedTime * 1000;
		simBall_.updatePhysics(targetSimElapsedTime);
		if (!isPositionValid(simBall_.posX_, simBall_.posY_)) {
			stopSim();
		}
		var didBallMove:Boolean = (Math.abs(simBall_.velX_) > 0.0001) || (Math.abs(simBall_.velY_) > 0.0001) 
		if (!isNaN(simBall_.lastPosX_) && didBallMove) {
			var collTable:Boolean = simTable_.handleCollision(simBall_, frameStartTimeMsec);
			if (collTable && TRACE_DEBUG_MSG) {
				trace("I   final ball pos: " + EngineUtil.roundBrief(simBall_.posX_) + ", " + 
					EngineUtil.roundBrief(simBall_.posY_));
			}
			
			var collPaddle:Boolean = false;
			if (simPaddleUserLeft_.isInitialized()) {
				collPaddle = simPaddleUserLeft_.handleCollision(targetSimElapsedTime);
			} else if (simPaddleDirectLeft_.isInitialized()) {
				collPaddle = simPaddleDirectLeft_.handleCollision(targetSimElapsedTime);
			} else {
				trace("W no paddle left initialized?");
			}
			if (simPaddleUserRight_.isInitialized()) {
				collPaddle = collPaddle || simPaddleUserRight_.handleCollision(targetSimElapsedTime);
			} else if (simPaddleDirectRight_.isInitialized()) {
				collPaddle = collPaddle || simPaddleDirectRight_.handleCollision(targetSimElapsedTime);
			}
		} else {
			trace("W ball physics updated but not check collision -- simBall_.lastPosX_: " + simBall_.lastPosX_ + ", didBallMove: " + didBallMove);
		}
		
		if (simUpdateObject_) {
			simUpdateObject_.onSimUpdate(simNumber_++, targetSimElapsedTime);
		}
		return true;
	}
	
	static public function getSimUpdateIntervel():Number {
		return 1 / GameConfig.getSingleton().getValue(GameConfig.PHYSICS_SIM_FPS_TAGET);
	}
	
	public function isPositionValid(x:Number, y:Number):Boolean {
		if (x < -1 * worldLevelWidthHalf_) {
			return false;
		}
		if (x > worldLevelWidthHalf_) {
			return false;
		}
		if (y < 0) {
			return false;
		}
		if (y > worldLevelHeight_) {
			return false;
		}
		
		return true;
	}
	
	public function getSimPaddleInitialized(side:String):SimPaddleBase {
		if (side == SimBall.LEFT_SIDE) {
			if (simPaddleUserLeft_.isInitialized() && simPaddleDirectLeft_.isInitialized()) {
				throw new Error("two sim paddles in the same side are initialized");
			} if (simPaddleUserLeft_.isInitialized()) {
				return simPaddleUserLeft_;
			} else if (simPaddleDirectLeft_.isInitialized()) {
				return simPaddleDirectLeft_;
			}  
		} else if (side == SimBall.RIGHT_SIDE) {
			if (simPaddleUserRight_.isInitialized() && simPaddleDirectRight_.isInitialized()) {
				throw new Error("two sim paddles in the same side are initialized");
			} if (simPaddleUserRight_.isInitialized()) {
				return simPaddleUserRight_;
			} else if (simPaddleDirectRight_.isInitialized()) {
				return simPaddleDirectRight_;
			}  
		}
		return null;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_SIM + "sim_world";	
	}
}
}
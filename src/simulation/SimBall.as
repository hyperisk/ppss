package simulation
{
import core.EngineUtil;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.events.EventDispatcher;

import level.BallEvent;

public class SimBall
{
	public var states_:SimpleStateMachine;
	public static const BALL_RADIUS_WORLD:Number = 0.02;
	public static const BALL_CIRCUMFERENCE:Number = 2 * Math.PI * BALL_RADIUS_WORLD;
	private static const BALL_MASS:Number = 2.7 / 1000;
	private static const BALL_AREA:Number = 0.0013;
	private static const BALL_DRAG_COEF:Number = 0.47;
	private static const AIR_DENSITY:Number = 1.2;
	public static const BALL_RESTITUTION_COEF:Number = 0.9;
	public static const BALL_MAX_SPEED:Number = 10;	// world record is about 30 m/s
	public static const BALL_MAX_SPIN:Number = 20;	// rev per sec
	public static const BALL_SPIN_PADDLE_REACTION_RATIO:Number = 0.5; // when ball is bouncing off paddle, reduce its spin effect
	public static const BALL_SPIN_TABLE_REACTION_RATIO:Number = 0.4;	
	private static const BALL_LIFT_PER_SPIN:Number = 0.0001;
	
	private var simWorld_:SimWorld;
	public var posX_:Number;
	public var posY_:Number;
	public var posAtCollX_:Number;
	public var posAtCollY_:Number;
	public var posFrameX_:Number;
	public var posFrameY_:Number;
	public var lastPosX_:Number;
	public var lastPosY_:Number;
	public var velX_:Number;
	public var velY_:Number;
	private var speedCached_:Number;
	public var spin_:Number;
	private const TRACE_DEBUG_MSG_SIM:Boolean = false;
	private const TRACE_DEBUG_MSG_FRAME:Boolean = false;
	public static const LEFT_SIDE:String = "left_side";
	public static const RIGHT_SIDE:String = "right_side";
	public static const INVALID_SIDE:String = "invalid_side";
	private var ballSide_:SimpleStateMachine;
	private var ballDirectionChangeListener_:Function;
	public static const EVENT_SET_INIT_POS:String = "event_set_init_pos";
	public static const INIT_POS_TYPE_OPPONENT_STRIKE:String = "init_pos_type_opponent_strike";
	public static const INIT_POS_TYPE_SERVICE:String = "init_pos_type_service";
	public static const EVENT_CHANGE_SIDE:String = "event_change_side";
	public static const EVENT_COLLISION:String = "event_collision";
	public static const COLLISION_TYPE_PADDLE:String = "collision_type_paddle"; 
	public static const COLLISION_TYPE_NET:String = "collision_type_net"; 
	public static const COLLISION_TYPE_TABLE:String = "collision_type_table";
	public var ballEventDispatcher_:EventDispatcher;
	
	public function SimBall(simWorld:SimWorld) {
		simWorld_ = simWorld;
		ballSide_ = new SimpleStateMachine([
			[INVALID_SIDE, 0],
			[LEFT_SIDE, 0],
			[RIGHT_SIDE, 0],
		]);
		ballEventDispatcher_ = new EventDispatcher();
		posX_ = NaN;
	}
	
	public function init(worldPosX:Number, worldPosY:Number, velX:Number, velY:Number, spin:Number, initPosSubtype:String):void {
		if (Math.abs(velX) + velY < 0.1) {
			throw new Error();
		}
		posX_ = worldPosX;
		posY_ = worldPosY;
		posFrameX_ = NaN;
		velX_ = velX;
		velY_ = velY;
		speedCached_ = NaN;
		lastPosX_ = NaN;
		spin_ = spin;
		updateBallSide();
		ballEventDispatcher_.dispatchEvent(new BallEvent(EVENT_SET_INIT_POS, false, false, posX_, posY_, velX_, velY_, initPosSubtype));
	}
	
	public function updateFromBallCfg(ballCfg:BallCfg):void {
		posX_ = ballCfg.posX_;
		posY_ = ballCfg.posY_;
		velX_ = ballCfg.velX_;
		velY_ = ballCfg.velY_;
		spin_ = ballCfg.spin_;
	}
	
	public function cleanUp():void {
	}
	
	private function updateBallSide():void {
		var posChangeEvent:BallEvent;
		if (ballSide_.isStateActive(INVALID_SIDE)) {
			if (posX_ < 0) {
				ballSide_.setActiveState(LEFT_SIDE);
			} else {
				ballSide_.setActiveState(RIGHT_SIDE);
			}
		} else if ((ballSide_.isStateActive(LEFT_SIDE) && (posX_ > SimWorld.LEFT_RIGHT_MARGIN))) {
			ballSide_.setActiveState(RIGHT_SIDE);
			posChangeEvent = new BallEvent(EVENT_CHANGE_SIDE, false, false, posX_, posY_, velX_, velY_, RIGHT_SIDE);			
			ballEventDispatcher_.dispatchEvent(posChangeEvent);
		} else if ((ballSide_.isStateActive(RIGHT_SIDE) && (posX_ < -SimWorld.LEFT_RIGHT_MARGIN))) {
			ballSide_.setActiveState(LEFT_SIDE);
			posChangeEvent = new BallEvent(EVENT_CHANGE_SIDE, false, false, posX_, posY_, velX_, velY_, LEFT_SIDE);			
			ballEventDispatcher_.dispatchEvent(posChangeEvent);
		}
	}
	
	public static function getOppositeSide(side:String):String {
		if (side == LEFT_SIDE) {
			return RIGHT_SIDE;
		} else if (side == RIGHT_SIDE) {
			return LEFT_SIDE;
		} else {
			throw new Error();
		}
	}
	
	public function inSameSide(side:String):Boolean {
		if (side == LEFT_SIDE && ballSide_.isStateActive(LEFT_SIDE)) {
			return true;
		} else if (side == RIGHT_SIDE && ballSide_.isStateActive(RIGHT_SIDE)) {
			return true;
		} 
		return false;
	}
	
	public function updatePhysics(elapsedTime:Number):void {
		// step 1. get all forces
		var fx:Number;
		var fy:Number;
		
		// gravity
		fx = 0;
		fy = -9.8 * BALL_MASS;
		
		// air drag
		var vsq:Number = getSpeedSq();
		if (vsq > 0.002) {
			var fd:Number = 0.5 * AIR_DENSITY * vsq * BALL_AREA * BALL_DRAG_COEF;
			fx -= fd * velX_ / getSpeed();
			fy -= fd * velY_ / getSpeed();
		}
		
		// spin lift
		EngineUtil.rotateVec(getVelUnitX(), getVelUnitY(), (spin_ > 0 ? 90 : -90) * Math.PI / 180);
		fx += EngineUtil.t1_ * Math.abs(spin_) * BALL_LIFT_PER_SPIN;
		fy += EngineUtil.t2_ * Math.abs(spin_) * BALL_LIFT_PER_SPIN;
		
		// step 2. update velocity
		velX_ += fx / BALL_MASS * elapsedTime;
		velX_ = Math.min(velX_, BALL_MAX_SPEED);
		velX_ = Math.max(velX_, -BALL_MAX_SPEED);
		velY_ += fy / BALL_MASS * elapsedTime;
		velY_ = Math.min(velY_, BALL_MAX_SPEED);
		velY_ = Math.max(velY_, -BALL_MAX_SPEED);
		speedCached_ = NaN;
		
		// step 3. update pos
		lastPosX_ = posX_;
		lastPosY_ = posY_;
		posX_ += velX_ * elapsedTime;
		posFrameX_ = NaN;
		posY_ += velY_ * elapsedTime;
		updateBallSide();

		if (TRACE_DEBUG_MSG_SIM) {
			trace("I sim ball update physics, world pos: " + EngineUtil.roundBrief(posX_) + ", " + EngineUtil.roundBrief(posY_) + 
				", vel: " + EngineUtil.roundBrief(velX_) + ", " + EngineUtil.roundBrief(velY_) +
				", speed: " + EngineUtil.roundBrief(getSpeed()) +
				", spin: " + EngineUtil.roundBrief(spin_) +
				", force: " + EngineUtil.roundBrief(fx) + ", " + EngineUtil.roundBrief(fy)
			);
		}
//		StageUtil.getSingleton().infoText_.setText("sim ball", "world pos: " + EngineUtil.roundBrief(posX_) + 
//			", " + EngineUtil.roundBrief(posY_)
//			//+ ", force: " + EngineUtil.roundBrief(fx) + ", " + EngineUtil.roundBrief(fy)
//		);
	}
	
	public function getSpeed():Number {
		if (isNaN(speedCached_)) {
			speedCached_ = Math.sqrt(getSpeedSq()); 
			return speedCached_;
		} else {
			return speedCached_;
		}
	}
	
	public function invalidateSpeedCache():void {
		speedCached_ = NaN;
	}
	
	public function getSpeedSq():Number {
		if (isNaN(velX_)) {
			return NaN;
		}
		return velX_ * velX_ + velY_ * velY_;
	}
	
	public function getVelUnitX():Number {
		if (!isNaN(velX_) && getSpeed() > 0) {
			return velX_ / getSpeed();
		} else {
			return 0;
		}
	}
	
	public function getVelSignX():Number {
		return velX_ >= 0 ? 1 : -1;
	}
	
	public function getVelUnitY():Number {
		if (!isNaN(velY_) && getSpeed() > 0) {
			return velY_ / getSpeed();
		} else {
			return 0;
		}
	}
	
	// interface impl
	public function updateFrame(elapsedTimeSinceLastPhysicsUpdate:Number):void {
		posFrameX_ = posX_ + velX_ * elapsedTimeSinceLastPhysicsUpdate;
		posFrameY_ = posY_ + velY_ * elapsedTimeSinceLastPhysicsUpdate;
		if (TRACE_DEBUG_MSG_FRAME) {
			trace("I sim ball update frame, world pos: " + EngineUtil.roundBrief(posFrameX_) + ", " + 
				EngineUtil.roundBrief(posFrameY_) + 
				", vel: (same) " +
				", speed: (same)" + 
				", elapsedTimeSinceLastPhysicsUpdate: " + EngineUtil.roundBrief(elapsedTimeSinceLastPhysicsUpdate));
		}
	}
	
	static public function updateEstimate(curCfg:BallCfg, nextCfg:BallCfg, elapsedTime:Number):void {
		
		// TODO: consider use higher order equations like Runge Kutta 
		
		var fx:Number = 0;
		var fy:Number = -9.8 * BALL_MASS; 
		
		var ballSpeedSq:Number = curCfg.getSpeedSq();
		var ballSpeed:Number = Math.sqrt(ballSpeedSq);
		if (ballSpeed > 0.1) {
			var fd:Number = 0.5 * AIR_DENSITY * ballSpeedSq * BALL_AREA * BALL_DRAG_COEF;
			fx -= fd * curCfg.velX_ / ballSpeed;
			fy -= fd * curCfg.velY_ / ballSpeed;
		}
		
		nextCfg.velX_ = curCfg.velX_ + fx / BALL_MASS * elapsedTime;
		nextCfg.velX_ = Math.min(nextCfg.velX_, BALL_MAX_SPEED);
		nextCfg.velX_ = Math.max(nextCfg.velX_, -BALL_MAX_SPEED);
		nextCfg.velY_ = curCfg.velY_ + fy / BALL_MASS * elapsedTime;
		nextCfg.velY_ = Math.min(nextCfg.velY_, BALL_MAX_SPEED);
		nextCfg.velY_ = Math.max(nextCfg.velY_, -BALL_MAX_SPEED);
		
		nextCfg.posX_ = curCfg.posX_ + nextCfg.velX_ * elapsedTime;
		nextCfg.posY_ = curCfg.posY_ + nextCfg.velY_ * elapsedTime;
		nextCfg.spin_ = curCfg.spin_;
	}
	
	// interface impl
	public function getDictKey():String {
		return "ball";
	}
}
}
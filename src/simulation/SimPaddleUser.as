package simulation
{
import core.GameConfig;
import core.PaddlePitchFileCache;
import core.StageUtil;

import flash.events.Event;

import level.IPaddleEventGenerator;
import level.PaddleEvent;

public class SimPaddleUser extends SimPaddleBase 
{
	private var tableTopPosY_:Number;
	private var maxFingerPaddleDist_:Number;
	private const TRACE_DEBUG_MSG:Boolean = false;
	private const TRACE_DEBUG_MSG_C:Boolean = false;
	
	private var eventPosX_:Number;
	private var eventPosY_:Number;
	
	private var posAvgElapsedTime_:Number;
	private const POS_AVG_TIME_INTERVAL:Number = 3 / GameConfig.INITIAL_FPS;
	private var posAvgLastFrameX_:Number;
	private var posAvgLastFrameY_:Number;
	private var velAvgX_:Number;
	private var velAvgY_:Number;
	private var lastFrameElapsedTime_:Number;

	public function SimPaddleUser(simWorld:SimWorld, side:String) {
		super(simWorld, side);
	}

	public override function init(eventGenerator:IPaddleEventGenerator, tableTopPosY:Number):void {
		super.init(eventGenerator, tableTopPosY);
		ballProximity_.setActiveState(BALL_PROXIMITY_GONE);
		tableTopPosY_ = tableTopPosY;
		maxFingerPaddleDist_ = SimTable.TABLE_LEG_HEIGHT * GameConfig.PADDLE_HANDLE_LENGTH_TABLE_LEG_RATIO;
		posAvgLastFrameX_ = NaN;
		eventPosX_ = NaN;
		velAvgX_ = 0;
		velAvgY_ = 0;
		lastFrameElapsedTime_ = 0;
	}
	
	public override function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		super.onFrameUpdate(frameNumber, frameStartTimeMsec, frameElapsedTime);
		
		if (isNaN(posAvgLastFrameX_) && !isNaN(eventPosX_)) {
			posAvgLastFrameX_ = eventPosX_;
			posAvgLastFrameY_ = eventPosY_;
			posAvgElapsedTime_ = 0;
		}
		
		if (!isNaN(posAvgLastFrameX_)) {
			posAvgElapsedTime_ += frameElapsedTime;
			if (posAvgElapsedTime_ > POS_AVG_TIME_INTERVAL) {
				velAvgX_ = (eventPosX_ - posAvgLastFrameX_) / POS_AVG_TIME_INTERVAL;
				velAvgY_ = (eventPosY_ - posAvgLastFrameY_) / POS_AVG_TIME_INTERVAL;
				posAvgElapsedTime_ -= POS_AVG_TIME_INTERVAL;
				posAvgLastFrameX_ = eventPosX_;
				posAvgLastFrameY_ = eventPosY_;
				lastFrameElapsedTime_ = frameElapsedTime;
			}
		}
		
		return true;
	}
	
	public override function onPaddleEvent(event:Event):void {
		var paddleEvent:PaddleEvent = event as PaddleEvent;
		if (isNaN(paddleEvent.posX_)) {
			throw new Error("paddleUser event pos is invalid");
		}
		
		eventPosX_ = paddleEvent.posX_;
		eventPosY_ = paddleEvent.posY_;

		var handleLen:Number = GameConfig.getSingleton().getValue(GameConfig.PADDLE_HANDLE_LENGTH) / 
				GameConfig.PADDLE_HANDLE_LENGTH_MAX_NUM_STEPS * maxFingerPaddleDist_;
		var handleVecUnitX:Number = (side_ == SimBall.LEFT_SIDE ? 1 : -1);
		var handleVecUnitY:Number = 1;

		var posNextFrameEstimatedX:Number = velAvgX_ * lastFrameElapsedTime_;
		var posNextFrameEstimatedY:Number = velAvgY_ * lastFrameElapsedTime_;
		posX_ = paddleEvent.posX_ + handleVecUnitX * handleLen + posNextFrameEstimatedX;
		posY_ = paddleEvent.posY_ + handleVecUnitY * handleLen + posNextFrameEstimatedY;

		dispatchSimOutputPaddleEvent();
	}

	private function updatePaddleControl():void {
		if (isNaN(getSpeedToHit())) {
			return;
		}
		if (side_ == SimBall.LEFT_SIDE) {
			if ((simWorld_.simBall_.velX_ > -0.1) || (simWorld_.simBall_.posX_ > 0)) {
				return;
			}
		}
		if (side_ == SimBall.RIGHT_SIDE) {
			if ((simWorld_.simBall_.velX_ < 0.1) || (simWorld_.simBall_.posX_ < 0)) {
				return;
			}
		}

		var p:Number = getPitchFromDatabase("SimPaddleUser@" + side_, true);
		if (!isNaN(p)) {
			pitchDeg_ = p;
		} else {
			var pitchDegLeft:int = 40 - Math.min(5, getSpeedToHit()) * 5;
			var pitchDegForPosY:int = (tableTopPosY_ - posY_) * 20 + 5; 
			pitchDeg_ = (pitchDegLeft + pitchDegForPosY) * (side_ == SimBall.RIGHT_SIDE ? -1 : 1);
			pitchDeg_ = Math.min(pitchDeg_, PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddlePitchMax()); 
			pitchDeg_ = Math.max(pitchDeg_, PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddlePitchMin()); 
		}
	}

	public override function handleCollision(simElapsedTime:Number):Boolean {
		return super.handleCollision(simElapsedTime);
	}
	
	public override function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "sim_paddleUser_" + side_;
	}
}
}
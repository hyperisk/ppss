package simulation
{
import core.EngineUtil;
import core.GameConfig;
import core.StageUtil;

import flash.utils.getTimer;

public class EstimatePath
{
	public var estimateSerialNum_:int;
	public var numSimSteps_:int; 
	public var estimatedCfgs_:Array;
	public var simTimeStep_:Number;
	private var simWorld_:SimWorld;
	private static var SIM_TIME_STEP_MIN:Number = 0.03;
	private static var SIM_TIME_STEP_MAX:Number = 0.08;
	public static const SIM_TIME_STEP_LARGE:String = "sim_time_step_large";
	public static const SIM_TIME_STEP_MEDIUM:String = "sim_time_step_medium";
	public static const SIM_TIME_STEP_SMALL:String = "sim_time_step_small";
	private static var MAX_SIM_TIME_SEC:Number = 5.0;
	private static var MAX_NUM_SIM_STEPS:int = MAX_SIM_TIME_SEC / SIM_TIME_STEP_MIN + 2;
	private const TRACE_DEBUG_MSG:Boolean = false;
	
	public function EstimatePath(simWorld:SimWorld) {
		simWorld_ = simWorld;
		estimateSerialNum_ = 0;
		numSimSteps_ = 0;
		estimatedCfgs_ = new Array(MAX_NUM_SIM_STEPS);
		for (var i:int = 0; i < MAX_NUM_SIM_STEPS; i++) {
			var newCfg:BallCfg = new BallCfg();
			estimatedCfgs_[i] = newCfg;
		}
	}
	
	public function run(simTimeStepType:String):void {
		estimateSerialNum_++;
		var simElapsedTime:Number = 0;
		
		numSimSteps_ = 0;
		var curCfg:BallCfg = estimatedCfgs_[0];
		var nextCfg:BallCfg = estimatedCfgs_[1];
		curCfg.updateFromSimBall(simWorld_.simBall_);
		switch (simTimeStepType) {
			case SIM_TIME_STEP_LARGE:
				simTimeStep_ = SIM_TIME_STEP_MAX;
				break;
			case SIM_TIME_STEP_MEDIUM:
				simTimeStep_ = (SIM_TIME_STEP_MAX + SIM_TIME_STEP_MIN) / 2;
				break;
			case SIM_TIME_STEP_SMALL:
				simTimeStep_ = SIM_TIME_STEP_MIN;
				break;
			default:
				throw new Error();
		}
		if (TRACE_DEBUG_MSG) {
			trace("I estimating ball path, MAX_NUM_SIM_STEPS: " + MAX_NUM_SIM_STEPS + 
				", simTimeStep: " + EngineUtil.roundBrief(simTimeStep_));
		}
		while (true) {
			if (!simWorld_.isPositionValid(curCfg.posX_, curCfg.posY_)) {
				if (TRACE_DEBUG_MSG) {
					trace("I   exit for invalid position");
				}
				break;
			}
			if (curCfg.getSpeedSq() < 0.3 * 0.3) {
				if (TRACE_DEBUG_MSG) {
					trace("I   exit for too-low speed");
				}
				break;
			}
			
			SimBall.updateEstimate(curCfg, nextCfg, simTimeStep_);
			simWorld_.simTable_.handleCollisionToSwing(curCfg, nextCfg);
			numSimSteps_++;
			
			if (numSimSteps_ >= MAX_NUM_SIM_STEPS) {
				throw new Error("too many estimate path steps");
			}

			simElapsedTime += simTimeStep_;
			if (simElapsedTime > MAX_SIM_TIME_SEC) {
				if (TRACE_DEBUG_MSG) {
					trace("I   exit for too-long simulation");
				}
				break;
			}
			
			curCfg = estimatedCfgs_[numSimSteps_];
			nextCfg = estimatedCfgs_[numSimSteps_ + 1];
		}
		
		if (TRACE_DEBUG_MSG) {
			trace("I numSimSteps estimated: " + numSimSteps_);
			for (var i:int = 0; i < numSimSteps_; i++) {
				if (TRACE_DEBUG_MSG) {
					estimatedCfgs_[i].traceThis("I   ");
				}
			}
		}
	}
}
}
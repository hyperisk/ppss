package simulation
{
import core.EngineUtil;

public class BallCfg
{
	public var posX_:Number;
	public var posY_:Number;
	public var velX_:Number;
	public var velY_:Number;
	public var spin_:Number;

	public function BallCfg() {
		init();
	}
	
	public function init():void {
		posX_ = NaN;
	}
	
	public function updateFromSimBall(simBall:SimBall):void {
		posX_ = simBall.posX_;
		posY_ = simBall.posY_;
		velX_ = simBall.velX_;
		velY_ = simBall.velY_;
		spin_ = simBall.spin_;
	}
	
	public function updateFromBallCfg(ballCfg:BallCfg):void {
		posX_ = ballCfg.posX_;
		posY_ = ballCfg.posY_;
		velX_ = ballCfg.velX_;
		velY_ = ballCfg.velY_;
		spin_ = ballCfg.spin_;
	}
	
	public function isValid():Boolean {
		return !isNaN(posX_);
	}
	
	public function getSpeedSq():Number {
		return velX_ * velX_ + velY_ * velY_;
	}
	
	public function getInfoString():String {
		return "pos: " + EngineUtil.roundBrief(posX_) + ", " + EngineUtil.roundBrief(posY_) +
			", vel: " + EngineUtil.roundBrief(velX_) + ", " + EngineUtil.roundBrief(velY_) +
			", spin: " + EngineUtil.roundBrief(spin_);
	}
	
	public function traceThis(prefix:String):void {
		trace(prefix + getInfoString());
	}
	
	public function mirrorToLeftSide():Boolean {
		if (velX_ > 0) {
			velX_ *= -1;
			posX_ *= -1;
			spin_ *= -1;
			return true;
		}
		return false;
	}
}
}
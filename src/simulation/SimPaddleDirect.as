package simulation
{
import core.IFrameUpdateObject;
import core.StageUtil;

import flash.events.Event;

import level.IPaddleEventGenerator;
import level.PaddleEvent;

public class SimPaddleDirect extends SimPaddleBase implements IFrameUpdateObject
{
	public function SimPaddleDirect(simWorld:SimWorld, side:String) {
		super(simWorld, side);
		capVelForPitchDatabase_ = false;
	}
	
	public override function init(eventGenerator:IPaddleEventGenerator, tableTopPosY:Number):void {
		super.init(eventGenerator, tableTopPosY);
	}
	
	public override function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		super.onFrameUpdate(frameNumber, frameStartTimeMsec, frameElapsedTime);
		return true;
	}
	
	public override function onPaddleEvent(event:Event):void {
		var paddleEvent:PaddleEvent = event as PaddleEvent;
		if (isNaN(paddleEvent.posX_)) {
			return;
			
			// why?
			//throw new Error("paddleDirect event pos is invalid");
		}
		posX_ = paddleEvent.posX_;
		posY_ = paddleEvent.posY_;
		
		var p:Number = getPitchFromDatabase("SimPaddleDirect@" + side_, true);
		if (!isNaN(p)) {
			pitchDeg_ = p;
		} else if (isNaN(pitchDeg_)) {
			pitchDeg_ = 15;
		}
		
		dispatchSimOutputPaddleEvent();
	}
	
	public override function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "sim_paddleDirect_" + side_;
	}
}
}
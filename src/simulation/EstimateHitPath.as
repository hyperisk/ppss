package simulation
{
	import core.EngineUtil;
	
	import flash.sampler.StackFrame;

public class EstimateHitPath
{
	public static var SIM_COLL_SATEFY_MARGIN_PERCENT:int = 5; 
	private static var MAX_SIM_TIME_SEC:Number = 5.0;
	public static var TRACE_DEBUG_MSG:Boolean = false;
	public static var TRACE_DEBUG_MSG_ALL_STEPS:Boolean = false;
	public static var TRACE_DEBUG_MSG_SEARCH:Boolean = false;
	
	public var estimateSerialNum_:int;
	private var simWorld_:SimWorld;
	private var nextCfg_:BallCfg;
	private var bouncedCfg_:BallCfg;

	public function EstimateHitPath() {
		estimateSerialNum_ = 0;
		nextCfg_ = new BallCfg();
		bouncedCfg_ = new BallCfg();
	}
	
	// return: -1: ball landed too near (hit net, etc),
	//          0: good
	//          1: ball flew over the table
	/*
	+----------------------------------------+
	|                                        |
	|                                        |
	|                                    1   |
	|               -1   |     0             |
	|        @@@@@@@@@@@@@@@@@@@@@@@@@       |
	|           @                 @          |
	|        -1 @                 @          |
	|           @                 @          |y
	|                                        |
	+----------------------------------------+
	*/
	
	public function run(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number, paddlePitchDeg:Number):int {
		var simTimeStep:Number = SimWorld.getSimUpdateIntervel();
		estimateSerialNum_++;
		var simElapsedTime:Number = 0;
		var simStep:int = 0;
		
		var traceDebugStr:String; 
		
		bouncedCfg_.updateFromBallCfg(ballCfg);
		SimPaddleBase.estimateHit(bouncedCfg_, paddleVelX, paddleVelY, paddlePitchDeg);
		if (TRACE_DEBUG_MSG) {
			trace("      start estimate hit, paddle pitch: " + EngineUtil.roundBrief(paddlePitchDeg) + 
				" ball vel before: " + EngineUtil.roundBrief(ballCfg.velX_) + ", " + EngineUtil.roundBrief(ballCfg.velY_) + 
				" after bounce: " + EngineUtil.roundBrief(bouncedCfg_.velX_) + ", " + EngineUtil.roundBrief(bouncedCfg_.velY_));
		}
		var returnType:int = NaN;
		var tableTopWidthSafe:Number = SimTable.TABLE_TOP_WIDTH * (100 + SIM_COLL_SATEFY_MARGIN_PERCENT) / 100;
		var debugStr:String;
		while (true) {
			if ((bouncedCfg_.posY_ < SimTable.TABLE_LEG_HEIGHT) &&
				(bouncedCfg_.posX_ > -tableTopWidthSafe / 2) && (bouncedCfg_.posX_ < 0) 
			) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...ball under table";
				}
				returnType = -1;
				break;
			}
			
			if (bouncedCfg_.posX_ > tableTopWidthSafe / 2) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...ball fly over table";
				}
				returnType = 1;
				break;
			}
			if (bouncedCfg_.posY_ > SimTable.TABLE_LEG_HEIGHT * 5) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...ball is too high";
				}
				returnType = 1;
				break;
			}
			if (bouncedCfg_.getSpeedSq() < 0.3 * 0.3) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...ball too slow";
				}
				returnType = -1;
				break;
			}
			SimBall.updateEstimate(bouncedCfg_, nextCfg_, simTimeStep);
			simStep++;
			
			var posType:int = SimTable.estimateCollision(bouncedCfg_, nextCfg_, SIM_COLL_SATEFY_MARGIN_PERCENT);
			if (posType == 0) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...?";
				}
			} else if (posType == 1) {
				if (TRACE_DEBUG_MSG) {
					debugStr = " ...hit net";
				}
				returnType = -1;
				break;
			} else if (posType == 2) {
				if (bouncedCfg_.posX_ > 0) {
					returnType = 0;
					if (TRACE_DEBUG_MSG) {
						debugStr = " ...hit table correct (right) side, ball pos X: " + EngineUtil.roundBrief(bouncedCfg_.posX_) + "!!!";
					}
				} else {
					returnType = -1;
					if (TRACE_DEBUG_MSG) {
						debugStr = " ...hit table wrong (left) side at: " + EngineUtil.roundBrief(bouncedCfg_.posX_);
					}
				}
				break;
			}
			var maxNumSteps:int = MAX_SIM_TIME_SEC / simTimeStep + 2;
			if (simStep >= maxNumSteps) {
				if (TRACE_DEBUG_MSG) {
					returnType = 1;
					debugStr = " ...simulated too many steps " + simStep + " >= " + maxNumSteps + " ballCfg: " + bouncedCfg_.getInfoString();
				}
				break;
			}
			simElapsedTime += simTimeStep;
			if (simElapsedTime > MAX_SIM_TIME_SEC) {
				if (TRACE_DEBUG_MSG) {
					returnType = 1;
					debugStr = " ...simulated too long time " + EngineUtil.roundBrief(simElapsedTime) + " ballCfg: " + bouncedCfg_.getInfoString();
				}
				break;
			}

			bouncedCfg_.updateFromBallCfg(nextCfg_);
			if (TRACE_DEBUG_MSG_ALL_STEPS) {
				trace("       sim step " + simStep + " ballCfg: " + bouncedCfg_.getInfoString() + debugStr);
			}
		}
		if (TRACE_DEBUG_MSG) {
			trace("       --> " + debugStr);
		}

		if (isNaN(returnType)) {
			throw new Error();
		}
		return returnType;
	}
	
	public function searchNeighbor(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number, paddlePitchDeg:Number, paddlePitchStep:int):Number {
		var maxNumIter:int = 5;
		var paddlePitchDegIter:Number = paddlePitchDeg;
		var paddlePitchDegLowerBound:Number = paddlePitchDegIter - 10;
		var paddlePitchDegUpperBound:Number = paddlePitchDegIter + 10; 
		if (TRACE_DEBUG_MSG_SEARCH) {
			trace("    estimate pitch step 2, search neighbor from " + EngineUtil.roundBrief(paddlePitchDeg) + "deg");
		}
		for (var iter:int = 0; iter < maxNumIter; iter++) {
			nextCfg_.updateFromBallCfg(ballCfg);
			var runResult:int = run(nextCfg_, paddleVelX, paddleVelY, paddlePitchDegIter);
			if (TRACE_DEBUG_MSG_SEARCH) {
				trace("     run #" + iter + " with pitch " + EngineUtil.roundBrief(paddlePitchDegIter) + "deg, " +
					" paddleVel " + EngineUtil.roundBrief(paddleVelX) + ", " + EngineUtil.roundBrief(paddleVelY) + 
					", result: " + runResult
					+ " sim end ballCfg: " + nextCfg_.getInfoString());
			}
			if (runResult == -1) {
				paddlePitchDegLowerBound = paddlePitchDegIter; 
				paddlePitchDegIter = (paddlePitchDegIter + paddlePitchDegUpperBound) / 2;
			} else if (runResult == 0) {
				return paddlePitchDegIter;
			} else {
				paddlePitchDegUpperBound = paddlePitchDegIter; 
				paddlePitchDegIter = (paddlePitchDegIter + paddlePitchDegLowerBound) / 2;
			}
		}
		return NaN;
	}
	
//	public function searchMaxPaddlePitch(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number, paddlePitchDeg:Number, paddlePitchStep:int):Number {
//		var maxNumIter:int = 5;
//		var paddlePitchDegIter:Number = paddlePitchDeg;
//		var lastValidPitch:Number = NaN;
//		for (var iter:int = 0; iter < maxNumIter; iter++) {
//			nextCfg_.updateFromBallCfg(ballCfg);
//			var runResult:int = run(nextCfg_, paddleVelX, paddleVelY, paddlePitchDegIter);
//			if (runResult == 0) {
//				lastValidPitch = paddlePitchDegIter;
//				paddlePitchDegIter
//			}
//		}
//	}
}
}
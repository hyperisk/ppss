package simulation
{
import core.EngineUtil;
import core.GameConfig;
import core.IFrameUpdateObject;
import core.PaddlePitchFileCache;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.events.Event;
import flash.events.EventDispatcher;

import level.BallEvent;
import level.IPaddleEventGenerator;
import level.PaddleEvent;

public class SimPaddleBase implements IFrameUpdateObject
{
	public static const PADDLE_HEIGHT:Number = 0.15;	// meters
	protected static const PADDLE_RESTITUTION_COEF_PERCENT:Number = 90;	// should be 89 ~ 92
	protected static const PADDLE_FRICTION_COEF:Number = 0.5;	// for spin
	protected static const PADDLE_MAX_SPEED:Number = 10;
	protected var simWorld_:SimWorld;
	protected var side_:String;
	protected var eventGenerator_:IPaddleEventGenerator;
	protected var posX_:Number;
	protected var posY_:Number;
	public var pitchDeg_:Number;
	private var hitSpeedCached_:Number;

	public var simOutputEventDispatcher_:EventDispatcher;
	
	protected var velCollX_:Number;
	private var lastVelCollGetPitchX_:Number;
	protected var velCollY_:Number;
	protected var lastCollPosX_:Number;
	protected var lastCollPosY_:Number;
	protected var capVelForPitchDatabase_:Boolean;
	
	public var ballProximity_:SimpleStateMachine;
	public var ballRegion_:SimpleStateMachine;
	public static const BALL_PROXIMITY_INCOMING:String = "ball_prox_incoming";
	public static const BALL_PROXIMITY_GONE:String = "ball_prox_gone";
	public static const BALL_PROXIMITY_HIT:String = "ball_prox_hit";
	public static const BALL_REGION_INVALID:String = "ball_region_invalid";
	public static const BALL_REGION_SAME_SIDE:String = "ball_region_same_side";
	public static const BALL_REGION_OTHER_SIDE:String = "ball_region_other_side";
	private const TRACE_DEBUG_MSG:Boolean = false;
	private const TRACE_DEBUG_MSG_PROX:Boolean = false;
	private const TRACE_DEBUG_MSG_PITCH:Boolean = false;
	private const TRACE_DEBUG_MSG_V:Boolean = false;
	private var highlightInfoText_:Boolean;
	private var getPitchErrorStr_:String;
	
	private var ballCfgTemp1_:BallCfg;
	private var ballCfgTemp2_:BallCfg;

	public function SimPaddleBase(simWorld:SimWorld, side:String) {
		simWorld_ = simWorld;
		side_ = side;
		simOutputEventDispatcher_ = new EventDispatcher();
		eventGenerator_ = null;
		capVelForPitchDatabase_ = true;

		ballProximity_ = new SimpleStateMachine([
			[BALL_PROXIMITY_GONE, 0],
			[BALL_PROXIMITY_INCOMING, 0],
			[BALL_PROXIMITY_HIT, 1],
		]);
		
		ballRegion_ = new SimpleStateMachine([
			[BALL_REGION_INVALID, 0],
			[BALL_REGION_SAME_SIDE, 0],
			[BALL_REGION_OTHER_SIDE, 0],
		]);
		
		simWorld.simBall_.ballEventDispatcher_.addEventListener(SimBall.EVENT_SET_INIT_POS, onBallEvent);
		simWorld.simBall_.ballEventDispatcher_.addEventListener(SimBall.EVENT_CHANGE_SIDE, onBallEvent);
		simWorld.simBall_.ballEventDispatcher_.addEventListener(SimBall.EVENT_COLLISION, onBallEvent);
		
		ballCfgTemp1_ = new BallCfg();
		ballCfgTemp2_ = new BallCfg();
	}
	
	public function init(eventGenerator:IPaddleEventGenerator, tableTopPosY:Number):void {
		eventGenerator_ = eventGenerator;
		eventGenerator.addEventListener(PaddleEvent.ACTION_SIM_INPUT, onPaddleEvent);
//		eventGenerator.addEventListener(PaddleEvent.ACTION_UP, onPaddleEvent);
		StageUtil.getSingleton().addFrameUpdateObject(this);
		
		posX_ = NaN;
		lastCollPosX_ = NaN;
		velCollX_ = 0;
		velCollY_ = 0;
		pitchDeg_ = 0;
		hitSpeedCached_ = NaN;
		highlightInfoText_ = (side_ == SimBall.LEFT_SIDE);
	}
	
	public function cleanUp():void {
		eventGenerator_.removeEventListener(PaddleEvent.ACTION_SIM_INPUT, onPaddleEvent);
//		eventGenerator_.removeEventListener(PaddleEvent.ACTION_UP, onPaddleEvent);
		eventGenerator_ = null;
		ballProximity_.setActiveState(BALL_PROXIMITY_GONE);
		ballRegion_.setActiveState(BALL_REGION_INVALID);
		StageUtil.getSingleton().removeFrameUpdateObject(this);
	}

	public function isInitialized():Boolean {
		return eventGenerator_ != null;
	}
	
	public function onPaddleEvent(event:Event):void {
		// override me
	}
	
	protected function dispatchSimOutputPaddleEvent():void {
		var simOutputPaddleEvent:PaddleEvent = new PaddleEvent(PaddleEvent.ACTION_SIM_OUTPUT);
		simOutputPaddleEvent.posX_ = posX_;
		simOutputPaddleEvent.posY_ = posY_;
		simOutputPaddleEvent.pitchDeg_ = pitchDeg_;
		simOutputEventDispatcher_.dispatchEvent(simOutputPaddleEvent);
	}
	
	public function onBallEvent(event:Event):void {
		var ballEvent:BallEvent = event as BallEvent;
		var inTheSameSide:Boolean = (ballEvent.posX_ < 0 ? (side_ == SimBall.LEFT_SIDE) : (side_ == SimBall.RIGHT_SIDE));
		var approaching:Boolean = (ballEvent.velX_ < 0 ? (side_ == SimBall.LEFT_SIDE) : (side_ == SimBall.RIGHT_SIDE));
		var paddleBallForwardDist:Number;
		if (!isNaN(posX_)) {
			var paddleBallRelPosX:Number = ballEvent.posX_ - posX_;
			var paddleBallRelPosY:Number = ballEvent.posY_ - posY_;
			paddleBallForwardDist = (side_ == SimBall.LEFT_SIDE ? paddleBallRelPosX : -paddleBallRelPosX);
		} else {
			paddleBallForwardDist = 1;
		}
		if (event.type == SimBall.EVENT_SET_INIT_POS) {
			if (approaching && (paddleBallForwardDist > 0)) {
				ballProximity_.setActiveState(BALL_PROXIMITY_INCOMING);
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball proximity for ' + side_, 
						'set to incoming (initial)', highlightInfoText_);
				}
			} else {
				ballProximity_.setActiveState(BALL_PROXIMITY_GONE);
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball proximity for ' + side_, 
						'set to gone (initial)', highlightInfoText_);
				}
			}
			
			if (inTheSameSide) {
				ballRegion_.setActiveState(BALL_REGION_SAME_SIDE); 
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball region for ' + side_, 
						'set to same side (initial)', highlightInfoText_);
				}
			} else {
				ballRegion_.setActiveState(BALL_REGION_OTHER_SIDE); 
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball region for ' + side_, 
						'set to other side (initial)', highlightInfoText_);
				}
			}
		} else if (event.type == SimBall.EVENT_CHANGE_SIDE) {
			if (inTheSameSide && !ballRegion_.isStateActive(BALL_REGION_SAME_SIDE)) {
				ballRegion_.setActiveState(BALL_REGION_SAME_SIDE)
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball region for ' + side_, 
						'set to same side (change region)', highlightInfoText_);
				}
			} else if (!inTheSameSide && !ballRegion_.isStateActive(BALL_REGION_OTHER_SIDE)) {
				ballRegion_.setActiveState(BALL_REGION_OTHER_SIDE)
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball region for ' + side_, 
						'set to other side (change region)', highlightInfoText_);
				}
			}
		} else if (event.type == SimBall.EVENT_COLLISION) {
			if (approaching && (paddleBallForwardDist > 0) && 
				(ballEvent.subtype_ == SimBall.COLLISION_TYPE_PADDLE) &&
				!ballProximity_.isStateActive(BALL_PROXIMITY_INCOMING)) {
				ballProximity_.setActiveState(BALL_PROXIMITY_INCOMING);
				if (TRACE_DEBUG_MSG_PROX) {
					StageUtil.getSingleton().infoText_.setText('ball proximity for ' + side_, 
						'set to incoming (coll)', highlightInfoText_);
				}
			}
		}
		
		if (!ballProximity_.isStateActive(BALL_PROXIMITY_INCOMING) && !isNaN(lastCollPosX_)) {
			lastCollPosX_ = NaN;
		}
	}
	
	public function handleCollision(simElapsedTime:Number):Boolean {
		if (!simWorld_.states_.isStateActive(SimWorld.STATE_SIMULATING)) {
			return false;
		}
		if (isNaN(posX_)) {
			return false;
		}
		if (isNaN(lastCollPosX_)) {
			lastCollPosX_ = posX_;
			lastCollPosY_ = posY_;
			return false;
		}
		if (!ballRegion_.isStateActive(BALL_REGION_SAME_SIDE)) {
			return false;
		}
		if (!ballProximity_.isStateActive(BALL_PROXIMITY_INCOMING)) {
			return false;
		}

		var paddleDispX:Number = posX_ - lastCollPosX_; 
		var paddleDispY:Number = posY_ - lastCollPosY_;
		velCollX_ = paddleDispX / simElapsedTime;
		velCollX_ = Math.min(PADDLE_MAX_SPEED, velCollX_);
		velCollX_ = Math.max(-PADDLE_MAX_SPEED, velCollX_);
		velCollY_ = paddleDispY / simElapsedTime;
		velCollY_ = Math.min(PADDLE_MAX_SPEED, velCollY_);
		velCollY_ = Math.max(-PADDLE_MAX_SPEED, velCollY_);
		if (capVelForPitchDatabase_) {
			capPaddleVelForPitchEstimateData();
		}
		if (velCollX_ != lastVelCollGetPitchX_) {
			var pitchDeg:Number = getPitchFromDatabase("SimPaddle.handleCollision@" + side_, false);
			if (!isNaN(pitchDeg)) {
				pitchDeg_ = pitchDeg;
			}
		}

		var paddleHeightMag:Number = PADDLE_HEIGHT * GameConfig.getSingleton().getValue(GameConfig.PADDLE_MAGNITUDE); 
		var pitchRad:Number = pitchDeg_ * (Math.PI / 180);
		var sinPitchRad:Number = Math.sin(pitchRad);
		var cosPitchRad:Number = Math.cos(pitchRad);
		var posTopX:Number = posX_ - sinPitchRad * paddleHeightMag / 2;
		var posTopY:Number = posY_ + cosPitchRad * paddleHeightMag / 2;
		var posBottomX:Number = posX_ + sinPitchRad * paddleHeightMag / 2;
		var posBottomY:Number = posY_ - cosPitchRad * paddleHeightMag / 2;
		var posLastTopX:Number = lastCollPosX_ - sinPitchRad * paddleHeightMag / 2;
		var posLastTopY:Number = lastCollPosY_ + cosPitchRad * paddleHeightMag / 2;
		var posLastBottomX:Number = lastCollPosX_ + sinPitchRad * paddleHeightMag / 2;
		var posLastBottomY:Number = lastCollPosY_ - cosPitchRad * paddleHeightMag / 2;

		var simBall:SimBall = simWorld_.simBall_;
		hitSpeedCached_ = NaN;
		var paddleDispMag:Number = Math.abs(paddleDispX) + Math.abs(paddleDispY);
		
		// step 1. if paddle moved, and may have jumped over the ball			
		var coll:Boolean = false;
		var logPaddleBallPos:Boolean = false;
		if (paddleDispMag > 0) {
			if (TRACE_DEBUG_MSG_V) {
				trace("I will check paddle-ball collision with paddle@" + side_ + " displacement " + EngineUtil.roundBrief(Math.sqrt(
					paddleDispX * paddleDispX + paddleDispY * paddleDispY)) + 
					", velColl: " + EngineUtil.roundBrief(velCollX_) + ", " + EngineUtil.roundBrief(velCollY_)
				);
				logPaddleBallPos = true;
			}
			var spinBefore:Number = simBall.spin_;
			var ballPosXBefore:Number = simBall.posX_;
			var ballVelXBefore:Number = simBall.velX_;
			coll = Collision.lineSegCollBounceBall(posLastBottomX, posLastBottomY, posLastTopX, posLastTopY,
				posBottomX, posBottomY, posTopX, posTopY, velCollX_, velCollY_,  
				simBall, PADDLE_RESTITUTION_COEF_PERCENT, PADDLE_FRICTION_COEF);
			if (coll) {
				simBall.invalidateSpeedCache();
			}
			if (TRACE_DEBUG_MSG && coll) {
				trace("I >>>");
				trace("I >>> simPaddle@" + side_ + " coll with PADDLE disp, hitSpeed: " + EngineUtil.roundBrief(getSpeedToHit()) +
					", paddle pitch " + EngineUtil.roundBrief(pitchDeg_) + "deg" +
					" (paddle vel: " + EngineUtil.roundBrief(velCollX_) + ", " + EngineUtil.roundBrief(velCollY_) + ")" +
					"\n  >>>    ball pos x before: " + EngineUtil.roundBrief(ballPosXBefore) +
					" after: " + EngineUtil.roundBrief(simBall.posX_) +
					", y: " + EngineUtil.roundBrief(simBall.posY_) + 
					", ball vel x before: " + EngineUtil.roundBrief(ballVelXBefore) +  
					" after: " + EngineUtil.roundBrief(simBall.velX_) +  
					", y after: " + EngineUtil.roundBrief(simBall.velY_) +
					", spin before: " + EngineUtil.roundBrief(spinBefore) + " after: " + EngineUtil.roundBrief(simBall.spin_)
				);
				trace("I >>>");
			}
		} else {
			if (TRACE_DEBUG_MSG_V) {
				trace("I will NOT check paddle-ball collision with paddle displacement = 0");
			}
		}
		
		// step 2. moving ball may have collided 
		if (!coll) {
			if (TRACE_DEBUG_MSG_V) {
				var ballDispX:Number = simBall.posX_ - simBall.lastPosX_; 
				var ballDispY:Number = simBall.posY_ - simBall.lastPosY_; 
				trace("I will check paddle@" + side_ + "-ball collision with ball displacement " + EngineUtil.roundBrief(Math.sqrt(
					ballDispX * ballDispX + ballDispY * ballDispY)));
				logPaddleBallPos = true;
			}
			coll = Collision.ballCollBounceLineSeg(simBall, posBottomX, posBottomY, posTopX, posTopY, velCollX_, velCollY_, 
				PADDLE_RESTITUTION_COEF_PERCENT, PADDLE_FRICTION_COEF);
			if (coll) {
				simBall.invalidateSpeedCache();
			}
			if (TRACE_DEBUG_MSG && coll) {
				trace("I ))) simPaddle@" + side_ + " coll with BALL disp, vel paddle: " + EngineUtil.roundBrief(getSpeedToHit()));
			}
		}
		
		if (logPaddleBallPos) {
			trace("   paddle pos: " + 
				EngineUtil.roundBrief(posX_) + ", " + EngineUtil.roundBrief(posY_) +
				", last pos: " + 
				EngineUtil.roundBrief(lastCollPosX_) + ", " + EngineUtil.roundBrief(lastCollPosY_) +
				"\n   ball pos: " + 
				EngineUtil.roundBrief(simBall.posX_) + ", " + EngineUtil.roundBrief(simBall.posY_) +
				", last pos: " + 
				EngineUtil.roundBrief(simBall.lastPosX_) + ", " + EngineUtil.roundBrief(simBall.lastPosY_) + 
				", p-b dist: " + EngineUtil.roundBrief(Math.sqrt(
					(posX_ - simBall.posX_) * (posX_ - simBall.posX_) + (posY_ - simBall.posY_) * (posY_ - simBall.posY_) 
				)) + (posX_ < simBall.posX_ ? " -->>" : " <<--") 
			);
		}
		
		if (coll) {
			ballProximity_.setActiveState(BALL_PROXIMITY_HIT);
			if (TRACE_DEBUG_MSG_PROX) {
				StageUtil.getSingleton().infoText_.setText('ball proximity for ' + side_, 'set to HIT', highlightInfoText_);
			}
			simBall.posAtCollX_ = simBall.posX_;
			simBall.posAtCollY_ = simBall.posY_;
			simBall.ballEventDispatcher_.dispatchEvent(new BallEvent(SimBall.EVENT_COLLISION, false, false, 
				simBall.posX_, simBall.posY_, simBall.velX_, simBall.velY_, SimBall.COLLISION_TYPE_PADDLE));
		}
		
		lastCollPosX_ = posX_;
		lastCollPosY_ = posY_;
		
		return coll;
	}
	
	public function isPosValid():Boolean {
		return !isNaN(posX_);
	}
	
	public function getPosX():Number {
		return posX_;
	}
	
	public function getPosY():Number {
		return posY_;
	}

	public function getSpeedToHit():Number {
		if (isNaN(hitSpeedCached_)) {
			hitSpeedCached_ = Math.sqrt(velCollX_ * velCollX_ + velCollY_ * velCollY_);  
		}
		if (isNaN(hitSpeedCached_)) {
			throw new Error("invalid speed to hit");
		}
		return hitSpeedCached_;
	}
	
	public function getVelX():Number {
		return velCollX_;
	}
	
	public function getVelY():Number {
		return velCollY_;
	}
	
	static public function estimateHit(ballCfg:BallCfg, paddleVelX:Number, paddleVelY:Number, paddlePitchDeg:Number):void {
		var paddleHeightMag:Number = PADDLE_HEIGHT * GameConfig.getSingleton().getValue(GameConfig.PADDLE_MAGNITUDE); 
		var pitchRad:Number = paddlePitchDeg * (Math.PI / 180);
		var sinPitchRad:Number = Math.sin(pitchRad);
		var cosPitchRad:Number = Math.cos(pitchRad);
		
		var lineX:Number = (- sinPitchRad * paddleHeightMag / 2) - (sinPitchRad * paddleHeightMag / 2);  
		var lineY:Number = (cosPitchRad * paddleHeightMag / 2) - (- cosPitchRad * paddleHeightMag / 2);
		
		Collision.handleBounceLineCfg(ballCfg, PADDLE_RESTITUTION_COEF_PERCENT, PADDLE_FRICTION_COEF,
			lineX, lineY, paddleVelX, paddleVelY);
		ballCfg.velX_ = Math.min(ballCfg.velX_, SimBall.BALL_MAX_SPEED);
		ballCfg.velX_ = Math.max(ballCfg.velX_, -SimBall.BALL_MAX_SPEED);
		ballCfg.velY_ = Math.min(ballCfg.velY_, SimBall.BALL_MAX_SPEED);
		ballCfg.velY_ = Math.max(ballCfg.velY_, -SimBall.BALL_MAX_SPEED);
	}
	
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		updateBallProximity(frameElapsedTime);
		return true;
	}
	
	protected function capPaddleVelForPitchEstimateData():void {
		var velSafeX:Number = velCollX_;
		if (side_ == SimBall.RIGHT_SIDE) {
			velSafeX *= -1;
		}
		var velSafeY:Number = velCollY_;
		var paddleVelMaxX:Number = PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddleVelMaxX();
		if (velSafeX > paddleVelMaxX) {
			if (TRACE_DEBUG_MSG_PITCH) {
				trace("  cap paddle@" + side_ + " velX from " + velSafeX + " to " + paddleVelMaxX + ", " + 
					(paddleVelMaxX / velSafeX * 100) + "%");  
			}
			velSafeY = velSafeY * paddleVelMaxX / velSafeX; 
			velSafeX = paddleVelMaxX;
		}
		
		var paddleVelMinY:Number = PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddleVelMinY();
		var paddleVelMaxY:Number = PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddleVelMaxY();
		if (velSafeY < paddleVelMinY) {
			if (TRACE_DEBUG_MSG_PITCH) {
				trace("  cap paddle@" + side_ + " velY from " + velSafeY + " to " + paddleVelMinY);  
			}
			velSafeX = velSafeX * paddleVelMinY / velSafeY; 
			velSafeY = paddleVelMinY;
		} else if (velSafeY > paddleVelMaxY) {
			if (TRACE_DEBUG_MSG_PITCH) {
				trace("  cap paddle@" + side_ + " velY from " + velSafeY + " to " + paddleVelMaxY);  
			}
			velSafeX = velSafeX * paddleVelMaxY / velSafeY; 
			velSafeY = paddleVelMaxY;
		}
		if (side_ == SimBall.RIGHT_SIDE) {
			velSafeX *= -1;
		}
		velCollX_ = velSafeX;
		velCollY_ = velSafeY;
	}
	
	protected function getPitchFromDatabase(fromWho:String, estimateNextFrame:Boolean):Number {
		var result:Number = NaN;
		if (estimateNextFrame) {
			ballCfgTemp2_.updateFromSimBall(simWorld_.simBall_);
			SimBall.updateEstimate(ballCfgTemp2_, ballCfgTemp1_, SimWorld.getSimUpdateIntervel()); 
		} else {
			ballCfgTemp1_.updateFromSimBall(simWorld_.simBall_);
		}
		var paddleVelX:Number = velCollX_;
		lastVelCollGetPitchX_ = velCollX_;
		if (side_ == SimBall.RIGHT_SIDE) {
			paddleVelX *= -1;
			ballCfgTemp1_.mirrorToLeftSide();
		}
		var paddleVelY:Number = velCollY_;
		var errorStr:String = PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.hasPaddlePitchEstimated(ballCfgTemp1_, paddleVelX, paddleVelY); 
		if (errorStr.length == 0) {
			if (TRACE_DEBUG_MSG_PITCH) {
				//				StageUtil.getSingleton().infoText_.setText("SimPaddleBase@" + side_, "pitch from DB: " + EngineUtil.roundBrief(result) + " deg", true);
				trace("I get pitch from DB, requested by " + fromWho +  
					", using " + ballCfgTemp1_.getInfoString() + " and paddle vel " + 
					EngineUtil.roundBrief(paddleVelX) + ", " + EngineUtil.roundBrief(paddleVelY) 
				);
			}

			result = PaddlePitchFileCache.getSingleton().paddlePitchDatabase_.getPaddlePitchEstimated(ballCfgTemp1_, paddleVelX, paddleVelY, true);
			
			if (TRACE_DEBUG_MSG_PITCH) {
				trace("  ==> pitch obtained: " + EngineUtil.roundBrief(result) + " deg");
			}
			if (side_ == SimBall.RIGHT_SIDE) {
				result *= -1;
			}
		} else {
			result = NaN;
			if (TRACE_DEBUG_MSG_PITCH && getPitchErrorStr_ != errorStr) {
//				StageUtil.getSingleton().infoText_.setText("SimPaddleBase@" + side_, "pitch from DB: out of range: " + errorStr, true);
				trace("I get pitch from DB, requested by " + fromWho + ", pitch obtained: NaN, why? " + errorStr + " ? ?  ?     ?");
			}
			getPitchErrorStr_ = errorStr;
		}
		return result;
	}
	
	private function updateBallProximity(frameElapsedTime:Number):void {
		//StageUtil.getSingleton().infoText_.setText("ball proximity", ballProximity_.getInfoText());
		ballProximity_.update(frameElapsedTime);
		if (ballProximity_.isStateActive(BALL_PROXIMITY_HIT) && ballProximity_.isActiveStateExpired()) {
			ballProximity_.setActiveState(BALL_PROXIMITY_GONE);
			if (TRACE_DEBUG_MSG_PROX) {
				StageUtil.getSingleton().infoText_.setText('ball proximity for ' + side_, 
					'set to gone (expired)', highlightInfoText_);
			}
		}
	}
	
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_SIM + "simPaddleBase";
	}
}
}
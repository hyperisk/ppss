package level
{
import core.IFrameUpdateObject;
import core.LevelManager;
import core.SimpleStateMachine;
import core.StageUtil;

import renderer.LauncherRenderer;

import simulation.SimBall;

public class Level_2 extends LevelCommonData implements ILevel, IFrameUpdateObject
{
	private static const tableWidthPercent:int = 45;
	private static const tableHeightPercent:int = 35;
	private var levelGeneral_:LevelGeneral;
	private var levelWithTable_:LevelWithTable;
	private var states_:SimpleStateMachine;

	function Level_2() {
		super();
	}
	
	public override function init(levelManager:LevelManager):void {
		states_ = new SimpleStateMachine([
			['show_ready', 0.3],
			['show_try_again', 0.3],
			['playing', 0]
		]);
		super.init(levelManager);
		levelGeneral_ = new LevelGeneral(this);
		levelWithTable_ = new LevelWithTable(this);
		
		levelWithTable_.showBackgroundAndTable(tableWidthPercent, tableHeightPercent);
		levelGeneral_.initSimWorld();
		levelWithTable_.init();
		levelWithTable_.addBallLauncher();
		levelWithTable_.showUserInputAndPaddleSinglePlay();
		levelWithTable_.showAiPlayerSinglePlay();
		levelGeneral_.showQuitButton();
		levelGeneral_.showBallStat();
		levelGeneral_.showBallEstimate();
		StageUtil.getSingleton().addFrameUpdateObject(this);
		
		levelManager_.rendererManager_.levelWidgetRenderer_.showTitleText("Level 2");
	}
	
	private function startPlaying(frameNumber:int, frameStartTimeMsec:int):void {
		trace("\n\n\nI ##### start playing");
		var nozzleRelPosLeftX:Number = 0;
		var nozzleRelPosY:Number = 0;
		launcherRenderer_.show(LauncherRenderer.ROLE_USER_OPPONENT,
			levelManager_.simWorld_.worldToScreenX(nozzleRelPosLeftX, true),
			levelManager_.simWorld_.worldToScreenY(nozzleRelPosY, true));
		levelGeneral_.onStartPlaying(frameNumber, frameStartTimeMsec, 
			5.6, 1, SimBall.BALL_MAX_SPIN * 0, launcherRenderer_
		);
		levelWithTable_.onStartPlaying();
		states_.setActiveState('playing');
	}
	
	// interface impl
	public function restart():void {
		states_.setActiveState('show_try_again');
		levelGeneral_.onRestart();
		levelWithTable_.onRestart();
		levelManager_.rendererManager_.levelWidgetRenderer_.showTitleText("Try Again");
	}
	
	// interface impl
	public function cleanUp():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		levelGeneral_.onCleanup();
		levelWithTable_.onCleanup();
		levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		states_.update(frameElapsedTime);
		if (states_.isStateActive('show_ready') && states_.isActiveStateExpired()) {
			levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
			startPlaying(frameNumber, frameStartTimeMsec);
		} else if (states_.isStateActive('show_try_again') && states_.isActiveStateExpired()) {
			levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
			startPlaying(frameNumber, frameStartTimeMsec);
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "level_2";
	}	
	
}
}
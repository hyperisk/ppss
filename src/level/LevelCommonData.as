package level
{
import core.LevelManager;

import renderer.LauncherRenderer;
import renderer.PaddleRenderer;
import renderer.UserControlWidget;

import simulation.SimPaddleBase;

public class LevelCommonData
{
	public var levelManager_:LevelManager;
	public var userControlWidgetLeft_:UserControlWidget;
	public var userControlWidgetRight_:UserControlWidget;
	public var simPaddleLeft_:SimPaddleBase;
	public var simPaddleRight_:SimPaddleBase;
	public var paddleRendererLeft_:PaddleRenderer;
	public var paddleRendererRight_:PaddleRenderer;
	public var aiPlayerTableLeft_:AiPlayerTable;
	public var aiPlayerTableRight_:AiPlayerTable;
	public var launcherRenderer_:LauncherRenderer;
	public var aiPlayerRendererLeft_:AiPlayerTableRenderer;
	public var aiPlayerRendererRight_:AiPlayerTableRenderer;
	
	public function LevelCommonData() {
	}
	
	public function init(levelManager:LevelManager):void {
		levelManager_ = levelManager;
	}
}
}
package level
{
import core.GameConfig;

import renderer.LauncherRenderer;

public class LevelGeneral
{
	private var lcd_:LevelCommonData;
	
	public function LevelGeneral(lcd:LevelCommonData) {
		lcd_ = lcd;
	}

	public function showQuitButton():void {
		lcd_.levelManager_.rendererManager_.levelWidgetRenderer_.showQuitButton();
	}
	
	public function initSimWorld():void {
		lcd_.levelManager_.simWorld_.init(lcd_.levelManager_.rendererManager_.tableRenderer_);
	}
	
	public function showBallStat():void {
		lcd_.levelManager_.rendererManager_.ballStatRenderer_.init(
			lcd_.levelManager_.rendererManager_.tableRenderer_);
	}
	
	public function showBallEstimate():void {
		lcd_.levelManager_.rendererManager_.ballPathEstimateRenderer_.init(
			lcd_.levelManager_.simWorld_.estimatePath_);
	}
	
	public function onStartPlaying(frameNumber:int, frameStartTimeMsec:int,  
			velLeftSideX:Number, velY:Number, spin:Number, launcher:LauncherRenderer):void {
		lcd_.levelManager_.rendererManager_.levelWidgetRenderer_.showReplayButton();
		var sideSign:int = GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 2 ? 1 : -1;
		if (launcher.getRole() == LauncherRenderer.ROLE_AI_OPPONENT) {
			sideSign *= -1;
		}
		lcd_.levelManager_.simWorld_.simBall_.init(
			lcd_.levelManager_.simWorld_.screenToWorldX(lcd_.launcherRenderer_.nozzlePosX_), 
			lcd_.levelManager_.simWorld_.screenToWorldY(lcd_.launcherRenderer_.nozzlePosY_),
			velLeftSideX * sideSign, velY, 
			spin /* * sideSign */,
			launcher.getInitPosSubtype()
		);
		lcd_.levelManager_.simWorld_.startSim(frameNumber, frameStartTimeMsec);
	}
	
	public function onRestart():void {
		lcd_.launcherRenderer_.hide();
		lcd_.levelManager_.simWorld_.stopSim();
		lcd_.levelManager_.rendererManager_.levelWidgetRenderer_.hideReplayButton();
	}
	
	public function onCleanup():void {
		lcd_.levelManager_.rendererManager_.levelWidgetRenderer_.hideQuitButton();
		lcd_.levelManager_.rendererManager_.levelWidgetRenderer_.hideReplayButton();
		lcd_.levelManager_.rendererManager_.ballStatRenderer_.cleanUp();
		lcd_.levelManager_.rendererManager_.ballPathEstimateRenderer_.cleanUp();
		lcd_.levelManager_.simWorld_.cleanUp();
	}
}
}
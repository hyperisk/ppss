package level
{
import core.GameConfig;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.events.Event;
import flash.events.EventDispatcher;

import renderer.BackgroundRenderer;
import renderer.TableRenderer;

import simulation.SimBall;

public class LevelWithTable
{
	public static const PLAY_NONE:String = "play_none";
	public static const PLAY_TOSS_UPWARD:String = "play_toss_upward";
	public static const PLAY_STRIKE_SERVICE:String = "play_strike_service";
	public static const PLAY_BOUNCE_SERVICE_COURT:String = "play_bounce_service_court";
	public static const PLAY_BOUNCE_TABLE:String = "play_bounce_table";
	public static const PLAY_STRIKE:String = "play_strike";
	public var ballPlayState_:SimpleStateMachine;
	private var lastTablePlayEventSubType_:String;
	private var lastTablePlayEventSide_:String;
	private var sideToScore_:String;
	private var lcd_:LevelCommonData;
	private const TRACE_DEBUG_MSG:Boolean = true;
	public var tablePlayEventDispatcher_:EventDispatcher;
	
	public function LevelWithTable(lcd:LevelCommonData) {
		lcd_ = lcd;
		ballPlayState_ = new SimpleStateMachine([
			[PLAY_NONE, 0],
			[PLAY_TOSS_UPWARD, 0],
			[PLAY_STRIKE_SERVICE, 0],
			[PLAY_BOUNCE_SERVICE_COURT, 0],
			[PLAY_BOUNCE_TABLE, 0],
			[PLAY_STRIKE, 0]
		]);
		tablePlayEventDispatcher_ = new EventDispatcher(); 
	}
	
	public function init():void {
		var ballEventDispatcher:EventDispatcher = lcd_.levelManager_.simWorld_.simBall_.ballEventDispatcher_;
		ballEventDispatcher.addEventListener(SimBall.EVENT_SET_INIT_POS, onBallEvent);
		ballEventDispatcher.addEventListener(SimBall.EVENT_CHANGE_SIDE, onBallEvent);
		ballEventDispatcher.addEventListener(SimBall.EVENT_COLLISION, onBallEvent);
		sideToScore_ = SimBall.INVALID_SIDE
		lastTablePlayEventSubType_ = "";
		tablePlayEventDispatcher_.dispatchEvent(new TablePlayEvent(TablePlayEvent.PLAY_EVENT_RESET, false, false, sideToScore_));		
	}
	
	public function showBackgroundAndTable(tableWidthPercent:int, tableHeightPercent:int):void {
		lcd_.levelManager_.rendererManager_.showBackground(BackgroundRenderer.TYPE_TABLE, tableHeightPercent);
		lcd_.levelManager_.rendererManager_.tableRenderer_.show(tableWidthPercent, tableHeightPercent);
	}
	
	public function addBallLauncher():void {
		if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 2) {
			lcd_.launcherRenderer_ = lcd_.levelManager_.rendererManager_.launcherRendererLeft_;
		} else if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 1) {
			lcd_.launcherRenderer_ = lcd_.levelManager_.rendererManager_.launcherRendererRight_;
		} else {
			throw new Error("invalid ball launcher side");
		}
	}
	
	public function showUserInputAndPaddleSinglePlay():void {
		var tableRenderer:TableRenderer = lcd_.levelManager_.rendererManager_.tableRenderer_;
		if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 2) {
			lcd_.userControlWidgetRight_ = lcd_.levelManager_.levelUserInputRight_;
			lcd_.userControlWidgetRight_.show(tableRenderer);
			lcd_.simPaddleRight_ = lcd_.levelManager_.simWorld_.simPaddleUserRight_;
			lcd_.paddleRendererRight_ = lcd_.levelManager_.rendererManager_.paddleRendererRight_;
			lcd_.simPaddleRight_.init(lcd_.userControlWidgetRight_, 
				lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
		} else if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 1) {
			lcd_.userControlWidgetLeft_ = lcd_.levelManager_.levelUserInputLeft_;
			lcd_.userControlWidgetLeft_.show(tableRenderer);
			lcd_.simPaddleLeft_ = lcd_.levelManager_.simWorld_.simPaddleUserLeft_;
			lcd_.paddleRendererLeft_ = lcd_.levelManager_.rendererManager_.paddleRendererLeft_;
			lcd_.simPaddleLeft_.init(lcd_.userControlWidgetLeft_, 
				lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
		} else {
			throw new Error("invalid user paddle side");
		}
	}
	
	public function showAiPlayerSinglePlay():void {
		var tableRenderer:TableRenderer = lcd_.levelManager_.rendererManager_.tableRenderer_;
		if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 2) {
			lcd_.aiPlayerTableLeft_ = lcd_.levelManager_.aiPlayerTableLeft_;
			lcd_.simPaddleLeft_ = lcd_.levelManager_.simWorld_.simPaddleDirectLeft_;
			lcd_.simPaddleLeft_.init(lcd_.aiPlayerTableLeft_, 
				lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
			lcd_.aiPlayerTableLeft_.init(ballPlayState_);
			lcd_.aiPlayerRendererLeft_ = lcd_.levelManager_.rendererManager_.aiPlayerTableRendererLeft_;
			lcd_.aiPlayerRendererLeft_.show();
			lcd_.paddleRendererLeft_ = lcd_.levelManager_.rendererManager_.paddleRendererLeft_;
		} else if (GameConfig.getSingleton().getValue(GameConfig.BALL_SINGLE_PADDLE_SIDE) == 1) {
			lcd_.aiPlayerTableRight_ = lcd_.levelManager_.aiPlayerTableRight_;
			lcd_.simPaddleRight_ = lcd_.levelManager_.simWorld_.simPaddleDirectRight_;
			lcd_.simPaddleRight_.init(lcd_.aiPlayerTableRight_, 
				lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
			lcd_.aiPlayerTableRight_.init(ballPlayState_);
			lcd_.aiPlayerRendererRight_ = lcd_.levelManager_.rendererManager_.aiPlayerTableRendererRight_;
			lcd_.aiPlayerRendererRight_.show();
			lcd_.paddleRendererRight_ = lcd_.levelManager_.rendererManager_.paddleRendererRight_;
		} else {
			throw new Error("invalid ai player side");
		}
	}
	
	public function showUserInputAndPaddleMultiPlay():void {
		var tableRenderer:TableRenderer = lcd_.levelManager_.rendererManager_.tableRenderer_;
		lcd_.userControlWidgetLeft_ = lcd_.levelManager_.levelUserInputLeft_;
		lcd_.userControlWidgetRight_ = lcd_.levelManager_.levelUserInputRight_;
		lcd_.simPaddleLeft_ = lcd_.levelManager_.simWorld_.simPaddleUserLeft_;
		lcd_.simPaddleLeft_.init(lcd_.userControlWidgetLeft_, 
			lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
		lcd_.simPaddleRight_ = lcd_.levelManager_.simWorld_.simPaddleUserRight_;
		lcd_.simPaddleRight_.init(lcd_.userControlWidgetRight_, 
			lcd_.levelManager_.simWorld_.screenToWorldY(tableRenderer.tableTopPosY_));
		lcd_.paddleRendererLeft_ = lcd_.levelManager_.rendererManager_.paddleRendererLeft_;
		lcd_.paddleRendererRight_ = lcd_.levelManager_.rendererManager_.paddleRendererRight_;
	}
	
	public function onStartPlaying():void {
		var tableRenderer:TableRenderer = lcd_.levelManager_.rendererManager_.tableRenderer_;
		if (lcd_.userControlWidgetLeft_) {
			lcd_.userControlWidgetLeft_.show(tableRenderer);
		}
		if (lcd_.userControlWidgetRight_) {
			lcd_.userControlWidgetRight_.show(tableRenderer);
		}
		if (lcd_.paddleRendererLeft_) {
			lcd_.paddleRendererLeft_.show();
		}
		if (lcd_.paddleRendererRight_) {
			lcd_.paddleRendererRight_.show();
		}
		if (lcd_.aiPlayerTableLeft_) {
			lcd_.aiPlayerTableLeft_.onStartPlaying();
		}
		if (lcd_.aiPlayerTableRight_) {
			lcd_.aiPlayerTableRight_.onStartPlaying();
		}
	}
	
	public function onRestart():void {
		if (lcd_.paddleRendererLeft_) {
			lcd_.paddleRendererLeft_.hide();
		}
		if (lcd_.paddleRendererRight_) {
			lcd_.paddleRendererRight_.hide();
		}
		if (lcd_.userControlWidgetLeft_) {
			lcd_.userControlWidgetLeft_.hide();
		}
		if (lcd_.userControlWidgetRight_) {
			lcd_.userControlWidgetRight_.hide();
		}
	}
	
	public function onCleanup():void {
		lcd_.launcherRenderer_.hide();
		if (lcd_.paddleRendererLeft_) {
			lcd_.paddleRendererLeft_.hide();
		}
		if (lcd_.paddleRendererRight_) {
			lcd_.paddleRendererRight_.hide();
		}
		if (lcd_.userControlWidgetLeft_) {
			lcd_.userControlWidgetLeft_.hide();
		}
		if (lcd_.userControlWidgetRight_) {
			lcd_.userControlWidgetRight_.hide();
		}
		if (lcd_.aiPlayerTableLeft_) {
			lcd_.aiPlayerTableLeft_.cleanUp();
		}
		if (lcd_.aiPlayerRendererLeft_) {
			lcd_.aiPlayerRendererLeft_.hide();
		}
		if (lcd_.aiPlayerTableRight_) {
			lcd_.aiPlayerTableRight_.cleanUp();
		}
		if (lcd_.aiPlayerRendererRight_) {
			lcd_.aiPlayerRendererRight_.hide();
		}
		if (lcd_.simPaddleLeft_) {
			lcd_.simPaddleLeft_.cleanUp();
		}
		if (lcd_.simPaddleRight_) {
			lcd_.simPaddleRight_.cleanUp();
		}
		
		lcd_.levelManager_.rendererManager_.tableRenderer_.hide();
		
		var ballEventDispatcher:EventDispatcher = lcd_.levelManager_.simWorld_.simBall_.ballEventDispatcher_;
		ballEventDispatcher.removeEventListener(SimBall.EVENT_SET_INIT_POS, onBallEvent);
		ballEventDispatcher.removeEventListener(SimBall.EVENT_CHANGE_SIDE, onBallEvent);
		ballEventDispatcher.removeEventListener(SimBall.EVENT_COLLISION, onBallEvent);
	}
	
	public function onBallEvent(event:Event):void {
		var ballEvent:BallEvent = event as BallEvent;
		var ballEventSide:String = ballEvent.posX_ > 0 ? SimBall.RIGHT_SIDE : SimBall.LEFT_SIDE;
		var ballPlayStateChanged:Boolean = false;
		if (event.type == SimBall.EVENT_SET_INIT_POS) {
			lastTablePlayEventSide_ = ballEventSide;
			if (ballEvent.subtype_ == SimBall.INIT_POS_TYPE_OPPONENT_STRIKE) {
				ballPlayState_.setActiveState(PLAY_STRIKE);
				sideToScore_ = SimBall.getOppositeSide(ballEventSide);
			} else if (ballEvent.subtype_ == SimBall.INIT_POS_TYPE_SERVICE) {
				ballPlayState_.setActiveState(PLAY_TOSS_UPWARD);
				sideToScore_ = SimBall.getOppositeSide(ballEventSide);
			}
			ballPlayStateChanged = true;
		} else if (event.type == SimBall.EVENT_COLLISION) {
			if (ballEvent.subtype_ == SimBall.COLLISION_TYPE_PADDLE) {
				if (ballPlayState_.isStateActive(PLAY_TOSS_UPWARD)) {
					ballPlayState_.setActiveState(PLAY_STRIKE_SERVICE);
					ballPlayStateChanged = true;
				} else if (ballPlayState_.isStateActive(PLAY_BOUNCE_TABLE)) {
					ballPlayState_.setActiveState(PLAY_STRIKE);
					lastTablePlayEventSide_ = ballEventSide;
					ballPlayStateChanged = true;
				}
			} else if (ballEvent.subtype_ == SimBall.COLLISION_TYPE_TABLE) {
				var sameAsLastSide:Boolean = (ballEventSide == lastTablePlayEventSide_); 
				if (ballPlayState_.isStateActive(PLAY_STRIKE_SERVICE)) {
					if (sameAsLastSide) {
						ballPlayState_.setActiveState(PLAY_BOUNCE_SERVICE_COURT);
						ballPlayStateChanged = true;
					} else {
						ballPlayState_.setActiveState(PLAY_NONE);
						ballPlayStateChanged = true;
					}
				} else if (ballPlayState_.isStateActive(PLAY_BOUNCE_SERVICE_COURT)) {
					if (sameAsLastSide) {
						ballPlayState_.setActiveState(PLAY_NONE);
						ballPlayStateChanged = true;
					} else {
						ballPlayState_.setActiveState(PLAY_BOUNCE_TABLE);
						ballPlayStateChanged = true;
					}
				} else if (ballPlayState_.isStateActive(PLAY_STRIKE)) {
					if (sameAsLastSide) {
						ballPlayState_.setActiveState(PLAY_NONE);
						ballPlayStateChanged = true;
					} else {
						ballPlayState_.setActiveState(PLAY_BOUNCE_TABLE);
						sideToScore_ = SimBall.getOppositeSide(ballEventSide);
						ballPlayStateChanged = true;
					}
				} else if (ballPlayState_.isStateActive(PLAY_BOUNCE_TABLE)) {
					ballPlayState_.setActiveState(PLAY_NONE);
					ballPlayStateChanged = true;
				}
			} else if (ballEvent.subtype_ == SimBall.COLLISION_TYPE_NET) {
				if (lastTablePlayEventSubType_ == SimBall.COLLISION_TYPE_NET) {
					if (!ballPlayState_.isStateActive(PLAY_NONE)) {
						ballPlayState_.setActiveState(PLAY_NONE);
						ballPlayStateChanged = true;
					}
				}
			}
		} else if (event.type == SimBall.EVENT_CHANGE_SIDE) {
			if (ballEvent.subtype_ == SimBall.INVALID_SIDE) {
				if (!ballPlayState_.isStateActive(PLAY_NONE)) {
					ballPlayState_.setActiveState(PLAY_NONE);
					ballPlayStateChanged = true;
				}
			}
		}
		
		if (ballPlayStateChanged) {
			if (ballPlayState_.isStateActive(PLAY_NONE)) {
				tablePlayEventDispatcher_.dispatchEvent(new TablePlayEvent(TablePlayEvent.PLAY_EVENT_SCORE, false, false, sideToScore_));
				sideToScore_ = SimBall.INVALID_SIDE;
			}
			
			if (TRACE_DEBUG_MSG) {
				trace("I set ballPlayState to [" + ballPlayState_.getInfoText() +  "] at " + ballEventSide +
					" (changed from " + lastTablePlayEventSubType_ + ")");
				
				StageUtil.getSingleton().infoText_.setText("LevelWithTable", "ballPlayState: " + ballPlayState_.getInfoText());
			}
			
			lastTablePlayEventSide_ = ballEventSide;
			lastTablePlayEventSubType_ = ballEvent.subtype_;
		}
	}
	
	public function setAiPlayerSwingParams(swingStyle:String, swingSpeed:String):void {
		if (lcd_.aiPlayerTableLeft_) {
			lcd_.aiPlayerTableLeft_.estimateSwing_.setSwingParams(swingStyle, swingSpeed);
		}
		if (lcd_.aiPlayerTableRight_) {
			lcd_.aiPlayerTableRight_.estimateSwing_.setSwingParams(swingStyle, swingSpeed);
		}
	}
}
}
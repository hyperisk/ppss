package level
{
import flash.events.Event;

public class TablePlayEvent extends Event
{
	public static const PLAY_EVENT_RESET:String = "play_event_reset";
	public static const PLAY_EVENT_SCORE:String = "play_event_score";
	public var side_:String;
	
	public function TablePlayEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, side:String="?") {
		super(type, bubbles, cancelable);
		side_ = side;	
	}
}
}
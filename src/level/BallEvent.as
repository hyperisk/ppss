package level
{
import flash.events.Event;

public class BallEvent extends Event
{
	public var posX_:Number;
	public var posY_:Number;
	public var velX_:Number;
	public var velY_:Number;
	public var subtype_:String;
	
	public function BallEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, 
	   	posX:Number=NaN, posY:Number=NaN, velX:Number=NaN, velY:Number=NaN, subtype:String="") {
		posX_ = posX;
		posY_ = posY;
		velX_ = velX;
		velY_ = velY;
		subtype_ = subtype;
		super(type, bubbles, cancelable);
	}
}
}
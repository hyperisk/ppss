package level
{
import core.EngineUtil;
import core.SimpleStateMachine;

import flash.events.EventDispatcher;

import simulation.EstimateSwing;
import simulation.ISimUpdateObject;
import simulation.SimBall;
import simulation.SimPaddleDirect;
import simulation.SimWorld;

public class AiPlayerTable implements ISimUpdateObject, IPaddleEventGenerator
{
	private var side_:String;
	private var simWorld_:SimWorld;
	private var simPaddle_:SimPaddleDirect;
	private var ballPlayState_:SimpleStateMachine;
	private var simInputEventDispatcher_:EventDispatcher;
	
	public var estimateSwing_:EstimateSwing;
	public var state_:SimpleStateMachine;
	public static const STATE_AI_PLAY_IDLE:String = "idle";
	public static const STATE_AI_PLAY_WORKING:String = "working";
	
	private const TRACE_DEBUG_MSG:Boolean = false;
	
	public function AiPlayerTable(simWorld:SimWorld, side:String) {
		simWorld_ = simWorld;
		side_ = side;
		state_ = new SimpleStateMachine([
			[STATE_AI_PLAY_IDLE, 0],
			[STATE_AI_PLAY_WORKING, 0]
		]);
		estimateSwing_ = new EstimateSwing(simWorld, side);
		simInputEventDispatcher_ = new EventDispatcher();
	}
	
	public function init(ballPlayState:SimpleStateMachine):void {
		simPaddle_ = (side_ == SimBall.LEFT_SIDE ? simWorld_.simPaddleDirectLeft_ : simWorld_.simPaddleDirectRight_);
		ballPlayState_ = ballPlayState;
		if (!simPaddle_.isInitialized()) {
			throw new Error("sim paddle not initialized");
		}
		simWorld_.setSimUpdateObject(this);
		estimateSwing_.init();
	}
	
	public function onStartPlaying():void {
		state_.setActiveState(STATE_AI_PLAY_WORKING);
		estimateSwing_.onStartPlaying();
	}
	
	public function cleanUp():void {
		state_.setActiveState(STATE_AI_PLAY_IDLE);
		simWorld_.clearSimUpdateObject();
	}
	
	// interface impl
	public function onSimStart():void {
		estimateSwing_.onSimStart();
	}
	
	// interface impl
	public function onSimStop():void {
		estimateSwing_.onSimStop();
	}

	// interface impl
	public function onSimUpdate(simNumber:int, simElapsedTime:Number):void {
		estimateSwing_.onSimUpdate(simNumber, simElapsedTime);			
		updateCommand(simNumber);
	}
	
	private function updateCommand(simNumber:int):void {
		var paddleEvent:PaddleEvent = new PaddleEvent(PaddleEvent.ACTION_SIM_INPUT);
		estimateSwing_.setEventPosFromEstimate(paddleEvent);
		simInputEventDispatcher_.dispatchEvent(paddleEvent);  
		
		if (TRACE_DEBUG_MSG) {
			trace("I dispatch paddle event from AI, pos: " + EngineUtil.roundBrief(paddleEvent.posX_) + 
				", " + EngineUtil.roundBrief(paddleEvent.posY_));
		}
	}
	
	// interface impl
	public function addEventListener(type:String, listner:Function):void {
		simInputEventDispatcher_.addEventListener(type, listner);
	}
	
	// interface impl
	public function removeEventListener(type:String, listner:Function):void {
		simInputEventDispatcher_.removeEventListener(type, listner);
	}
}
}
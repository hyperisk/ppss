package level
{
import core.IFrameUpdateObject;

import flash.events.Event;
import flash.events.EventDispatcher;

import renderer.ScoreRenderer;

import simulation.SimBall;

public class GameWithTableScore implements IFrameUpdateObject
{
	private var scoreRenderer_:ScoreRenderer;
	private var scoreLeft_:int;
	private var scoreRight_:int;
	
	public function GameWithTableScore(tablePlayEventDispatcher:EventDispatcher, scoreRendere:ScoreRenderer) {
		tablePlayEventDispatcher.addEventListener(TablePlayEvent.PLAY_EVENT_RESET, onTablePlayEvent);
		tablePlayEventDispatcher.addEventListener(TablePlayEvent.PLAY_EVENT_SCORE, onTablePlayEvent);
		scoreRenderer_ = scoreRendere;
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		return true;
	}
	
	// interface impl
	public function toString():String {
		return null;
	}

	private function onScoreChanged():void {
		scoreRenderer_.drawScore(SimBall.LEFT_SIDE, scoreLeft_);
		scoreRenderer_.drawScore(SimBall.RIGHT_SIDE, scoreRight_);
	}
	
	public function onTablePlayEvent(event:Event):void {
		var tablePlayEvent:TablePlayEvent = event as TablePlayEvent;

		if (tablePlayEvent.type == TablePlayEvent.PLAY_EVENT_RESET) {
			scoreLeft_ = 0;
			scoreRight_ = 0;
			onScoreChanged();
		} else if (tablePlayEvent.type == TablePlayEvent.PLAY_EVENT_SCORE) {
			if (tablePlayEvent.side_ == SimBall.INVALID_SIDE) {
				throw new Error();
			}
			trace("I onTablePlayEvent " + tablePlayEvent.type + " at " + tablePlayEvent.side_);
			if (tablePlayEvent.side_ == SimBall.LEFT_SIDE) {
				scoreLeft_++;
			} else {
				scoreRight_++;
			}
			onScoreChanged();
		}
	}
}
}
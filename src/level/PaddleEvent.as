package level
{
import flash.events.Event;

public class PaddleEvent extends Event
{
	public static const ACTION_SIM_INPUT:String = "sim_input";
	public static const ACTION_SIM_OUTPUT:String = "sim_output";
	
	public var posX_:Number;
	public var posY_:Number;
	public var pitchDeg_:Number;
	
	public function PaddleEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, posX:Number=NaN, posY:Number=NaN) {
		posX_ = posX;
		posY_ = posY;
		super(type, bubbles, cancelable);
		if ((type != ACTION_SIM_INPUT) && (type != ACTION_SIM_OUTPUT)) {
			throw new Error();
		}
	}
}
}
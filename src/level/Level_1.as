package level
{
import core.EngineUtil;
import core.IFrameUpdateObject;
import core.LevelManager;
import core.SimpleStateMachine;
import core.StageUtil;

import renderer.LauncherRenderer;
import renderer.ScoreRenderer;

import simulation.EstimateSwing;
import simulation.SimBall;

public class Level_1 extends LevelCommonData implements ILevel, IFrameUpdateObject
{
	private static const tableWidthPercent:int = 40;
	private static const tableHeightPercent:int = 35;
	private var levelGeneral_:LevelGeneral;
	private var levelWithTable_:LevelWithTable;
	private var gameWithTableScore_:GameWithTableScore;
	private var scoreRendere_:ScoreRenderer;
	private var states_:SimpleStateMachine;
	
	function Level_1() {
		super();
	}
	
	public override function init(levelManager:LevelManager):void {
		states_ = new SimpleStateMachine([
			['show_ready', 0.3],
			['show_try_again', 0.3],
			['playing', 0]
		]);
		super.init(levelManager);
		levelGeneral_ = new LevelGeneral(this);
		levelWithTable_ = new LevelWithTable(this);
		scoreRendere_ = new ScoreRenderer();
		gameWithTableScore_ = new GameWithTableScore(levelWithTable_.tablePlayEventDispatcher_, scoreRendere_); 
		
		levelWithTable_.showBackgroundAndTable(tableWidthPercent, tableHeightPercent);
		levelGeneral_.initSimWorld();
		levelWithTable_.init();
		levelWithTable_.addBallLauncher();
		levelWithTable_.showUserInputAndPaddleSinglePlay();
		levelWithTable_.showAiPlayerSinglePlay();
		levelGeneral_.showQuitButton();
		levelGeneral_.showBallStat();
		levelGeneral_.showBallEstimate();
		StageUtil.getSingleton().addFrameUpdateObject(this);
		scoreRendere_.show();
		
		levelManager_.rendererManager_.levelWidgetRenderer_.showTitleText("Level 1");
		EngineUtil.randomSeed_ = -2123110752;
	}
	
	private function startPlaying(frameNumber:int, frameStartTimeMsec:int):void {
		var randomSeedAtStart:int = EngineUtil.randomSeed_;
		var nozzleRelPosLeftX:Number = EngineUtil.seededRandom() * 1.3 - 1.4;
		var nozzleRelPosY:Number = EngineUtil.seededRandom() * 0.5 - 0.3;
		var ballInitialVelX:Number = 6 - nozzleRelPosLeftX * 2;
		var ballInitialVelY:Number = 2 - nozzleRelPosY;
		trace("\n\n\nI ##### start playing" + 
		      "\n  nozzleRelPos: " + EngineUtil.roundBrief(nozzleRelPosLeftX) + ", " + EngineUtil.roundBrief(nozzleRelPosY) +
			  "\n  randomSeed: " + randomSeedAtStart
			);
		launcherRenderer_.show(LauncherRenderer.ROLE_AI_OPPONENT,
			levelManager_.simWorld_.worldToScreenX(nozzleRelPosLeftX, true),
			levelManager_.simWorld_.worldToScreenY(nozzleRelPosY, true));
		var swingStyle:String;
		var swingSpeed:String;
		if (EngineUtil.seededRandom() > 0.7) {
			swingStyle = EstimateSwing.SWING_STYLE_SHORT_HIT;
			swingSpeed = EstimateSwing.SWING_SPEED_SLOW;
		} else if (EngineUtil.seededRandom() > 0.3) {
			swingStyle = EstimateSwing.SWING_STYLE_LONG_HIT;
			swingSpeed = EstimateSwing.SWING_SPEED_MEDIUM;
		} else {
			swingStyle = EstimateSwing.SWING_STYLE_LONG_HIT;
			swingSpeed = EstimateSwing.SWING_SPEED_FAST;
		}
		levelWithTable_.setAiPlayerSwingParams(swingStyle, swingSpeed);
		levelGeneral_.onStartPlaying(frameNumber, frameStartTimeMsec, 
			ballInitialVelX, ballInitialVelY, SimBall.BALL_MAX_SPIN * -1, launcherRenderer_
		);
		levelWithTable_.onStartPlaying();
		states_.setActiveState('playing');
	}
	
	// interface impl
	public function restart():void {
		states_.setActiveState('show_try_again');
		levelGeneral_.onRestart();
		levelWithTable_.onRestart();
		levelManager_.rendererManager_.levelWidgetRenderer_.showTitleText("Try Again");
	}
	
	// interface impl
	public function cleanUp():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		levelGeneral_.onCleanup();
		levelWithTable_.onCleanup();
		levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
		scoreRendere_.hide();
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number): Boolean {
		states_.update(frameElapsedTime);
		if (states_.isStateActive('show_ready') && states_.isActiveStateExpired()) {
			levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
			startPlaying(frameNumber, frameStartTimeMsec);
		} else if (states_.isStateActive('show_try_again') && states_.isActiveStateExpired()) {
			levelManager_.rendererManager_.levelWidgetRenderer_.hideTitleText();
			startPlaying(frameNumber, frameStartTimeMsec);
		}
		
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_USER + "Level_1";
	}	
}
}
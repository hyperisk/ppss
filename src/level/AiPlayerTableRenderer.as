package level
{
import core.IFrameUpdateObject;
import core.SimpleStateMachine;
import core.StageUtil;

import flash.display.Sprite;

import simulation.BallCfg;
import simulation.EstimateSwing;
import simulation.SimWorld;

public class AiPlayerTableRenderer implements IFrameUpdateObject
{
	private var simWorld_:SimWorld;
	private var aiPlayer_:AiPlayerTable;
	private var hitTarget_:Sprite;
	private var estimatedCfg_:Sprite;
	private var targetSwing_:Sprite;
	private var debugState_:SimpleStateMachine = new SimpleStateMachine([
		['hidden', 0],
		['showing', 0]
	]);
	private static const TARGET_HIT_WIDGET_RADIUS:Number = 0.08; 
	
	public function AiPlayerTableRenderer(simWorld:SimWorld, aiPlayer:AiPlayerTable) {
		simWorld_ = simWorld;
		aiPlayer_ = aiPlayer;
		hitTarget_ = new Sprite;
		estimatedCfg_ = new Sprite;
		targetSwing_ = new Sprite;
	}

	public function show():void {
		StageUtil.getSingleton().addFrameUpdateObject(this);
		StageUtil.getSingleton().addToStage(hitTarget_, StageUtil.STAGE_OBJECT_GROUP_DEBUGINFO);
		StageUtil.getSingleton().addToStage(estimatedCfg_, StageUtil.STAGE_OBJECT_GROUP_DEBUGINFO);
		StageUtil.getSingleton().addToStage(targetSwing_, StageUtil.STAGE_OBJECT_GROUP_DEBUGINFO);
		debugState_.setActiveState("showing");

		var targetHitWidgetRadius:int = simWorld_.worldToScreenX(TARGET_HIT_WIDGET_RADIUS, true);
		if (targetHitWidgetRadius == 0) {
			throw new Error("hit target radius 0");
		}
		hitTarget_.graphics.clear();
		hitTarget_.graphics.lineStyle(4, 0x3333AA, 0.7);
		hitTarget_.graphics.moveTo(-targetHitWidgetRadius, -targetHitWidgetRadius);
		hitTarget_.graphics.lineTo(0, 0);
		hitTarget_.graphics.lineTo(targetHitWidgetRadius, -targetHitWidgetRadius);
		hitTarget_.graphics.endFill();
		hitTarget_.alpha = 0;
		
		estimatedCfg_.graphics.clear();
		estimatedCfg_.graphics.lineStyle(4, 0x339933, 0.7);
		estimatedCfg_.graphics.moveTo(-targetHitWidgetRadius, 0);
		estimatedCfg_.graphics.lineTo(0, targetHitWidgetRadius);
		estimatedCfg_.graphics.lineTo(targetHitWidgetRadius, 0);
		estimatedCfg_.graphics.lineTo(0, -targetHitWidgetRadius);
		estimatedCfg_.graphics.lineTo(-targetHitWidgetRadius, 0);
		estimatedCfg_.graphics.endFill();
		estimatedCfg_.alpha = 0;
		
		targetSwing_.graphics.clear();
		targetSwing_.graphics.lineStyle(4, 0xAA4433, 0.7);
		targetSwing_.graphics.moveTo(targetHitWidgetRadius, targetHitWidgetRadius);
		targetSwing_.graphics.lineTo(targetHitWidgetRadius, -targetHitWidgetRadius);
		targetSwing_.graphics.lineTo(-targetHitWidgetRadius, -targetHitWidgetRadius);
		targetSwing_.graphics.lineTo(-targetHitWidgetRadius, targetHitWidgetRadius);
		targetSwing_.graphics.lineTo(targetHitWidgetRadius, targetHitWidgetRadius);
		targetSwing_.graphics.endFill();
		targetSwing_.alpha = 0;
	}
	
	public function hide():void {
		StageUtil.getSingleton().removeFrameUpdateObject(this);
		StageUtil.getSingleton().removeFromStage(hitTarget_);
		StageUtil.getSingleton().removeFromStage(estimatedCfg_);
		StageUtil.getSingleton().removeFromStage(targetSwing_);
		debugState_.setActiveState("hidden");
	}
	
	// interface impl
	public function onFrameUpdate(frameNumber:int, frameStartTimeMsec:int, frameElapsedTime:Number):Boolean {
		var estimatedCfg:BallCfg = aiPlayer_.estimateSwing_.estimatedCfg_;
		var swingTargetCfg:BallCfg = aiPlayer_.estimateSwing_.swingTargetCfg_;
		if (aiPlayer_.state_.isStateActive(AiPlayerTable.STATE_AI_PLAY_WORKING)) {
			hitTarget_.alpha = aiPlayer_.estimateSwing_.hasHitTarget() ? 1 : 0;
			estimatedCfg_.alpha = aiPlayer_.estimateSwing_.hasEstimatedCfg() ? 1 : 0;
			targetSwing_.alpha = aiPlayer_.estimateSwing_.hasTargetSwing() ? 1 : 0;
			
			if (hitTarget_.alpha == 1) {
				hitTarget_.x = simWorld_.worldToScreenX(aiPlayer_.estimateSwing_.hitTargetPosX_);
				hitTarget_.y = simWorld_.worldToScreenY(aiPlayer_.estimateSwing_.hitTargetPosY_);
			}
			if (estimatedCfg_.alpha == 1) {
				estimatedCfg_.x = simWorld_.worldToScreenX(estimatedCfg.posX_);
				estimatedCfg_.y = simWorld_.worldToScreenY(estimatedCfg.posY_);
			}
			if (targetSwing_.alpha == 1) {
				targetSwing_.x = simWorld_.worldToScreenX(swingTargetCfg.posX_);
				targetSwing_.y = simWorld_.worldToScreenY(swingTargetCfg.posY_);
			}
		} else {
			hitTarget_.alpha = 0;
			estimatedCfg_.alpha = 0;
			targetSwing_.alpha = 0;
		}
		return true;
	}
	
	// interface impl
	public function toString():String {
		return StageUtil.FRAMEUPDATE_KEY_RENDER + "ai_player_renderer";
	}
}
}
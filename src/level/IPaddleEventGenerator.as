package level
{
public interface IPaddleEventGenerator {
	function addEventListener(type:String, listner:Function):void;
	function removeEventListener(type:String, listner:Function):void;
}
}